<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Usuarios
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Usuarios
        <small>Listado</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <!-- Default box -->
        <div class="box box-info">
            <div class="box-header with-border">
                <a class="btn btn-primary" href="<?= base_url(route_to('usuarios_create')) ?>"><i class="fa fa-plus-circle"></i> Nuevo Registro</a>

                <div class="box-tools">
                    <form id="form-search" action="<?= base_url(route_to('usuarios_search')) ?>" method="POST">
                        <div class="input-group input-group-md" style="width: 250px;">
                            <input type="text" name="patron" id="patron" class="form-control pull-right text-uppercase" placeholder="Buscar Usuario...">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12 table-responsive">
                        <?php if (session('msg')) : ?>
                            <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                            </div>
                        <?php endif ?>
                        <hr>
                        <table id="t_usuarios" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Avatar</th>
                                    <th>Nombre</th>
                                    <th>Usuario</th>
                                    <th>Activo</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_usuarios">
                                <!-- Aquí se pintarán los registros recuperados de la BD mediante AJAX -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scripts') ?>
<script src="<?php echo base_url(); ?>/public/js/funciones.js"></script>
<script>
    $(document).ready(function() {
        listarUsuarios();
        $('#form-search').submit(function(e) {
            e.preventDefault();
            let url = $(this).attr('action');
            let method = $(this).attr('method');
            var request = $.ajax({
                url: url,
                method: method,
                data: {
                    patron: $('#patron').val()
                },
                dataType: "json"
            });

            request.done(function(data) {
                let html = '';
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        html += '<tr>' +
                            '<td>' + (i + 1) + '</td>' +
                            '<td>' + data[i].id_asignatura + '</td>' +
                            '<td>' + data[i].ar_nombre + '</td>' +
                            '<td>' + data[i].as_nombre + '</td>' +
                            '<td>' + data[i].as_abreviatura + '</td>' +
                            '<td>' +
                            '<div class="btn-group">' +
                            '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_asignatura +
                            '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                            '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_asignatura +
                            '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                            '</div>' +
                            '</td>' +
                            '</tr>';
                    }
                    $("#tbody_usuarios").html(html);
                } else {
                    html += '<tr><td colspan="6">No se han encontrado coincidencias...</td></tr>';
                    $("#tbody_usuarios").html(html);
                }
            });

            request.fail(function(jqXHR, textStatus) {
                alert("Requerimiento fallido: " + jqXHR.responseText);
            });
        });
    });
    function listarUsuarios() {
        var request = $.ajax({
            url: "<?= base_url(route_to('usuarios_listar')) ?>",
            method: "get",
            dataType: "json"
        });

        request.done(function(data) {
            var html = '';
            if (data.length > 0) {
                for (let i = 0; i < data.length; i++) {
                    us_activo = (data[i].us_activo == 1) ? "Sí" : "No";
                    html += '<tr>' +
                        '<td>' + data[i].id_usuario + '</td>' +
                        '<td><img class="img-thumbnail" width="50" src="<?= base_url(); ?>/public/uploads/' + data[i].us_foto + '" alt="Avatar del Usuario"></td>' +
                        '<td>' + data[i].us_shortname + '</td>' +
                        '<td>' + data[i].us_login + '</td>' +
                        '<td>' + us_activo + '</td>' +
                        '<td>' +
                        '<div class="btn-group">' +
                        '<a href="<?= base_url(); ?>/admin/usuarios/edit/' + data[i].id_usuario + '" class="btn btn-warning item-edit" data="' + data[i].id_usuario +
                        '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                        '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_usuario +
                        '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                        '</div>' +
                        '</td>' +
                        '</tr>';
                }
                $("#tbody_usuarios").html(html);
            } else {
                html += '<tr><td colspan="6">No se han ingresado Usuarios todavía...</td></tr>';
                $("#tbody_usuarios").html(html);
            }
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requerimiento fallido: " + jqXHR.responseText);
        });
    }
</script>
<?= $this->endsection('scripts') ?>