<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Usuarios
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Usuarios
        <small>Crear</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Nuevo Usuario</div>
                    </div>
                    <div class="panel-body">
                        <?php if (session('msg')) : ?>
                            <?php if (session('msg')) : ?>
                                <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                        <form id="frm_usuario" class="form-horizontal" action="<?= base_url((route_to('usuarios_store'))) ?>" method="post" enctype="multipart/form-data" onsubmit="return validar()">
                            <div class="form-group <?= session('errors.us_titulo') ? 'has-error' : '' ?>">
                                <label for="us_titulo" class="col-sm-2 control-label">Título:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="us_titulo" id="us_titulo" value="<?= old('us_titulo') ?>" class="form-control">
                                    <span class="help-block"><?= session('errors.us_titulo') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.us_apellidos') ? 'has-error' : '' ?>">
                                <label for="us_apellidos" class="col-sm-2 control-label">Apellidos:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="us_apellidos" id="us_apellidos" value="<?= old('us_apellidos') ?>" class="form-control">
                                    <span class="help-block"><?= session('errors.us_apellidos') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.us_nombres') ? 'has-error' : '' ?>">
                                <label for="us_nombres" class="col-sm-2 control-label">Nombres:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="us_nombres" id="us_nombres" value="<?= old('us_nombres') ?>" class="form-control">
                                    <span class="help-block"><?= session('errors.us_nombres') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.us_login') ? 'has-error' : '' ?>">
                                <label for="us_login" class="col-sm-2 control-label">Usuario:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="us_login" id="us_login" value="<?= old('us_login') ?>" class="form-control">
                                    <span class="help-block"><?= session('errors.us_login') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.us_password') ? 'has-error' : '' ?>">
                                <label for="us_password" class="col-sm-2 control-label">Password:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="us_password" id="us_password" value="<?= old('us_password') ?>" class="form-control">
                                    <span class="help-block"><?= session('errors.us_password') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.us_genero') ? 'has-error' : '' ?>">
                                <label for="us_genero" class="col-sm-2 control-label">Género:</label>
                                <div class="col-sm-10">
                                    <select name="us_genero" id="us_genero" class="form-control">
                                        <option value="F">Femenino</option>
                                        <option value="M">Masculino</option>
                                    </select>
                                    <span class="help-block"><?= session('errors.us_genero') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.us_activo') ? 'has-error' : '' ?>">
                                <label for="us_activo" class="col-sm-2 control-label">Activo:</label>
                                <div class="col-sm-10">
                                    <select name="us_activo" id="us_activo" class="form-control">
                                        <option value="1">Sí</option>
                                        <option value="0">No</option>
                                    </select>
                                    <span class="help-block"><?= session('errors.us_activo') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.id_perfil') ? 'has-error' : '' ?>">
                                <label for="id_perfil" class="col-sm-2 control-label">Perfil:</label>
                                <div class="col-sm-10">
                                    <select name="id_perfil[]" id="id_perfil" class="form-control" multiple size="7">
                                    <?php foreach($perfiles as $perfil): ?>
                                        <option value="<?=$perfil->id_perfil;?>"><?=$perfil->pe_nombre;?></option>
                                    <?php endforeach; ?>
                                    </select>
                                    <span class="help-block"><?= session('errors.id_perfil') ?></span>
                                </div>
                            </div>
                            <div id="img_upload" class="<?= session('errors.us_foto') ? 'has-error' : '' ?>">
                                <div class="form-group">
                                    <label for="us_avatar" class="col-sm-2 control-label"></label>

                                    <div id="img_div" class="col-sm-10 hide">
                                        <img id="us_avatar" name="us_avatar" class="img-thumbnail" width="75" alt="Avatar del usuario">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="us_foto" class="col-sm-2 control-label" style="margin-top: -4px;">Imagen:</label>

                                    <div class="col-sm-10">
                                        <input type="file" name="us_foto" id="us_foto">
                                    </div>
                                    <span class="help-block"><?= session('errors.us_foto') ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                </div>
                                <div class="col-sm-10">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <a href="<?= base_url(route_to('usuarios')) ?>" class="btn btn-default">Regresar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scripts') ?>
<script>
$(document).ready(function () {

    $("#us_foto").change(function () {
        $("#img_div").removeClass("hide");
        filePreview(this);
    });

});

function filePreview(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(input.files[0]);
        reader.onload = function (e) {
            $("#us_avatar").attr("src", e.target.result);
        }
    }
}

function validar() {
    // Validar la entrada del campo id_perfil
    if ($("#id_perfil").val().trim()==='') {
        alert("Debe seleccionar el Perfil.");
        $("#id_perfil").focus();
        return false;
    }

    // Validar la entrada de los campos input
    if ($("#us_titulo").val().trim()==='') {
        alert("Debe ingresar el valor del campo Título.");
        $("#us_titulo").focus();
        return false;
    } else if ($("#us_apellidos").val().trim()==='') {
        alert("Debe ingresar el valor del campo Apellidos.");
        $("#us_apellidos").focus();
        return false;
    } else if ($("#us_nombres").val().trim()==='') {
        alert("Debe ingresar el valor del campo Nombres.");
        $("#us_nombres").focus();
        return false;
    } else if ($("#us_login").val().trim()==='') {
        alert("Debe ingresar el valor del campo Usuario.");
        $("#us_login").focus();
        return false;
    } else if ($("#us_password").val().trim()==='') {
        alert("Debe ingresar el valor del campo Password.");
        $("#us_password").focus();
        return false;
    }

    var img=document.forms['frm_usuario']['us_foto'];
    var validExt=["jpeg","png","jpg","JPEG","JPG","PNG"];

    if (img.value!='') {
        var img_ext=img.value.substring(img.value.lastIndexOf('.')+1);

        var result=validExt.includes(img_ext);

        if (result==false) {
            alert("Debe cargar un archivo .jpg o .jpeg o .png");
            return false;
        } else {
            var CurrentFileSize = parseFloat(img.files[0].size/(1024*1024));
            if (CurrentFileSize >= 1) {
                alert("El archivo de imagen debe tener un tamaño máximo de 1 Mb. Tamaño actual: "+CurrentFileSize.toPrecision(4)+" Mb.");
                return false;
            }
        }
    } else {
        alert("Debe cargar un archivo de imagen.");
        return false;
    }

    return true;
}

</script>
<?= $this->endsection('scripts') ?>