<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Escalas de Calificaciones
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Escalas de Calificaciones
        <small>Listado</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <a class="btn btn-primary" href="<?= base_url(route_to('escalas_calificaciones_create')) ?>"><i class="fa fa-plus-circle"></i> Nuevo Registro</a>
                    <?php if (session('msg')) : ?>
                        <div class="alert alert-<?= session('msg.type') ?> alert-dismissible" style="margin-top: 2px">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                        </div>
                    <?php endif ?>
                    <hr>
                    <table id="t_escalas" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Id</th>
                                <th>Cualitativa</th>
                                <th>Cuantitativa</th>
                                <th>Nota Mínima</th>
                                <th>Nota Máxima</th>
                                <th>Equivalencia</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_escalas">
                            <?php $cont = 1; foreach ($escalas_calificaciones as $v) : ?>
                                <tr data-index="<?= $v->id_escala_calificaciones ?>" data-orden="<?= $v->ec_orden ?>">
                                    <td><?= $cont ?></td>
                                    <td><?= $v->id_escala_calificaciones ?></td>
                                    <td><?= $v->ec_cualitativa ?></td>
                                    <td><?= $v->ec_cuantitativa ?></td>
                                    <td><?= $v->ec_nota_minima ?></td>
                                    <td><?= $v->ec_nota_maxima ?></td>
                                    <td><?= $v->ec_equivalencia ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="<?= base_url(route_to('escalas_calificaciones_edit', $v->id_escala_calificaciones)) ?>" class="btn btn-warning btn-sm" title="Editar"><span class="fa fa-pencil"></span></a>
                                            <a href="<?= base_url(route_to('escalas_calificaciones_delete', $v->id_escala_calificaciones)) ?>" class="btn btn-danger btn-sm" title="Eliminar"><span class="fa fa-remove"></span></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php $cont++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scripts') ?>
<script>
$(document).ready(function() {
    //Hacer la tabla "sortable"
    $('table tbody').sortable({
        update: function(event, ui) {
            $(this).children().each(function(index) {
                if ($(this).attr('data-orden') != (index + 1)) {
                    $(this).attr('data-orden', (index + 1)).addClass('updated');
                }
            });
            saveNewPositions();
        }
    });
});

function saveNewPositions() {
    var positions = [];
    $('.updated').each(function() {
        positions.push([$(this).attr('data-index'), $(this).attr('data-orden')]);
        $(this).removeClass('updated');
    });

    $.ajax({
        url: "<?= base_url(route_to('escalas_calificaciones_saveNewPositions')) ?>",
        method: 'POST',
        dataType: 'text',
        data: {
            positions: positions
        },
        success: function(response) {
            //console.log(response);
        }
    });
}
</script>
<?= $this->endsection('scripts') ?>
