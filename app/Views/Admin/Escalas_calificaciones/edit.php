<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Escalas de Calificaciones
<?= $this->endsection('titulo') ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>/public/css/custom.css">
<?= $this->endsection('styles') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Escalas de Calificaciones
        <small>Editar</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-info">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="box-header with-border">
                        <div id="titulo" class="box-title">Editar Escala de Calificación</div>
                    </div>
                    <?php if (session('msg')) : ?>
                        <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                        </div>
                    <?php endif ?>
                    <form id="frm_id" class="form-horizontal" action="<?= base_url((route_to('escalas_calificaciones_update'))) ?>" method="post" autocomplete="off">
                        <input type="hidden" name="id_escala_calificaciones" value="<?= $escala_calificaciones->id_escala_calificaciones ?>">
                        <div class="form-group <?= session('errors.ec_cualitativa') ? 'has-error' : '' ?>">
                            <label for="ec_cualitativa" class="col-sm-2 control-label requerido">Escala Cualitativa:</label>
                            <div class="col-sm-10">
                                <input type="text" name="ec_cualitativa" id="ec_cualitativa" value="<?= old('ec_cualitativa') ?? $escala_calificaciones->ec_cualitativa ?>" class="form-control" autofocus required>
                                <span class="help-block"><?= session('errors.ec_cualitativa') ?></span>
                            </div>
                        </div>
                        <div class="form-group <?= session('errors.ec_cuantitativa') ? 'has-error' : '' ?>">
                            <label for="ec_cuantitativa" class="col-sm-2 control-label requerido">Escala Cuantitativa:</label>
                            <div class="col-sm-10">
                                <input type="text" name="ec_cuantitativa" id="ec_cuantitativa" value="<?= old('ec_cuantitativa') ?? $escala_calificaciones->ec_cuantitativa ?>" class="form-control" required>
                                <span class="help-block"><?= session('errors.ec_cuantitativa') ?></span>
                            </div>
                        </div>
                        <div class="form-group <?= session('errors.ec_nota_minima') ? 'has-error' : '' ?>">
                            <label for="ec_nota_minima" class="col-sm-2 control-label requerido">Nota Mínima:</label>
                            <div class="col-sm-10">
                                <input type="number" step="any" min="0" max="10" name="ec_nota_minima" id="ec_nota_minima" value="<?= old('ec_nota_minima') ?? $escala_calificaciones->ec_nota_minima ?>" class="form-control" required>
                                <span class="help-block"><?= session('errors.ec_nota_minima') ?></span>
                            </div>
                        </div>
                        <div class="form-group <?= session('errors.ec_nota_maxima') ? 'has-error' : '' ?>">
                            <label for="ec_nota_maxima" class="col-sm-2 control-label requerido">Nota Máxima:</label>
                            <div class="col-sm-10">
                                <input type="number" step="any" min="0" max="10" name="ec_nota_maxima" id="ec_nota_maxima" value="<?= old('ec_nota_maxima') ?? $escala_calificaciones->ec_nota_maxima ?>" class="form-control" required>
                                <span class="help-block"><?= session('errors.ec_nota_maxima') ?></span>
                            </div>
                        </div>
                        <div class="form-group <?= session('errors.ec_equivalencia') ? 'has-error' : '' ?>">
                            <label for="ec_equivalencia" class="col-sm-2 control-label requerido">Equivalencia:</label>
                            <div class="col-sm-10">
                                <input type="text" name="ec_equivalencia" id="ec_equivalencia" value="<?= old('ec_equivalencia') ?? $escala_calificaciones->ec_equivalencia ?>" class="form-control" required>
                                <span class="help-block"><?= session('errors.ec_equivalencia') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-10">
                                <button id="btn-save" type="submit" class="btn btn-success">Actualizar</button>
                                <a href="<?= base_url(route_to('escalas_calificaciones')) ?>" class="btn btn-default">Regresar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>
