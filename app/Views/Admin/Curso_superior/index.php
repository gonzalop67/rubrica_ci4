<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Cursos Superiores
<?= $this->endsection('titulo') ?>

<?= $this->section('styles') ?>
<style>
    .fuente10 {
        font: 10pt helvetica;
    }
</style>
<?= $this->endsection('styles') ?>

<?= $this->section('contenido') ?>
<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Asociar Cursos Superiores</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <form id="form_id" action="" class="form-horizontal">
                <div class="form-group <?= session('errors.id_curso_inferior') ? 'has-error' : '' ?>">
                    <label for="id_curso" class="col-sm-2 control-label text-right">Curso Inferior:</label>

                    <div class="col-sm-10">
                        <select class="form-control fuente10" name="id_curso_inferior" id="id_curso_inferior" required>
                            <option value="">Seleccione...</option>
                            <?php foreach ($cursos as $v) : ?>
                                <option value="<?= $v->id_curso; ?>" <?= old('id_curso') == $v->id_curso ? 'selected' : '' ?>><?= "[" . $v->es_figura . "] - " . $v->cu_nombre; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span class="help-block"><?= session('errors.id_curso_inferior') ?></span>
                    </div>
                </div>
                <div class="form-group <?= session('errors.id_curso_superior') ? 'has-error' : '' ?>">
                    <label for="id_curso_superior" class="col-sm-2 control-label text-right">Curso Superior:</label>

                    <div class="col-sm-10">
                        <select class="form-control fuente10" name="id_curso_superior" id="id_curso_superior" required>
                            <option value="">Seleccione...</option>
                            <?php foreach ($cursos_superiores as $v) : ?>
                                <option value="<?= $v->id_curso_superior; ?>" <?= old('id_curso_superior') == $v->id_curso_superior ? 'selected' : '' ?>><?= $v->cs_nombre; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span class="help-block"><?= session('errors.id_asignatura') ?></span>
                    </div>
                </div>
                <div class="row" id="botones_insercion">
                    <div class="col-sm-12" style="margin-top: 4px;">
                        <button id="btn-add-item" type="submit" class="btn btn-block btn-primary">
                            Asociar
                        </button>
                    </div>
                </div>
            </form>
            <!-- Línea de división -->
            <hr>
            <!-- message -->
            <div id="text_message" class="fuente10 text-center"></div>
            <!-- table -->
            <table class="table fuente10">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Curso Inferior</th>
                        <th>Curso Superior</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody id="lista_items">
                    <!-- Aqui desplegamos el contenido de la base de datos -->
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scriptsPlugins') ?>
<script src="<?php echo base_url(); ?>/public/jquery-ui/jquery-ui.min.js"></script>
<?= $this->endsection('scriptsPlugins') ?>

<?= $this->section('scripts') ?>
<script src="<?php echo base_url(); ?>/public/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>/public/jquery-validation/localization/messages_es.min.js"></script>
<script src="<?php echo base_url(); ?>/public/js/funciones.js"></script>
<script>
    $(document).ready(function() {
        Biblioteca.validacionGeneral('form_id');

        cargar_cursos_asociados();

        $('#lista_items').on('click', '.item-delete', function(e) {
            e.preventDefault();
            let id = $(this).attr('data');
            $("#text_message").html("<img src='<?php echo base_url(); ?>/public/images/ajax-loader-blue.GIF' alt='procesando...' />");
            $.ajax({
                url: "<?= base_url(route_to('cursos_superiores_delete')) ?>",
                method: "post",
                data: {
                    id: id
                },
                dataType: "json",
                success: function(response) {
                    $("#text_message").html("");
                    swal({
                        title: response.titulo,
                        text: response.mensaje,
                        type: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    cargar_cursos_asociados();
                },
                error: function(jqXHR, textStatus) {
                    alert(jqXHR.responseText);
                }
            });
        });

        $("#form_id").submit(function(e) {
            e.preventDefault();

            //Insertar Asociación de Curso Superior
            let id_curso_inferior = document.getElementById("id_curso_inferior").value;
            let id_curso_superior = document.getElementById("id_curso_superior").value;

            if (id_curso_inferior !== "" && id_curso_superior !== "") {
                $("#text_message").html("<img src='<?php echo base_url(); ?>/public/images/ajax-loader-blue.GIF' alt='procesando...' />");

                $.ajax({
                    url: "<?= base_url(route_to('cursos_superiores_store')) ?>",
                    method: "post",
                    data: {
                        id_curso_inferior: id_curso_inferior,
                        id_curso_superior: id_curso_superior
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#text_message").html("");
                        swal({
                            title: response.titulo,
                            text: response.mensaje,
                            type: response.tipo_mensaje,
                            confirmButtonText: 'Aceptar'
                        });
                        cargar_cursos_asociados();
                    },
                    error: function(jqXHR, textStatus) {
                        alert(jqXHR.responseText);
                    }
                });
            } // fin if (id_curso_inferior !== "" && id_curso_superior !== "")
        });
    });

    function cargar_cursos_asociados(id_curso) {
        var request = $.ajax({
            url: "<?= base_url(route_to('cursos_superiores_list')) ?>",
            method: "get",
            dataType: "html"
        });

        request.done(function(response) {
            $("#lista_items").html(response);
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requerimiento fallido: " + jqXHR.responseText);
        });
    }

</script>
<?= $this->endsection('scripts') ?>