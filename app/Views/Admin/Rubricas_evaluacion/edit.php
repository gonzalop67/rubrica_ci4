<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Insumos de Evaluación
<?= $this->endsection('titulo') ?>

<?= $this->section('styles') ?>
<!-- jquery-ui -->
<link rel="stylesheet" href="<?php echo base_url(); ?>/public/jquery-ui/jquery-ui.css">
<?= $this->endsection('styles') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Insumos de Evaluación
        <small>Editar</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Editar Insumo de Evaluación</div>
                    </div>
                    <div class="panel-body">
                        <?php if (session('msg')) : ?>
                            <?php if (session('msg')) : ?>
                                <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                        <form id="frm-aporte-evaluacion" action="<?= base_url((route_to('rubricas_evaluacion_update'))) ?>" method="post">
                            <input type="hidden" name="id_rubrica_evaluacion" value="<?= $rubrica_evaluacion->id_rubrica_evaluacion ?>">
                            <div class="form-group <?= session('errors.ru_nombre') ? 'has-error' : '' ?>">
                                <label for="ru_nombre">Nombre:</label>
                                <input type="text" name="ru_nombre" id="ru_nombre" value="<?= old('ru_nombre') ?? $rubrica_evaluacion->ru_nombre ?>" class="form-control" autofocus>
                                <span class="help-block"><?= session('errors.ru_nombre') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.ru_abreviatura') ? 'has-error' : '' ?>">
                                <label for="ru_abreviatura">Abreviatura:</label>
                                <input type="text" name="ru_abreviatura" id="ru_abreviatura" value="<?= old('ru_abreviatura') ?? $rubrica_evaluacion->ru_abreviatura ?>" class="form-control">
                                <span class="help-block"><?= session('errors.ru_abreviatura') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.id_aporte_evaluacion') ? 'has-error' : '' ?>">
                                <label for="id_aporte_evaluacion">Aporte de Evaluación:</label>
                                <select name="id_aporte_evaluacion" id="id_aporte_evaluacion" class="form-control">
                                    <?php foreach ($aportes_evaluacion as $v) : ?>
                                        <?php
                                        $selected = '';
                                        if (!empty(old('id_aporte_evaluacion'))) {
                                            if (old('id_aporte_evaluacion') == $v->id_aporte_evaluacion) {
                                                $selected = 'selected';
                                            }
                                        } else {
                                            if ($rubrica_evaluacion->id_aporte_evaluacion == $v->id_aporte_evaluacion) {
                                                $selected = 'selected';
                                            }
                                        }
                                        ?>
                                        <option value="<?= $v->id_aporte_evaluacion ?>" <?= $selected ?>>
                                            <?= $v->ap_nombre . ' - ' . $v->pe_nombre ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"><?= session('errors.id_aporte_evaluacion') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.id_tipo_asignatura') ? 'has-error' : '' ?>">
                                <label for="id_tipo_asignatura">Tipo de Asignatura:</label>
                                <select name="id_tipo_asignatura" id="id_tipo_asignatura" class="form-control">
                                    <?php foreach ($tipos_asignatura as $v) : ?>
                                        <?php
                                        $selected = '';
                                        if (!empty(old('id_tipo_asignatura'))) {
                                            if (old('id_tipo_asignatura') == $v->id_tipo_asignatura) {
                                                $selected = 'selected';
                                            }
                                        } else {
                                            if ($rubrica_evaluacion->id_tipo_asignatura == $v->id_tipo_asignatura) {
                                                $selected = 'selected';
                                            }
                                        }
                                        ?>
                                        <option value="<?= $v->id_tipo_asignatura ?>" <?= $selected ?>>
                                            <?= $v->ta_descripcion ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"><?= session('errors.id_tipo_asignatura') ?></span>
                            </div>
                            <div class="form-group">
                                <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                <a href="<?= base_url(route_to('rubricas_evaluacion')) ?>" class="btn btn-default">Regresar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>