<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Periodos lectivos
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Periodos lectivos
        <small>Listado</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <?php if (session('msg')) : ?>
                        <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                        </div>
                    <?php endif ?>
                    <a class="btn btn-danger" href="<?= base_url(route_to('periodos_lectivos_create')) ?>">Crear Periodo Lectivo</a>
                    <hr>
                    <table id="t_periodos_lectivos" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Año Inicial</th>
                                <th>Año Final</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_periodos_lectivos">
                            <?php foreach ($periodos_lectivos as $v) : ?>
                                <tr>
                                    <td><?= $v->id_periodo_lectivo ?></td>
                                    <td><?= $v->pe_anio_inicio ?></td>
                                    <td><?= $v->pe_anio_fin ?></td>
                                    <td><?= $v->pe_descripcion ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="<?= base_url(route_to('periodos_lectivos_edit', $v->id_periodo_lectivo)) ?>" class="btn btn-warning btn-sm" title="Editar"><span class="fa fa-pencil"></span></a>
                                            <a href="<?= base_url(route_to('periodos_lectivos_delete', $v->id_periodo_lectivo)) ?>" class="btn btn-danger btn-sm" title="Cerrar"><span class="fa fa-remove"></span></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                    <?= $pager->links() ?>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>