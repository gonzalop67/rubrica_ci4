<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Modalidades
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Modalidades
        <small>Editar</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Modalidad: <?= $modalidad->mo_nombre ?> </div>
                    </div>
                    <div class="panel-body">
                        <?php if (session('msg')) : ?>
                            <?php if (session('msg')) : ?>
                                <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                        <form id="frm-modalidad" action="<?= base_url((route_to('modalidades_update'))) ?>" method="post">
                            <input type="hidden" name="id_modalidad" id="id_modalidad" value="<?= $modalidad->id_modalidad ?>">
                            <div class="form-group <?= session('errors.mo_nombre') ? 'has-error' : '' ?>">
                                <label for="mo_nombre">Nombre:</label>
                                <input type="text" name="mo_nombre" id="mo_nombre" class="form-control" value="<?= old('mo_nombre') ?? $modalidad->mo_nombre ?>" autofocus>
                                <span class="help-block"><?= session('errors.mo_nombre') ?></span>
                            </div>
                            <div class="form-group">
                                <label for="mo_activo">Activo:</label>
                                <select name="mo_activo" id="mo_activo" class="form-control">
                                    <option value="1" <?= $modalidad->mo_activo == 1 ? 'selected' : '' ?>>Sí</option>
                                    <option value="0" <?= $modalidad->mo_activo == 0 ? 'selected' : '' ?>>No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                <a href="<?= base_url(route_to('modalidades')) ?>" class="btn btn-default">Regresar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>