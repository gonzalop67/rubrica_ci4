<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Información de la Institución
<?= $this->endsection('titulo') ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>/public/css/custom.css">
<?= $this->endsection('styles') ?>

<?= $this->section('contenido') ?>
<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Datos de la Institución Educativa</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <?php if (session('msg')) : ?>
                <?php if (session('msg')) : ?>
                    <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                    </div>
                <?php endif ?>
            <?php endif ?>
            <form id="form_id" action="" method="POST" class="form-horizontal">
                <div class="form-group <?= session('errors.in_nombre') ? 'has-error' : '' ?>">
                    <label for="in_nombre" class="col-sm-2 control-label text-right requerido">Nombre:</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control fuente10" name="in_nombre" id="in_nombre" value="" required>
                        <span class="help-block"><?= session('errors.in_nombre') ?></span>
                    </div>
                </div>
                <div class="form-group <?= session('errors.in_direccion') ? 'has-error' : '' ?>">
                    <label for="in_direccion" class="col-sm-2 control-label text-right requerido">Dirección:</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control fuente10" name="in_direccion" id="in_direccion" value="" required>
                        <span class="help-block"><?= session('errors.in_direccion') ?></span>
                    </div>
                </div>
                <div class="form-group <?= session('errors.in_telefono') ? 'has-error' : '' ?>">
                    <label for="in_telefono" class="col-sm-2 control-label text-right requerido">Teléfono:</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control fuente10" name="in_telefono" id="in_telefono" value="" required>
                        <span class="help-block"><?= session('errors.in_telefono') ?></span>
                    </div>
                </div>
                <div class="form-group <?= session('errors.in_nom_rector') ? 'has-error' : '' ?>">
                    <label for="in_nom_rector" class="col-sm-2 control-label text-right requerido">Rector (a):</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control fuente10" name="in_nom_rector" id="in_nom_rector" value="" required>
                        <span class="help-block"><?= session('errors.in_nom_rector') ?></span>
                    </div>
                </div>
                <div class="form-group <?= session('errors.in_nom_vicerrector') ? 'has-error' : '' ?>">
                    <label for="in_nom_vicerrector" class="col-sm-2 control-label text-right">Vicerrector (a):</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control fuente10" name="in_nom_vicerrector" id="in_nom_vicerrector" value="">
                        <span class="help-block"><?= session('errors.in_nom_vicerrector') ?></span>
                    </div>
                </div>
                <div class="form-group <?= session('errors.in_nom_secretario') ? 'has-error' : '' ?>">
                    <label for="in_nom_secretario" class="col-sm-2 control-label text-right">Secretario (a):</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control fuente10" name="in_nom_secretario" id="in_nom_secretario" value="">
                        <span class="help-block"><?= session('errors.in_nom_secretario') ?></span>
                    </div>
                </div>
                <div class="form-group <?= session('errors.in_url') ? 'has-error' : '' ?>">
                    <label for="in_url" class="col-sm-2 control-label text-right">URL:</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control fuente10" name="in_url" id="in_url" value="">
                        <span class="help-block"><?= session('errors.in_url') ?></span>
                    </div>
                </div>
                <div class="form-group <?= session('errors.in_amie') ? 'has-error' : '' ?>">
                    <label for="in_amie" class="col-sm-2 control-label text-right requerido">AMIE:</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control fuente10" name="in_amie" id="in_amie" value="" required>
                        <span class="help-block"><?= session('errors.in_amie') ?></span>
                    </div>
                </div>
                <div class="form-group <?= session('errors.in_ciudad') ? 'has-error' : '' ?>">
                    <label for="in_ciudad" class="col-sm-2 control-label text-right requerido">Ciudad:</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control fuente10" name="in_ciudad" id="in_ciudad" value="" required>
                        <span class="help-block"><?= session('errors.in_ciudad') ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="in_copiar_y_pegar" class="col-sm-2 control-label">Copy & Paste:</label>
                    <div class="col-sm-10" style="position: relative; top:7px;">
                        <input type="checkbox" id="in_copiar_y_pegar" name="in_copiar_y_pegar">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12" style="margin-top: 4px;">
                        <button id="btn-add-item" type="submit" class="btn btn-block btn-primary">
                            Guardar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scripts') ?>
<script src="<?php echo base_url(); ?>/public/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>/public/jquery-validation/localization/messages_es.min.js"></script>
<script src="<?php echo base_url(); ?>/public/js/funciones.js"></script>
<script>
    $(document).ready(function() {
        Biblioteca.validacionGeneral('form_id');
        obtenerDatosInstitucion();
        $("#form_id").submit(function(e) {
            e.preventDefault();

            //Insertar o Actualizar los datos de la IE
            let in_nombre = document.getElementById("in_nombre").value;
            let in_direccion = document.getElementById("in_direccion").value;
            let in_telefono = document.getElementById("in_telefono").value;
            let in_nom_rector = document.getElementById("in_nom_rector").value;
            let in_nom_vicerrector = document.getElementById("in_nom_vicerrector").value;
            let in_nom_secretario = document.getElementById("in_nom_secretario").value;
            let in_url = document.getElementById("in_url").value;
            let in_amie = document.getElementById("in_amie").value;
            let in_ciudad = document.getElementById("in_ciudad").value;
            let in_copiar_y_pegar = document.getElementById("in_copiar_y_pegar").checked ? 1 : 0;

            if (in_nombre !== "" && in_direccion !== "" && in_telefono !== "" && in_nom_rector !== "" && in_amie !== "" && in_ciudad !== "") {
                $.ajax({
                    url: "<?= base_url(route_to('instituciones_store')) ?>",
                    method: "post",
                    data: {
                        in_nombre: in_nombre,
                        in_direccion: in_direccion,
                        in_telefono: in_telefono,
                        in_nom_rector: in_nom_rector,
                        in_nom_vicerrector: in_nom_vicerrector,
                        in_nom_secretario: in_nom_secretario,
                        in_url: in_url,
                        in_amie: in_amie,
                        in_ciudad: in_ciudad,
                        in_copiar_y_pegar: in_copiar_y_pegar
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#text_message").html("");
                        swal({
                            title: response.titulo,
                            text: response.mensaje,
                            type: response.tipo_mensaje,
                            confirmButtonText: 'Aceptar'
                        });
                        obtenerDatosInstitucion();
                    },
                    error: function(jqXHR, textStatus) {
                        alert(jqXHR.responseText);
                    }
                });
            } // fin if (in_nombre !== "" && in_direccion !== "" && in_telefono !== "" && in_nom_rector !== "" && in_amie !== "" && in_ciudad !== "")
        });
    });

    function obtenerDatosInstitucion() {
        $.ajax({
            url: "<?= base_url(route_to('instituciones_getData')) ?>",
            success: function(resultado) {
                var JSONInstitucion = eval('(' + resultado + ')');
                //Aqui se van a pintar los datos de la institucion educativa
                document.getElementById("in_nombre").value = (JSONInstitucion.in_nombre) ? JSONInstitucion.in_nombre : "";
                document.getElementById("in_direccion").value = (JSONInstitucion.in_direccion) ? JSONInstitucion.in_direccion : "";
                document.getElementById("in_telefono").value = (JSONInstitucion.in_telefono) ? JSONInstitucion.in_telefono : "";
                document.getElementById("in_nom_rector").value = (JSONInstitucion.in_nom_rector) ? JSONInstitucion.in_nom_rector : "";
                document.getElementById("in_nom_vicerrector").value = (JSONInstitucion.in_nom_vicerrector) ? JSONInstitucion.in_nom_vicerrector : "";
                document.getElementById("in_nom_secretario").value = (JSONInstitucion.in_nom_secretario) ? JSONInstitucion.in_nom_secretario : "";
                document.getElementById("in_url").value = (JSONInstitucion.in_url) ? JSONInstitucion.in_url : "";
                document.getElementById("in_amie").value = (JSONInstitucion.in_amie) ? JSONInstitucion.in_amie : "";
                document.getElementById("in_ciudad").value = (JSONInstitucion.in_ciudad) ? JSONInstitucion.in_ciudad : "";
                document.getElementById("in_copiar_y_pegar").checked = (JSONInstitucion.in_copiar_y_pegar == 1) ? true : false;
                document.getElementById("in_nombre").focus();
            }
        });
    }
</script>
<?= $this->endsection('scripts') ?>