<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Niveles de Educación
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Niveles de Educación
        <small>Editar</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Nivel de Educación: <?= $tipo_educacion->te_nombre ?></div>
                    </div>
                    <div class="panel-body">
                        <?php if (session('msg')) : ?>
                            <?php if (session('msg')) : ?>
                                <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                        <form id="frm-nivel-educacion" action="<?= base_url((route_to('tipos_educacion_update'))) ?>" method="post">
                        <input type="hidden" name="id_tipo_educacion" id="id_tipo_educacion" value="<?= $tipo_educacion->id_tipo_educacion ?>">
                            <div class="form-group <?= session('errors.te_nombre') ? 'has-error' : '' ?>">
                                <label for="te_nombre">Nombre:</label>
                                <input type="text" name="te_nombre" id="te_nombre" value="<?= old('te_nombre') ?? $tipo_educacion->te_nombre ?>" class="form-control" autofocus>
                                <span class="help-block"><?= session('errors.te_nombre') ?></span>
                            </div>
                            <div class="form-group <?= session('errors.te_bachillerato') ? 'has-error' : '' ?>">
                                <label for="te_bachillerato">¿Es Bachillerato?:</label>
                                <select name="te_bachillerato" id="te_bachillerato" class="form-control">
                                    <option value="1" <?= old('te_bachillerato') ?? $tipo_educacion->te_bachillerato == 1 ? 'selected' : '' ?>>Sí</option>
                                    <option value="0" <?= old('te_bachillerato') ?? $tipo_educacion->te_bachillerato == 0 ? 'selected' : '' ?>>No</option>
                                </select>
                                <span class="help-block"><?= session('errors.te_bachillerato') ?></span>
                            </div>
                            <div class="form-group">
                                <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                <a href="<?= base_url(route_to('tipos_educacion')) ?>" class="btn btn-default">Regresar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>