<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Especialidades
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Especialidades
        <small>Editar</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Especialidad: <?= $especialidad->es_figura ?></div>
                    </div>
                    <div class="panel-body">
                        <?php if (session('msg')) : ?>
                            <?php if (session('msg')) : ?>
                                <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                        <form id="frm_especialidad" class="form-horizontal" action="<?= base_url((route_to('especialidades_update'))) ?>" method="post">
                            <input type="hidden" name="id_especialidad" id="id_especialidad" value="<?= $especialidad->id_especialidad ?>">
                            <div class="form-group <?= session('errors.es_nombre') ? 'has-error' : '' ?>">
                                <label for="es_nombre" class="col-sm-2 control-label">Nombre:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="es_nombre" id="es_nombre" value="<?= old('es_nombre') ?? $especialidad->es_nombre ?>" class="form-control">
                                    <span class="help-block"><?= session('errors.es_nombre') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.es_figura') ? 'has-error' : '' ?>">
                                <label for="es_figura" class="col-sm-2 control-label">Figura Profesional:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="es_figura" id="es_figura" value="<?= old('es_figura') ?? $especialidad->es_figura ?>" class="form-control">
                                    <span class="help-block"><?= session('errors.es_figura') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.es_abreviatura') ? 'has-error' : '' ?>">
                                <label for="es_abreviatura" class="col-sm-2 control-label">Abreviatura:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="es_abreviatura" id="es_abreviatura" value="<?= old('es_abreviatura') ?? $especialidad->es_abreviatura ?>" class="form-control">
                                    <span class="help-block"><?= session('errors.es_abreviatura') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.id_tipo_educacion') ? 'has-error' : '' ?>">
                                <label for="id_tipo_educacion" class="col-sm-2 control-label">Nivel de Educación:</label>
                                <div class="col-sm-10">
                                    <select name="id_tipo_educacion" id="id_tipo_educacion" class="form-control">
                                    <?php foreach($tipos_educacion as $tipo_educacion): ?>
                                        <option value="<?=$tipo_educacion->id_tipo_educacion;?>" <?= old('id_tipo_educacion') ?? $tipo_educacion->id_tipo_educacion==$especialidad->id_tipo_educacion ? 'selected' : '' ?>><?=$tipo_educacion->te_nombre;?></option>
                                    <?php endforeach; ?>
                                    </select>
                                    <span class="help-block"><?= session('errors.id_tipo_educacion') ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                </div>
                                <div class="col-sm-10">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <a href="<?= base_url(route_to('especialidades')) ?>" class="btn btn-default">Regresar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>
