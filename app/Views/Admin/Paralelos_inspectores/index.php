<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Paralelos Inspectores
<?= $this->endsection('titulo') ?>

<?= $this->section('styles') ?>
<style>
    .fuente10 {
        font: 10pt helvetica;
    }
</style>
<?= $this->endsection('styles') ?>

<?= $this->section('contenido') ?>
<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Asociar Paralelos con Inspectores</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <form id="form_id" action="" class="form-horizontal">
                <div class="form-group <?= session('errors.id_paralelo') ? 'has-error' : '' ?>">
                    <label for="id_paralelo" class="col-sm-2 control-label text-right">Paralelo:</label>

                    <div class="col-sm-10">
                        <select class="form-control fuente10" name="id_paralelo" id="id_paralelo" required>
                            <option value="">Seleccione...</option>
                            <?php foreach ($paralelos as $v) : ?>
                                <option value="<?= $v->id_paralelo; ?>" <?= old('id_paralelo') == $v->id_paralelo ? 'selected' : '' ?>><?= $v->cu_nombre . " " . $v->pa_nombre . " - " . $v->es_figura . " - " . $v->jo_nombre; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span class="help-block"><?= session('errors.id_paralelo') ?></span>
                    </div>
                </div>
                <div class="form-group <?= session('errors.id_usuario') ? 'has-error' : '' ?>">
                    <label for="id_usuario" class="col-sm-2 control-label text-right">Inspector:</label>

                    <div class="col-sm-10">
                        <select class="form-control fuente10" name="id_usuario" id="id_usuario" required>
                            <option value="">Seleccione...</option>
                            <?php foreach ($usuarios as $v) : ?>
                                <option value="<?= $v->id_usuario; ?>" <?= old('id_usuario') == $v->id_usuario ? 'selected' : '' ?>><?= $v->us_apellidos . " " . $v->us_nombres . ", " . $v->us_titulo; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span class="help-block"><?= session('errors.id_usuario') ?></span>
                    </div>
                </div>
                <div class="row" id="botones_insercion">
                    <div class="col-sm-12" style="margin-top: 4px;">
                        <button id="btn-add-item" type="submit" class="btn btn-block btn-primary">
                            Asociar
                        </button>
                    </div>
                </div>
            </form>
            <!-- Línea de división -->
            <hr>
            <!-- message -->
            <div id="text_message" class="fuente10 text-center"></div>
            <!-- table -->
            <table class="table fuente10">
                <thead>
                    <tr>
                        <th>Nro.</th>
                        <th>Id</th>
                        <th>Paralelo</th>
                        <th>Inspector</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody id="lista_items">
                    <!-- Aqui desplegamos el contenido de la base de datos -->
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-10 text-right">
                    <label class="control-label" style="position:relative; top:7px;">Total Paralelos Asociados:</label>
                </div>
                <div class="col-sm-2" style="margin-top: 2px;">
                    <input type="text" class="form-control fuente10 text-right" id="total_inspectores" value="0" disabled>
                </div>
            </div>
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scriptsPlugins') ?>
<script src="<?php echo base_url(); ?>/public/jquery-ui/jquery-ui.min.js"></script>
<?= $this->endsection('scriptsPlugins') ?>

<?= $this->section('scripts') ?>
<script src="<?php echo base_url(); ?>/public/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>/public/jquery-validation/localization/messages_es.min.js"></script>
<script src="<?php echo base_url(); ?>/public/js/funciones.js"></script>
<script>
    $(document).ready(function() {
        Biblioteca.validacionGeneral('form_id');

        cargar_paralelos_inspectores();

        $('#lista_items').on('click', '.item-delete', function(e) {
            e.preventDefault();
            let id = $(this).attr('data');
            $("#text_message").html("<img src='<?php echo base_url(); ?>/public/images/ajax-loader-blue.GIF' alt='procesando...' />");
            $.ajax({
                url: "<?= base_url(route_to('paralelos_inspectores_delete')) ?>",
                method: "post",
                data: {
                    id: id
                },
                dataType: "json",
                success: function(response) {
                    $("#text_message").html("");
                    swal({
                        title: response.titulo,
                        text: response.mensaje,
                        type: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    cargar_paralelos_inspectores();
                },
                error: function(jqXHR, textStatus) {
                    alert(jqXHR.responseText);
                }
            });
        });

        $("#form_id").submit(function(e) {
            e.preventDefault();

            //Insertar Asociación de Paralelo con Tutor
            let id_paralelo = document.getElementById("id_paralelo").value;
            let id_usuario = document.getElementById("id_usuario").value;

            if (id_paralelo !== "" && id_usuario !== "") {
                $("#text_message").html("<img src='<?php echo base_url(); ?>/public/images/ajax-loader-blue.GIF' alt='procesando...' />");

                $.ajax({
                    url: "<?= base_url(route_to('paralelos_inspectores_store')) ?>",
                    method: "post",
                    data: {
                        id_paralelo: id_paralelo,
                        id_usuario: id_usuario
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#text_message").html("");
                        swal({
                            title: response.titulo,
                            text: response.mensaje,
                            type: response.tipo_mensaje,
                            confirmButtonText: 'Aceptar'
                        });
                        cargar_paralelos_inspectores();
                    },
                    error: function(jqXHR, textStatus) {
                        alert(jqXHR.responseText);
                    }
                });
            } // fin if (id_curso !== "" && id_asignatura !== "")
        });
    });

    function cargar_paralelos_inspectores() {
        var request = $.ajax({
            url: "<?= base_url(route_to('paralelos_inspectores_list')) ?>",
            method: "get",
            dataType: "json"
        });

        request.done(function(data) {
            var html = '';
            if (data.length > 0) {
                for (let i = 0; i < data.length; i++) {
                    html += '<tr>' +
                        '<td>' + (i + 1) + '</td>' +
                        '<td>' + data[i].id_paralelo_inspector + '</td>' +
                        '<td>' + data[i].cu_nombre + ' ' + data[i].pa_nombre + ' - [' + data[i].es_figura + ']</td>' +
                        '<td>' + data[i].us_titulo + ' ' + data[i].us_fullname + '</td>' +
                        '<td>' +
                        '<div class="btn-group">' +
                        '<a href="javascript:;" class="btn btn-danger btn-sm item-delete" data="' + data[i].id_paralelo_inspector +
                        '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                        '</div>' +
                        '</td>' +
                        '</tr>';
                }
                $("#lista_items").html(html);
                $("#total_inspectores").val(data.length);
            } else {
                $("#lista_items").html("<tr><td colspan='5' align='center'>No se han asociado paralelos con inspectores...</td></tr>");
                $("#total_inspectores").val(0);
            }
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requerimiento fallido: " + jqXHR.responseText);
        });
    }
</script>
<?= $this->endsection('scripts') ?>