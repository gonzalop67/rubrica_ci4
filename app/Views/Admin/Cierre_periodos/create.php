<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Cierre de Periodos
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Cierre de Periodos
        <small>Crear</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-success">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="box-header with-border mb-5">
                        <div id="titulo" class="box-title">Nuevo Cierre de Periodo</div>
                    </div>
                    <?php if (session('msg')) : ?>
                        <?php if (session('msg')) : ?>
                            <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                            </div>
                        <?php endif ?>
                    <?php endif ?>
                    <form id="frm-cierre-periodo" action="<?= base_url((route_to('cierre_periodos_store'))) ?>" method="post">
                        <div class="form-group <?= session('errors.id_paralelo') ? 'has-error' : '' ?>">
                            <label for="id_paralelo">Paralelo:</label>
                            <select name="id_paralelo" id="id_paralelo" class="form-control">
                                <?php foreach ($paralelos as $paralelo) : ?>
                                    <option value="<?= $paralelo->id_paralelo; ?>" <?= old('id_paralelo') == $paralelo->id_paralelo ? 'selected' : '' ?>>
                                        <?= "[" . $paralelo->es_figura . "] " . $paralelo->cu_nombre . " " . $paralelo->pa_nombre . " (" . $paralelo->jo_nombre . ")"; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <span class="help-block"><?= session('errors.id_paralelo') ?></span>
                        </div>
                        <div class="form-group <?= session('errors.id_aporte_evaluacion') ? 'has-error' : '' ?>">
                            <label for="id_aporte_evaluacion">Aporte de Evaluación:</label>
                            <select name="id_aporte_evaluacion" id="id_aporte_evaluacion" class="form-control">
                                <?php foreach ($aportes_evaluacion as $aporte_evaluacion) : ?>
                                    <option value="<?= $aporte_evaluacion->id_aporte_evaluacion; ?>" <?= old('id_aporte_evaluacion') == $aporte_evaluacion->id_aporte_evaluacion ? 'selected' : '' ?>>
                                        <?= $aporte_evaluacion->ap_nombre; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <span class="help-block"><?= session('errors.id_aporte_evaluacion') ?></span>
                        </div>
                        <div class="form-group <?= session('errors.ap_fecha_apertura') ? 'has-error' : '' ?>">
                            <label for="ap_fecha_apertura">Fecha de apertura:</label>
                            <div class="controls">
                                <div class="input-group date">
                                    <input type="text" name="ap_fecha_apertura" id="ap_fecha_apertura" class="form-control" autocomplete="off">
                                    <label class="input-group-addon generic-btn" style="cursor: pointer;" onclick="$('#ap_fecha_apertura').focus();"><i class="fa fa-calendar" aria-hidden="true"></i></label>
                                </div>
                                <span class="help-block"><?= session('errors.ap_fecha_apertura') ?></span>
                            </div>
                        </div>
                        <div class="form-group <?= session('errors.ap_fecha_cierre') ? 'has-error' : '' ?>">
                            <label for="ap_fecha_cierre">Fecha de cierre:</label>
                            <div class="controls">
                                <div class="input-group date">
                                    <input type="text" name="ap_fecha_cierre" id="ap_fecha_cierre" class="form-control" autocomplete="off">
                                    <label class="input-group-addon generic-btn" style="cursor: pointer;" onclick="$('#ap_fecha_cierre').focus();"><i class="fa fa-calendar" aria-hidden="true"></i></label>
                                </div>
                                <span class="help-block"><?= session('errors.ap_fecha_cierre') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                            <a href="<?= base_url(route_to('cierre_periodos')) ?>" class="btn btn-default">Regresar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scripts') ?>
<script>
    $(document).ready(function() {
        $("#ap_fecha_apertura").datepicker({
            dateFormat: 'yy-mm-dd',
            firstDay: 1
        });

        $("#ap_fecha_cierre").datepicker({
            dateFormat: 'yy-mm-dd',
            firstDay: 1
        });
    });
</script>
<?= $this->endsection('scripts') ?>