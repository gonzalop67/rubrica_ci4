<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Cierre de Periodos
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Cierre de Periodos
        <small>Editar</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-warning">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="box-header with-border mb-5">
                        <div id="titulo" class="box-title">Editar Cierre de Periodo</div>
                    </div>
                    <?php if (session('msg')) : ?>
                        <?php if (session('msg')) : ?>
                            <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                            </div>
                        <?php endif ?>
                    <?php endif ?>
                    <form id="frm-cierre-periodo" action="<?= base_url((route_to('cierre_periodos_update'))) ?>" method="post">
                        <input type="hidden" name="id_aporte_paralelo_cierre" value="<?= $cierre_periodo->id_aporte_paralelo_cierre ?>">
                        <div class="form-group <?= session('errors.id_paralelo') ? 'has-error' : '' ?>">
                            <label for="id_paralelo">Paralelo:</label>
                            <select name="id_paralelo" id="id_paralelo" class="form-control">
                                <?php foreach ($paralelos as $v) : ?>
                                    <option value="<?= $v->id_paralelo; ?>" <?= old('id_paralelo') == $v->id_paralelo ? 'selected' : ($v->id_paralelo == $cierre_periodo->id_paralelo ? 'selected' : '') ?>>
                                        <?= "[" . $v->es_figura . "] " . $v->cu_nombre . " " . $v->pa_nombre . " (" . $v->jo_nombre . ")"; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <span class="help-block"><?= session('errors.id_paralelo') ?></span>
                        </div>
                        <div class="form-group <?= session('errors.id_aporte_evaluacion') ? 'has-error' : '' ?>">
                            <label for="id_aporte_evaluacion">Aporte de Evaluación:</label>
                            <select name="id_aporte_evaluacion" id="id_aporte_evaluacion" class="form-control">
                                <?php foreach ($aportes_evaluacion as $v) : ?>
                                    <option value="<?= $v->id_aporte_evaluacion; ?>" <?= old('id_aporte_evaluacion') == $v->id_aporte_evaluacion ? 'selected' : ($v->id_aporte_evaluacion == $cierre_periodo->id_aporte_evaluacion ? 'selected' : '') ?>>
                                        <?= $v->ap_nombre; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <span class="help-block"><?= session('errors.id_aporte_evaluacion') ?></span>
                        </div>
                        <div class="form-group <?= session('errors.ap_fecha_apertura') ? 'has-error' : '' ?>">
                            <label for="ap_fecha_apertura">Fecha de apertura:</label>
                            <div class="controls">
                                <div class="input-group date">
                                    <input type="text" name="ap_fecha_apertura" id="ap_fecha_apertura" class="form-control" value="<?= old('ap_fecha_apertura') ?? $cierre_periodo->ap_fecha_apertura ?>" autocomplete="off">
                                    <label class="input-group-addon generic-btn" style="cursor: pointer;" onclick="$('#ap_fecha_apertura').focus();"><i class="fa fa-calendar" aria-hidden="true"></i></label>
                                </div>
                                <span class="help-block"><?= session('errors.ap_fecha_apertura') ?></span>
                            </div>
                        </div>
                        <div class="form-group <?= session('errors.ap_fecha_cierre') ? 'has-error' : '' ?>">
                            <label for="ap_fecha_cierre">Fecha de cierre:</label>
                            <div class="controls">
                                <div class="input-group date">
                                    <input type="text" name="ap_fecha_cierre" id="ap_fecha_cierre" class="form-control" value="<?= old('ap_fecha_cierre') ?? $cierre_periodo->ap_fecha_cierre ?>" autocomplete="off">
                                    <label class="input-group-addon generic-btn" style="cursor: pointer;" onclick="$('#ap_fecha_cierre').focus();"><i class="fa fa-calendar" aria-hidden="true"></i></label>
                                </div>
                                <span class="help-block"><?= session('errors.ap_fecha_cierre') ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                            <a href="<?= base_url(route_to('cierre_periodos')) ?>" class="btn btn-default">Regresar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scripts') ?>
<script>
    $(document).ready(function() {
        $("#ap_fecha_apertura").datepicker({
            dateFormat: 'yy-mm-dd',
            firstDay: 1
        });

        $("#ap_fecha_cierre").datepicker({
            dateFormat: 'yy-mm-dd',
            firstDay: 1
        });
    });
</script>
<?= $this->endsection('scripts') ?>