<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Paralelos
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Paralelos
        <small>Crear</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div id="titulo" class="panel-heading">Nuevo Paralelo</div>
                    </div>
                    <div class="panel-body">
                        <?php if (session('msg')) : ?>
                            <?php if (session('msg')) : ?>
                                <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                        <form id="frm_paralelo" class="form-horizontal" action="<?= base_url((route_to('paralelos_store'))) ?>" method="post">
                            <div class="form-group <?= session('errors.pa_nombre') ? 'has-error' : '' ?>">
                                <label for="pa_nombre" class="col-sm-2 control-label">Nombre:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="pa_nombre" id="pa_nombre" value="<?= old('pa_nombre') ?>" class="form-control">
                                    <span class="help-block"><?= session('errors.pa_nombre') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.id_curso') ? 'has-error' : '' ?>">
                                <label for="id_curso" class="col-sm-2 control-label">Curso:</label>
                                <div class="col-sm-10">
                                    <select name="id_curso" id="id_curso" class="form-control">
                                        <option value="">Seleccione...</option>
                                        <?php foreach($cursos as $curso): ?>
                                        <option value="<?=$curso->id_curso;?>" <?= old('id_curso') == $curso->id_curso ? 'selected' : '' ?>><?="[".$curso->es_figura."] ".$curso->cu_nombre;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="help-block"><?= session('errors.id_curso') ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= session('errors.id_jornada') ? 'has-error' : '' ?>">
                                <label for="id_jornada" class="col-sm-2 control-label requerido">Jornada:</label>
                                <div class="col-sm-10">
                                    <select name="id_jornada" id="id_jornada" class="form-control">
                                        <option value="">Seleccione...</option>
                                        <?php foreach($jornadas as $jornada): ?>
                                        <option value="<?=$jornada->id_jornada;?>" <?= old('id_jornada') == $jornada->id_jornada ? 'selected' : '' ?>><?=$jornada->jo_nombre;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="help-block"><?= session('errors.id_jornada') ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                </div>
                                <div class="col-sm-10">
                                    <button id="btn-save" type="submit" class="btn btn-success">Guardar</button>
                                    <a href="<?= base_url(route_to('paralelos')) ?>" class="btn btn-default">Regresar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>
