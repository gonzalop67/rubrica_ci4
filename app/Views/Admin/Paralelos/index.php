<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Paralelos
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Paralelos
        <small>Listado</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-solid">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <a class="btn btn-primary" href="<?= base_url(route_to('paralelos_create')) ?>"><i class="fa fa-plus-circle"></i> Nuevo Registro</a>
                    <?php if (session('msg')) : ?>
                        <div class="alert alert-<?= session('msg.type') ?> alert-dismissible" style="margin-top: 2px">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                        </div>
                    <?php endif ?>
                    <hr>
                    <table id="t_paralelos" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Id</th>
                                <th>Especialidad</th>
                                <th>Curso</th>
                                <th>Nombre</th>
                                <th>Jornada</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_paralelos">
                            <!-- Aquí se poblarán los paralelos mediante -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <input type="hidden" id="id_paralelo">
    <input type="hidden" id="id_curso">
</section>
<!-- /.content -->

<!-- Edit Paralelo Modal -->
<div class="modal fade" id="editParalelo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title text-center" id="myModalLabel3">Editar Paralelo</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-2">
                        <label class="control-label" style="position:relative; top:7px;">Curso:</label>
                    </div>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="edit_cu_nombre" name="edit_cu_nombre" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <label class="control-label" style="position:relative; top:7px;">Jornada:</label>
                    </div>
                    <div class="col-lg-10">
                        <select class="form-control" id="edit_cboJornada">
                            <option value="0">Seleccione...</option>
                        </select>
                        <span class="help-desk error" id="mensaje2"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <label class="control-label" style="position:relative; top:7px;">Nombre:</label>
                    </div>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="edit_pa_nombre" name="edit_pa_nombre" value="">
                        <span class="help-desk error" id="mensaje3"></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="updateParalelo()"><span class="glyphicon glyphicon-pencil"></span> Actualizar</a>
            </div>
        </div>
    </div>
</div>
<?= $this->endsection('contenido') ?>

<?= $this->section('scripts') ?>
<script src="<?php echo base_url(); ?>/public/js/funciones.js"></script>
<script>
    $(document).ready(function() {
        listarParalelos();
        cargar_jornadas();
        $('table tbody').sortable({
            update: function(event, ui) {
                $(this).children().each(function(index) {
                    if ($(this).attr('data-orden') != (index + 1)) {
                        $(this).attr('data-orden', (index + 1)).addClass('updated');
                    }
                });
                saveNewPositions();
                listarParalelos();
            }
        });
        $('table tbody').on('click', '.item-edit', function() {
            var id_paralelo = $(this).attr('data');
            $.ajax({
                url: "<?= base_url(route_to('paralelos_getById')) ?>",
                method: "post",
                data: {
                    id_paralelo: id_paralelo
                },
                dataType: "json",
                success: function(data) {
                    $("#id_paralelo").val(id_paralelo);
                    $("#id_curso").val(data.id_curso);
                    $("#edit_cu_nombre").val("[" + data.es_figura + "] " + data.cu_nombre);
                    setearIndice("edit_cboJornada", data.id_jornada);
                    $("#edit_pa_nombre").val(data.pa_nombre);
                    $('#editParalelo').modal('show');
                },
                error: function(jqXHR, textStatus) {
                    alert(jqXHR.responseText);
                }
            });
        });
        $('table tbody').on('click', '.item-delete', function() {
            var id_paralelo = $(this).attr('data');
            $.ajax({
                url: "<?= base_url(route_to('paralelos_delete')) ?>",
                method: "post",
                data: {
                    id_paralelo: id_paralelo
                },
                dataType: "json",
                success: function(response) {
                    swal({
                        title: response.titulo,
                        text: response.mensaje,
                        type: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    listarParalelos();
                },
                error: function(jqXHR, textStatus) {
                    alert(jqXHR.responseText);
                }
            });
        });
    });

    function listarParalelos() {
        var request = $.ajax({
            url: "<?= base_url(route_to('paralelos_listar')) ?>",
            method: "get",
            dataType: "html"
        });

        request.done(function(response) {
            $("#tbody_paralelos").html(response);
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requerimiento fallido: " + jqXHR.responseText);
        });
    }

    function cargar_jornadas() {
        var request = $.ajax({
            url: "<?= base_url(route_to('jornadas_obtener')) ?>",
            method: "get",
            dataType: "html"
        });

        request.done(function(response) {
            $("#edit_cboJornada").append(response);
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requerimiento fallido: " + jqXHR.responseText);
        });
    }

    function saveNewPositions() {
        var positions = [];
        $('.updated').each(function() {
            positions.push([$(this).attr('data-index'), $(this).attr('data-orden')]);
            $(this).removeClass('updated');
        });

        $.ajax({
            url: "<?= base_url(route_to('paralelos_saveNewPositions')) ?>",
            method: 'POST',
            dataType: 'text',
            data: {
                positions: positions
            },
            success: function(response) {
                console.log(response);
            }
        });
    }

    function updateParalelo() {
        var id_paralelo = $("#id_paralelo").val();
        var id_curso = $("#id_curso").val();
        var id_jornada = $("#edit_cboJornada").val();
        var pa_nombre = $("#edit_pa_nombre").val();

        // expresion regular para validar el ingreso del nombre
        var reg_nombre = /^([a-zA-Z.]{1,5})$/i;

        // contador de errores
        var cont_errores = 0;

        if (id_jornada == 0) {
            $("#mensaje2").html("Debes seleccionar la Jornada");
            $("#mensaje2").fadeIn("slow");
            cont_errores++;
        } else {
            $("#mensaje3").fadeOut();
        }

        if (pa_nombre.trim() == "") {
            $("#mensaje3").html("Debes ingresar el nombre del Paralelo");
            $("#mensaje3").fadeIn("slow");
            cont_errores++;
        } else if (!reg_nombre.test(pa_nombre)) {
            $("#mensaje3").html("Debes ingresar un nombre válido para el Paralelo");
            $("#mensaje3").fadeIn("slow");
            cont_errores++;
        } else {
            $("#mensaje3").fadeOut();
        }

        if (cont_errores == 0) {
            $.ajax({
                url: "<?= base_url((route_to('paralelos_update'))) ?>",
                method: "POST",
                dataType: "json",
                data: {
                    id_paralelo: id_paralelo,
                    id_curso: id_curso,
                    id_jornada: id_jornada,
                    pa_nombre: pa_nombre
                },
                success: function(response) {
                    $('#editParalelo').modal('hide');
                    swal({
                        title: response.titulo,
                        text: response.mensaje,
                        type: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    listarParalelos();
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
        }
    }
</script>
<?= $this->endsection('scripts') ?>