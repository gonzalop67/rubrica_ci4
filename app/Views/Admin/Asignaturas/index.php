<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Asignaturas
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Asignaturas
        <small>Listado</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
            <a class="btn btn-primary btn-md" href="<?= base_url(route_to('asignaturas_create')) ?>"><i class="fa fa-plus-circle"></i> Nuevo Registro</a>

            <div class="box-tools">
                <form id="form-search" action="<?= base_url(route_to('asignaturas_search')) ?>" method="POST">
                    <div class="input-group input-group-md" style="width: 250px;">
                        <input type="text" name="patron" id="patron" class="form-control pull-right text-uppercase" placeholder="Buscar Asignatura...">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="box-body">
            <input type="hidden" name="id_area" id="id_area">
            <input type="hidden" name="id_asignatura" id="id_asignatura">
            <div class="row">
                <div class="col-md-12 table-responsive">
                    <?php if (session('msg')) : ?>
                        <div class="alert alert-<?= session('msg.type') ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><i class="icon fa fa-<?= session('msg.icon') ?>"></i> <?= session('msg.body') ?></p>
                        </div>
                    <?php endif ?>
                    <table id="t_asignaturas" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Id</th>
                                <th>Area</th>
                                <th>Nombre</th>
                                <th>Abreviatura</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="tbody_asignaturas">
                            <!-- Aquí se pintarán los registros recuperados de la BD mediante AJAX -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- Edit Asignatura Modal -->
    <div class="modal fade" id="editAsignatura" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title text-center" id="myModalLabel3">Editar Asignatura</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group row">
                            <label for="edit_cbo_areas" class="col-sm-2 col-form-label">Area:</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="edit_cbo_areas" name="edit_cbo_areas">
                                    <option value="0">Seleccione...</option>
                                </select>
                                <span class="help-desk error" id="mensaje4"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="edit_as_nombre" class="col-sm-2 col-form-label">Nombre:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="edit_as_nombre" name="edit_as_nombre" value="">
                                <span class="help-desk error" id="mensaje5"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="edit_as_abreviatura" class="col-sm-2 col-form-label">Abreviatura:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="edit_as_abreviatura" name="edit_as_abreviatura" value="">
                                <span class="help-desk error" id="mensaje6"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="edit_cbo_tipos" class="col-sm-2 col-form-label">Tipo:</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="edit_cbo_tipos">
                                    <!-- Aqui se cargan los tipos de asignaturas dinamicamente -->
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>
                    <button type="button" class="btn btn-primary" onclick="updateAsignatura()"><span class="glyphicon glyphicon-pencil"></span> Actualizar</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scripts') ?>
<script src="<?php echo base_url(); ?>/public/js/funciones.js"></script>
<script>
    $(document).ready(function() {
        listarAsignaturas();
        cargar_areas();
        cargar_tipos_asignatura();
        $('table tbody').on('click', '.item-edit', function() {
            var id_asignatura = $(this).attr('data');
            $.ajax({
                url: "<?= base_url(route_to('asignaturas_getById')) ?>",
                method: "post",
                data: {
                    id_asignatura: id_asignatura
                },
                dataType: "json",
                success: function(asignatura) {
                    $("#id_asignatura").val(id_asignatura);
                    $("#id_area").val(asignatura.id_area);
                    $("#edit_ar_nombre").val(asignatura.ar_nombre);
                    $("#edit_as_nombre").val(asignatura.as_nombre);
                    $("#edit_as_abreviatura").val(asignatura.as_abreviatura);
                    setearIndice("edit_cbo_tipos", asignatura.id_tipo_asignatura);
                    setearIndice("edit_cbo_areas", asignatura.id_area);
                    $('#editAsignatura').modal('show');
                },
                error: function(jqXHR, textStatus) {
                    alert(jqXHR.responseText);
                }
            });
        });
        $('table tbody').on('click', '.item-delete', function() {
            var id_asignatura = $(this).attr('data');
            $.ajax({
                url: "<?= base_url(route_to('asignaturas_delete')) ?>",
                method: "post",
                data: {
                    id_asignatura: id_asignatura
                },
                dataType: "json",
                success: function(response) {
                    swal({
                        title: response.titulo,
                        text: response.mensaje,
                        type: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    listarAsignaturas();
                },
                error: function(jqXHR, textStatus) {
                    alert(jqXHR.responseText);
                }
            });
        });
        
        $('#form-search').submit(function(e) {
            e.preventDefault();
            let url = $(this).attr('action');
            let method = $(this).attr('method');
            var request = $.ajax({
                url: url,
                method: method,
                data: {
                    patron: $('#patron').val()
                },
                dataType: "json"
            });

            request.done(function(data) {
                let html = '';
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        html += '<tr>' +
                            '<td>' + (i + 1) + '</td>' +
                            '<td>' + data[i].id_asignatura + '</td>' +
                            '<td>' + data[i].ar_nombre + '</td>' +
                            '<td>' + data[i].as_nombre + '</td>' +
                            '<td>' + data[i].as_abreviatura + '</td>' +
                            '<td>' +
                            '<div class="btn-group">' +
                            '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_asignatura +
                            '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                            '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_asignatura +
                            '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                            '</div>' +
                            '</td>' +
                            '</tr>';
                    }
                    $("#tbody_asignaturas").html(html);
                } else {
                    html += '<tr><td colspan="6">No se han encontrado coincidencias...</td></tr>';
                    $("#tbody_asignaturas").html(html);
                }
            });

            request.fail(function(jqXHR, textStatus) {
                alert("Requerimiento fallido: " + jqXHR.responseText);
            });
        });
    });

    function listarAsignaturas() {
        var request = $.ajax({
            url: "<?= base_url(route_to('asignaturas_listar')) ?>",
            method: "get",
            dataType: "json"
        });

        request.done(function(data) {
            var html = '';
            if (data.length > 0) {
                for (let i = 0; i < data.length; i++) {
                    html += '<tr>' +
                        '<td>' + (i + 1) + '</td>' +
                        '<td>' + data[i].id_asignatura + '</td>' +
                        '<td>' + data[i].ar_nombre + '</td>' +
                        '<td>' + data[i].as_nombre + '</td>' +
                        '<td>' + data[i].as_abreviatura + '</td>' +
                        '<td>' +
                        '<div class="btn-group">' +
                        '<a href="javascript:;" class="btn btn-warning item-edit" data="' + data[i].id_asignatura +
                        '" title="Editar"><span class="fa fa-pencil"></span></a>' +
                        '<a href="javascript:;" class="btn btn-danger item-delete" data="' + data[i].id_asignatura +
                        '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                        '</div>' +
                        '</td>' +
                        '</tr>';
                }
                $("#tbody_asignaturas").html(html);
            } else {
                html += '<tr><td colspan="6">No se han ingresado Asignaturas todavía...</td></tr>';
                $("#tbody_asignaturas").html(html);
            }
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requerimiento fallido: " + jqXHR.responseText);
        });
    }

    function cargar_areas() {
        var request = $.ajax({
            url: "<?= base_url(route_to('areas_obtener')) ?>",
            method: "get",
            dataType: "html"
        });

        request.done(function(response) {
            $("#edit_cbo_areas").append(response);
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requerimiento fallido: " + jqXHR.responseText);
        });
    }

    function cargar_tipos_asignatura() {
        var request = $.ajax({
            url: "<?= base_url(route_to('tipos_asignatura_obtener')) ?>",
            method: "get",
            dataType: "html"
        });

        request.done(function(response) {
            $("#edit_cbo_tipos").append(response);
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requerimiento fallido: " + jqXHR.responseText);
        });
    }

    function updateAsignatura() {
        var id_area = $("#edit_cbo_areas").val();
        var id_asignatura = $("#id_asignatura").val();
        var as_nombre = $("#edit_as_nombre").val();
        var as_abreviatura = $("#edit_as_abreviatura").val();
        var id_tipo_asignatura = $("#edit_cbo_tipos").val();

        // expresiones regulares para validar el ingreso de datos
        var reg_nombre = /^([a-zA-Z0-9 ñáéíóúÑÁÉÍÓÚ]{4,84})$/i;
        var reg_abreviatura = /^([a-zA-Z.]{3,8})$/i;

        // contador de errores
        var cont_errores = 0;

        if (as_nombre.trim() == "") {
            $("#mensaje5").html("Debes ingresar el nombre de la Asignatura");
            $("#mensaje5").fadeIn("slow");
            cont_errores++;
        } else if (!reg_nombre.test(as_nombre)) {
            $("#mensaje5").html("Debes ingresar un nombre válido para la Asignatura");
            $("#mensaje5").fadeIn("slow");
            cont_errores++;
        } else {
            $("#mensaje5").fadeOut();
        }

        if (as_abreviatura.trim() == "") {
            $("#mensaje6").html("Debes ingresar la abreviatura de la Asignatura");
            $("#mensaje6").fadeIn("slow");
            cont_errores++;
        } else if (!reg_abreviatura.test(as_abreviatura)) {
            $("#mensaje6").html("Debes ingresar una abreviatura válida para la Asignatura");
            $("#mensaje6").fadeIn("slow");
            cont_errores++;
        } else {
            $("#mensaje6").fadeOut();
        }

        if (cont_errores == 0) {
            $.ajax({
                url: "<?= base_url((route_to('asignaturas_update'))) ?>",
                method: "POST",
                dataType: "json",
                data: {
                    id_asignatura: id_asignatura,
                    id_area: id_area,
                    as_nombre: as_nombre,
                    as_abreviatura: as_abreviatura,
                    id_tipo_asignatura: id_tipo_asignatura
                },
                success: function(response) {
                    $('#editAsignatura').modal('hide');
                    swal({
                        title: response.titulo,
                        text: response.mensaje,
                        type: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    listarAsignaturas();
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
        }
    }
</script>
<?= $this->endsection('scripts') ?>