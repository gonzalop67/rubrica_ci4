<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Distributivo Docente
<?= $this->endsection('titulo') ?>

<?= $this->section('styles') ?>
<style>
    .fuente10 {
        font: 10pt helvetica;
    }
</style>
<?= $this->endsection('styles') ?>

<?= $this->section('contenido') ?>
<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Distributivo Docente</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <form id="form_id" action="" class="form-horizontal">
                <input type="hidden" name="id_distributivo" id="id_distributivo">
                <div class="form-group">
                    <label for="id_usuario" class="col-sm-2 control-label text-right">Docente:</label>

                    <div class="col-sm-10">
                        <select class="form-control fuente10" name="id_usuario" id="id_usuario" required>
                            <option value="">Seleccione...</option>
                            <?php foreach ($usuarios as $v) : ?>
                                <option value="<?= $v->id_usuario; ?>" <?= old('id_usuario') == $v->id_usuario ? 'selected' : '' ?>><?= $v->us_titulo . " " . $v->us_fullname; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span id="mensaje1" style="color: #e73d4a"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="id_paralelo" class="col-sm-2 control-label text-right">Paralelos:</label>

                    <div class="col-sm-10">
                        <select class="form-control fuente10" name="id_paralelo" id="id_paralelo" required>
                            <option value="">Seleccione...</option>
                            <?php foreach ($paralelos as $v) : ?>
                                <option value="<?= $v->id_paralelo; ?>" <?= old('id_paralelo') == $v->id_paralelo ? 'selected' : '' ?>><?= $v->cu_nombre . " " . $v->pa_nombre . " - " . $v->es_figura; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span id="mensaje2" style="color: #e73d4a"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="id_asignatura" class="col-sm-2 control-label text-right">Asignatura:</label>

                    <div class="col-sm-10">
                        <select class="form-control fuente10" name="id_asignatura" id="id_asignatura" required>
                            <option value="">Seleccione...</option>
                            <!-- Aquí se va a poblar las asignaturas dinámicamente mediante AJAX -->
                        </select>
                        <span id="mensaje3" style="color: #e73d4a"></span>
                    </div>
                </div>
                <div class="row" id="botones_insercion">
                    <div class="col-sm-12" style="margin-top: 4px;">
                        <button id="btn-add-item" type="button" class="btn btn-block btn-primary" onclick="insertarItemDistributivo()">
                            Asociar
                        </button>
                    </div>
                </div>
            </form>
            <!-- Línea de división -->
            <hr>
            <!-- message -->
            <div id="text_message" class="fuente10 text-center"></div>
            <!-- table -->
            <table class="table fuente10">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Paralelo</th>
                        <th>Asignatura</th>
                        <th>Presencial</th>
                        <th>Autónomo</th>
                        <th>Tutoría</th>
                        <th>SubTotal</th>
                        <th>
                            <!-- Aqui va el boton de eliminar -->
                        </th>
                    </tr>
                </thead>
                <tbody id="lista_items">
                    <!-- Aqui desplegamos el contenido de la base de datos -->
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-3 text-right">
                    <label class="control-label" style="position:relative; top:7px;">Presenciales:</label>
                </div>
                <div class="col-sm-1" style="margin-top: 2px;">
                    <input type="text" class="form-control fuente9 text-right" id="horas_presenciales" value="0" disabled>
                </div>
                <div class="col-sm-3 text-right">
                    <label class="control-label" style="position:relative; top:7px;">Tutorias:</label>
                </div>
                <div class="col-sm-1" style="margin-top: 2px;">
                    <input type="text" class="form-control fuente9 text-right" id="horas_tutorias" value="0" disabled>
                </div>
                <div class="col-sm-3 text-right">
                    <label class="control-label" style="position:relative; top:7px;">Total Horas:</label>
                </div>
                <div class="col-sm-1" style="margin-top: 2px;">
                    <input type="text" class="form-control fuente9 text-right" id="total_horas" value="0" disabled>
                </div>
            </div>
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scripts') ?>
<script src="<?php echo base_url(); ?>/public/js/funciones.js"></script>
<script>
    $(document).ready(function() {

        $("#lista_items").html("<tr><td colspan='8' align='center'>Debe seleccionar un docente...</td></tr>");

        $("#id_paralelo").change(function() {
            var id_paralelo = $(this).val();
            if (id_paralelo == "") {
                $("#mensaje2").html("Debe seleccionar un paralelo...");
                $("#mensaje2").fadeIn();
            } else {
                $("#mensaje2").fadeOut("slow");
                showAsignaturasAsociadas($(this).val());
            }
        });

        $("#id_usuario").change(function() {
            var id_usuario = $(this).val();
            if (id_usuario == "") {
                $("#horas_presenciales").val("0");
                $("#horas_tutorias").val("0");
                $("#total_horas").val("0");
                $("#mensaje1").html("Debe seleccionar un docente...");
                $("#mensaje1").fadeIn();
            } else {
                $("#mensaje1").fadeOut("slow");
                listarDistributivo();
            }
        });

        $('#lista_items').on('click', '.item-delete', function(e) {
            e.preventDefault();
            let id = $(this).attr('data');
            $("#text_message").html("<img src='<?php echo base_url(); ?>/public/images/ajax-loader-blue.GIF' alt='procesando...' />");
            $.ajax({
                url: "<?= base_url(route_to('distributivos_delete')) ?>",
                method: "post",
                data: {
                    id: id
                },
                dataType: "json",
                success: function(response) {
                    $("#text_message").html("");
                    swal({
                        title: response.titulo,
                        text: response.mensaje,
                        type: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    listarDistributivo();
                },
                error: function(jqXHR, textStatus) {
                    alert(jqXHR.responseText);
                }
            });
        });

    });

    function showAsignaturasAsociadas(id_paralelo) {
        $.each($('label'), function(key, value) {
            $(this).closest('.form-group').removeClass('has-error');
        });

        var request = $.ajax({
            url: "<?= base_url(route_to('asignaturas_cursos_getByParaleloId')) ?>",
            method: "post",
            data: {
                id_paralelo: id_paralelo
            },
            dataType: "json"
        });

        request.done(function(data) {
            var html = '';
            if (data.length > 0) {
                document.getElementById("id_asignatura").length = 0;
                $("#id_asignatura").append("<option value=''>Seleccione...</option>");
                for (let i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id_asignatura + '">' + data[i].as_nombre + '</option>';
                }
                $("#id_asignatura").append(html);
            } else {
                $("#lista_items").html("<tr><td colspan='7' align='center'>No se han asociado asignaturas a este paralelo...</td></tr>");
                $("#total_horas").val(0);
            }
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requerimiento fallido: " + jqXHR.responseText);
        });
    }

    function listarDistributivo() {
        var id_usuario = $("#id_usuario").val();
        if (id_usuario == "") {
            $("#mensaje1").html("Debe seleccionar un docente...");
            $("#mensaje1").fadein();
        } else {
            var request = $.ajax({
                url: "<?= base_url(route_to('distributivos_getByUsuarioId')) ?>",
                method: "post",
                data: {
                    id_usuario: id_usuario
                },
                dataType: "json"
            });

            request.done(function(data) {
                var datos = JSON.parse(data);
                $("#lista_items").html(datos.cadena);
                $("#horas_presenciales").val(datos.horas_presenciales);
                $("#horas_tutorias").val(datos.horas_tutorias);
                $("#total_horas").val(datos.total_horas);
                $("#text_message").html("");
            });

            request.fail(function(jqXHR, textStatus) {
                alert("Requerimiento fallido: " + jqXHR.responseText);
            });
        }
    }

    function insertarItemDistributivo() {
        // Recolección de datos
        var cont_errores = 0;
        var id_usuario = $("#id_usuario").val();
        var id_paralelo = $("#id_paralelo").val();
        var id_asignatura = $("#id_asignatura").val();

        // Validación de ingreso de datos
        if (id_usuario === "") {
            $("#mensaje1").html("Debe elegir el docente...");
            $("#mensaje1").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje1").fadeOut("slow");
        }

        if (id_paralelo === "") {
            $("#mensaje2").html("Debe elegir el paralelo...");
            $("#mensaje2").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje2").fadeOut("slow");
        }

        if (id_asignatura === "") {
            $("#mensaje3").html("Debe elegir la asignatura...");
            $("#mensaje3").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje3").fadeOut("slow");
        }

        if (cont_errores == 0) {
            // Se procede a la inserción del item del distributivo
            var request = $.ajax({
                url: "<?= base_url(route_to('distributivos_store')) ?>",
                method: "post",
                data: {
                    id_usuario: id_usuario,
                    id_paralelo: id_paralelo,
                    id_asignatura: id_asignatura
                },
                dataType: "json"
            });

            request.done(function(response) {
                swal({
                    title: response.titulo,
                    text: response.mensaje,
                    type: response.tipo_mensaje,
                    confirmButtonText: 'Aceptar'
                });
                listarDistributivo();
                $("#text_message").html("");
            });

            request.fail(function(jqXHR, textStatus) {
                alert("Requerimiento fallido: " + jqXHR.responseText);
            });
        }
    }
</script>
<?= $this->endsection('scripts') ?>