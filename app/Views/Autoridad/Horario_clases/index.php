<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Horarios de Clases
<?= $this->endsection('titulo') ?>

<?= $this->section('styles') ?>
<style>
    .fuente9 {
        font: 9pt helvetica;
    }
</style>
<?= $this->endsection('styles') ?>

<?= $this->section('contenido') ?>
<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Horarios de Clases</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <form id="form_id" action="" class="form-horizontal">
                <div class="form-group <?= session('errors.id_paralelo') ? 'has-error' : '' ?>">
                    <label for="id_paralelo" class="col-sm-2 control-label text-right">Paralelo:</label>

                    <div class="col-sm-10">
                        <select class="form-control fuente10" name="id_paralelo" id="id_paralelo">
                            <option value="">Seleccione...</option>
                            <?php foreach ($paralelos as $v) : ?>
                                <option value="<?= $v->id_paralelo; ?>" <?= old('id_paralelo') == $v->id_paralelo ? 'selected' : '' ?>><?= $v->cu_nombre . " " . $v->pa_nombre . " - " . $v->es_figura . " - " . $v->jo_nombre; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span id="mensaje1" style="color: #e73d4a"></span>
                    </div>
                </div>
            </form>
            <!-- Línea de división -->
            <hr>
            <!-- message -->
            <div id="message" class="fuente9 text-center">Debe seleccionar un paralelo...</div>
            <!-- table -->
            <table class="table table-bordered fuente9">
                <thead id="horario_cabecera">
                    <!-- Aqui desplegamos los dias de la semana de este paralelo -->
                </thead>
                <tbody id="horario_clases">
                    <!-- Aqui desplegamos las horas clase con su asignatura y docente -->
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scripts') ?>
<script>
    $(document).ready(function() {
        $("#id_paralelo").change(function(e) {
            e.preventDefault();
            if ($(this).val() == "") {
                $("#message").html("Debe seleccionar un paralelo...");
                $("#horario_cabecera").html("");
                $("#horario_clases").html("");
            } else {
                $("#text_message").html("");
                // Obtengo los días de la semana
                $.ajax({
                    url: "<?= base_url(route_to('dias_semana_por_paraleloID')) ?>",
                    data: {
                        id_paralelo: $(this).val()
                    },
                    method: "POST",
                    type: "html",
                    success: function(response) {
                        $("#message").html("");
                        $("#horario_cabecera").html(response);
                    },
                    error: function(xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
                // Luego las horas clase con sus asignaturas y docentes
                $.ajax({
                    url: "<?= base_url(route_to('horas_clase_por_paraleloID')) ?>",
                    data: {
                        id_paralelo: $(this).val()
                    },
                    method: "POST",
                    type: "html",
                    success: function(response) {
                        $("#message").html("");
                        $("#horario_clases").html(response);
                    },
                    error: function(xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
        });
    });
</script>
<?= $this->endsection('scripts') ?>