<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Asociar Horas Clase - Día de la Semana 
<?= $this->endsection('titulo') ?>

<?= $this->section('styles') ?>
<style>
    .fuente10 {
        font: 10pt helvetica;
    }
</style>
<?= $this->endsection('styles') ?>

<?= $this->section('contenido') ?>
<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Asociar Horas Clase - Día de la Semana</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <form id="form_id" action="" class="form-horizontal">
                <div class="form-group <?= session('errors.id_dia_semana') ? 'has-error' : '' ?>">
                    <label for="id_dia_semana" class="col-sm-2 control-label text-right">Día de la Semana:</label>

                    <div class="col-sm-10">
                        <select class="form-control fuente10" name="id_dia_semana" id="id_dia_semana" required>
                            <option value="">Seleccione...</option>
                            <?php foreach ($dias_semana as $v) : ?>
                                <option value="<?= $v->id_dia_semana; ?>" <?= old('id_dia_semana') == $v->id_dia_semana ? 'selected' : '' ?>><?= $v->ds_nombre; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span class="help-block"><?= session('errors.id_dia_semana') ?></span>
                    </div>
                </div>
                <div class="form-group <?= session('errors.id_hora_clase') ? 'has-error' : '' ?>">
                    <label for="id_hora_clase" class="col-sm-2 control-label text-right">Hora Clase:</label>

                    <div class="col-sm-10">
                        <select class="form-control fuente10" name="id_hora_clase" id="id_hora_clase" required>
                            <option value="">Seleccione...</option>
                            <?php foreach ($horas_clase as $v) : ?>
                                <option value="<?= $v->id_hora_clase; ?>" <?= old('id_hora_clase') == $v->id_hora_clase ? 'selected' : '' ?>><?= $v->hc_nombre . "(" . $v->hc_hora_inicio . " - " . $v->hc_hora_fin . ")"; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span class="help-block"><?= session('errors.id_hora_clase') ?></span>
                    </div>
                </div>
                <div class="row" id="botones_insercion">
                    <div class="col-sm-12" style="margin-top: 4px;">
                        <button id="btn-add-item" type="submit" class="btn btn-block btn-primary">
                            Asociar
                        </button>
                    </div>
                </div>
            </form>
            <!-- Línea de división -->
            <hr>
            <!-- message -->
            <div id="text_message" class="fuente10 text-center"></div>
            <!-- table -->
            <table class="table fuente10">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Día</th>
                        <th>Hora</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody id="lista_items">
                    <!-- Aqui desplegamos el contenido de la base de datos -->
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-10 text-right">
                    <label class="control-label" style="position:relative; top:7px;">Total Horas:</label>
                </div>
                <div class="col-sm-2" style="margin-top: 2px;">
                    <input type="text" class="form-control fuente10 text-right" id="total_horas" value="0" disabled>
                </div>
            </div>
        </div>
        <!-- /.box-footer -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scriptsPlugins') ?>
<script src="<?php echo base_url(); ?>/public/jquery-ui/jquery-ui.min.js"></script>
<?= $this->endsection('scriptsPlugins') ?>

<?= $this->section('scripts') ?>
<script src="<?php echo base_url(); ?>/public/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>/public/jquery-validation/localization/messages_es.min.js"></script>
<script src="<?php echo base_url(); ?>/public/js/funciones.js"></script>
<script>
    $(document).ready(function() {
        Biblioteca.validacionGeneral('form_id');

        $("#lista_items").html("<tr><td colspan='4' align='center'>Debe seleccionar un día de la semana...</td></tr>");

        $("#id_dia_semana").change(function() {
            var id_dia_semana = $(this).val();
            if (id_dia_semana == "")
                $("#lista_items").html("<tr><td colspan='4' align='center'>Debe seleccionar un día de la semana...</td></tr>");
            else
                showHorasClaseAsociadas(id_dia_semana);
        });

        $('#lista_items').on('click', '.item-delete', function(e) {
            e.preventDefault();
            let id = $(this).attr('data');
            let id_dia_semana = $("#id_dia_semana").val();
            $("#text_message").html("<img src='<?php echo base_url(); ?>/public/images/ajax-loader-blue.GIF' alt='procesando...' />");
            $.ajax({
                url: "<?= base_url(route_to('horas_dia_delete')) ?>",
                method: "post",
                data: {
                    id: id
                },
                dataType: "json",
                success: function(response) {
                    $("#text_message").html("");
                    swal({
                        title: response.titulo,
                        text: response.mensaje,
                        type: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    showHorasClaseAsociadas(id_dia_semana);
                },
                error: function(jqXHR, textStatus) {
                    alert(jqXHR.responseText);
                }
            });
        });

        $("#form_id").submit(function(e) {
            e.preventDefault();

            //Insertar Asociación de Día de la Semana con Hora Clase
            let id_dia_semana = document.getElementById("id_dia_semana").value;
            let id_hora_clase = document.getElementById("id_hora_clase").value;

            if (id_dia_semana !== "" && id_hora_clase !== "") {
                $("#text_message").html("<img src='<?php echo base_url(); ?>/public/images/ajax-loader-blue.GIF' alt='procesando...' />");

                $.ajax({
                    url: "<?= base_url(route_to('horas_dia_store')) ?>",
                    method: "post",
                    data: {
                        id_dia_semana: id_dia_semana,
                        id_hora_clase: id_hora_clase
                    },
                    dataType: "json",
                    success: function(response) {
                        $("#text_message").html("");
                        swal({
                            title: response.titulo,
                            text: response.mensaje,
                            type: response.tipo_mensaje,
                            confirmButtonText: 'Aceptar'
                        });
                        showHorasClaseAsociadas(id_dia_semana);
                    },
                    error: function(jqXHR, textStatus) {
                        alert(jqXHR.responseText);
                    }
                });
            } // fin if (id_dia_semana !== "" && id_hora_clase !== "")
        });
    });

    function showHorasClaseAsociadas(id_dia_semana) {
        $.each($('label'), function(key, value) {
            $(this).closest('.form-group').removeClass('has-error');
        });
        
        var request = $.ajax({
            url: "<?= base_url(route_to('horas_dia_getByDiaSemanaId')) ?>",
            method: "post",
            data: {
                id_dia_semana: id_dia_semana
            },
            dataType: "json"
        });

        request.done(function(data) {
            var html = '';
            if (data.length > 0) {
                for (let i = 0; i < data.length; i++) {
                    html += '<tr>' +
                        '<td>' + data[i].id_hora_dia + '</td>' +
                        '<td>' + data[i].ds_nombre + '</td>' +
                        '<td>' + data[i].hc_nombre + ' (' + data[i].hc_hora_inicio + ' - ' + data[i].hc_hora_fin + ')' + '</td>' +
                        '<td>' +
                        '<div class="btn-group">' +
                        '<a href="javascript:;" class="btn btn-danger btn-sm item-delete" data="' + data[i].id_hora_dia +
                        '" title="Eliminar"><span class="fa fa-remove"></span></a>' +
                        '</div>' +
                        '</td>' +
                        '</tr>';
                }
                $("#lista_items").html(html);
                $("#total_horas").val(data.length);
            } else {
                $("#lista_items").html("<tr><td colspan='4' align='center'>No se han asociado horas clase a este día de la semana...</td></tr>");
                $("#total_horas").val(0);
            }
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requerimiento fallido: " + jqXHR.responseText);
        });
    }
</script>
<?= $this->endsection('scripts') ?>