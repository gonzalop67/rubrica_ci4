<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Días de la semana
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Días de la semana</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <form id="form_id" action="" method="POST" class="form-horizontal">
                <input type="hidden" name="id_dia_semana" id="id_dia_semana">
                <div class="form-group">
                    <label for="ds_nombre" class="col-sm-2 control-label">Nombre:</label>
                    <div class="col-sm-10">
                        <input type="text" name="ds_nombre" id="ds_nombre" value="" class="form-control" style="text-transform:uppercase">
                        <span id="mensaje1" class="help-block" style="color: #e73d4a"></span>
                    </div>
                </div>
                <div class="row" id="botones_insercion">
                    <div class="col-sm-12">
                        <button id="btn-add-item" type="button" class="btn btn-block btn-primary" onclick="insertarDiaSemana()">
                            Añadir
                        </button>
                    </div>
                </div>
                <div class="row" style="display: none;" id="botones_edicion">
                    <div class="col-sm-6">
                        <button id="btn-cancel" type="button" class="btn btn-block" onclick="cancelarEdicion()">
                            Cancelar
                        </button>
                    </div>
                    <div class="col-sm-6">
                        <button id="btn-update" type="button" class="btn btn-block btn-primary" onclick="actualizarDiaSemana()">
                            Actualizar
                        </button>
                    </div>
                </div>
            </form>
            <!-- Línea de división -->
            <hr>
            <!-- message -->
            <div id="text_message" class="text-center"></div>
            <!-- table -->
            <table class="table fuente10">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody id="lista_items">
                    <!-- Aqui desplegamos el contenido de la base de datos -->
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scripts') ?>
<script>
    $(document).ready(function() {
        cargarDiasSemana();

        $("#form_id").submit(function(e){
            e.preventDefault();
        });

        $('table tbody').on('click', '.item-edit', function() {
            var id_dia_semana = $(this).attr('data');
            $("#id_dia_semana").val(id_dia_semana);
            $("#botones_insercion").hide();
            $("#botones_edicion").show();
            //Recupero los datos del dia de la semana
            $.ajax({
                url: "<?= base_url(route_to('dias_semana_getById')) ?>",
                method: "POST",
                type: "html",
                data: {
                    id_dia_semana: id_dia_semana
                },
                success: function(response) {
                    var dia_semana = jQuery.parseJSON(response);
                    $("#ds_nombre").val(dia_semana.ds_nombre);
                    $("#text_message").html("");
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
        });

        $('#lista_items').on('click', '.item-delete', function(e) {
            e.preventDefault();
            let id = $(this).attr('data');
            $("#text_message").html("<img src='<?php echo base_url(); ?>/public/images/ajax-loader-blue.GIF' alt='procesando...' />");
            $.ajax({
                url: "<?= base_url(route_to('dias_semana_delete')) ?>",
                method: "post",
                data: {
                    id: id
                },
                dataType: "json",
                success: function(response) {
                    $("#text_message").html("");
                    swal({
                        title: response.titulo,
                        text: response.mensaje,
                        type: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    cargarDiasSemana();
                },
                error: function(jqXHR, textStatus) {
                    alert(jqXHR.responseText);
                }
            });
        });

        $('table tbody').sortable({
            update: function(event, ui) {
                $(this).children().each(function(index) {
                    if ($(this).attr('data-orden') != (index + 1)) {
                        $(this).attr('data-orden', (index + 1)).addClass('updated');
                    }
                });
                saveNewPositions();
            }
        });
    });

    function cancelarEdicion() {
        $("#botones_edicion").hide();
        $("#botones_insercion").show();
        $("#form_id")[0].reset();
    }

    function cargarDiasSemana() {
        var request = $.ajax({
            url: "<?= base_url(route_to('dias_semana_listar')) ?>",
            method: "get",
            dataType: "html"
        });

        request.done(function(data) {
            $("#lista_items").html(data);
        });

        request.fail(function(jqXHR, textStatus) {
            alert("Requerimiento fallido: " + jqXHR.responseText);
        });
    }

    function insertarDiaSemana() {
        // Procedimiento para insertar un dia de la semana
        var nombre = $('#ds_nombre').val();
        var cont_errores = 0;

        if (nombre.trim() == "") {
            $("#mensaje1").html("Debes ingresar el nombre del dia de la semana...");
            $("#mensaje1").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje1").fadeOut("slow");
        }

        if (cont_errores == 0) {
            $.ajax({
                url: "<?= base_url(route_to('dias_semana_store')) ?>",
                method: "POST",
                data: {
                    ds_nombre: nombre
                },
                dataType: "json",
                success: function(response) {
                    swal({
                        title: response.titulo,
                        text: response.mensaje,
                        type: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    cargarDiasSemana();
                    $("#form_id")[0].reset();
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
        }
    }

    function actualizarDiaSemana() {
        // Recolección de datos
        var cont_errores = 0;
        var id_dia_semana = $("#id_dia_semana").val();
        var nombre = $("#ds_nombre").val();

        // Validación de ingreso de datos
        if (nombre.trim() == "") {
            $("#mensaje1").html("Debe ingresar el nombre del dia de la semana...");
            $("#mensaje1").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje1").fadeOut("slow");
        }

        if (cont_errores == 0) {
            // Se procede a la actualización del dia de la semana
            $.ajax({
                url: "<?= base_url(route_to('dias_semana_update')) ?>",
                method: "POST",
                dataType: "json",
                data: {
                    id_dia_semana: id_dia_semana,
                    ds_nombre: nombre
                },
                success: function(response) {
                    swal({
                        title: response.titulo,
                        text: response.mensaje,
                        type: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    cargarDiasSemana();
                    cancelarEdicion();
                    $("#text_message").html("");
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
        }
    }

    function saveNewPositions() {
        var positions = [];
        $('.updated').each(function() {
            positions.push([$(this).attr('data-index'), $(this).attr('data-orden')]);
            $(this).removeClass('updated');
        });

        $.ajax({
            url: "<?= base_url(route_to('dias_semana_saveNewPositions')) ?>",
            method: 'POST',
            dataType: 'text',
            data: {
                positions: positions
            },
            success: function(response) {
                cargarDiasSemana();
            }
        });
    }
</script>
<?= $this->endsection('scripts') ?>