<?= $this->extend('layouts/layout') ?>

<?= $this->section('titulo') ?>
Horas Clase
<?= $this->endsection('titulo') ?>

<?= $this->section('contenido') ?>
<!-- Main content -->
<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Horas Clase</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <form id="form_id" action="" method="POST" class="app-form">
                <input type="hidden" name="id_hora_clase" id="id_hora_clase">
                <div class="row">
                    <div class="col-sm-2 text-right">
                        <label class="control-label" style="position:relative; top:7px;">Nombre:</label>
                    </div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control fuente9" id="hc_nombre" value="" autofocus>
                        <span style="color: #e73d4a" id="mensaje1"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2 text-right">
                        <label class="control-label" style="position:relative; top:7px;">Hora de Inicio:</label>
                    </div>
                    <div class="col-sm-4" style="margin-top: 3px;">
                        <input type="text" class="form-control fuente9" placeholder="formato hh:mm" id="hc_hora_inicio" value="">
                        <span style="color: #e73d4a" id="mensaje2"></span>
                    </div>
                    <div class="col-sm-2 text-right">
                        <label class="control-label" style="position:relative; top:7px;">Hora de Fin:</label>
                    </div>
                    <div class="col-sm-4" style="margin-top: 3px;">
                        <input type="text" class="form-control fuente9" placeholder="formato hh:mm" id="hc_hora_fin" value="">
                        <span style="color: #e73d4a" id="mensaje3"></span>
                    </div>
                </div>
                <div class="row" id="botones_insercion">
                    <div class="col-sm-12" style="margin-top: 4px;">
                        <button id="btn-add-item" type="button" class="btn btn-block btn-primary" onclick="insertarHoraClase()">
                            Añadir
                        </button>
                    </div>
                </div>
                <div class="row" style="margin-top: 4px; display: none;" id="botones_edicion">
                    <div class="col-sm-6">
                        <button id="btn-cancel" type="button" class="btn btn-block" onclick="cancelarEdicion()">
                            Cancelar
                        </button>
                    </div>
                    <div class="col-sm-6">
                        <button id="btn-update" type="button" class="btn btn-block btn-primary" onclick="actualizarHoraClase()">
                            Actualizar
                        </button>
                    </div>
                </div>
            </form>
            <!-- Línea de división -->
            <hr>
            <!-- message -->
            <div id="text_message" class="text-center"></div>
            <!-- table -->
            <table class="table fuente10">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Hora de Inicio</th>
                        <th>Hora de Fin</th>
                        <th>Ordinal</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody id="lista_items">
                    <!-- Aqui desplegamos el contenido de la base de datos -->
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<?= $this->endsection('contenido') ?>

<?= $this->section('scripts') ?>
<script>
    $(document).ready(function() {
        listarHorasClase();

        $("#form_id").submit(function(e) {
            e.preventDefault();
        });

        $('input').on('focus', function() {
            $(this).select();
        });

        $('table tbody').on('click', '.item-edit', function() {
            var id_hora_clase = $(this).attr('data');
            $("#id_hora_clase").val(id_hora_clase);
            $("#botones_insercion").hide();
            $("#botones_edicion").show();
            //Recupero los datos de la hora clase
            $.ajax({
                url: "<?= base_url(route_to('horas_clase_getById')) ?>",
                method: "POST",
                type: "html",
                data: {
                    id_hora_clase: id_hora_clase
                },
                success: function(response) {
                    var hora_clase = jQuery.parseJSON(response);
                    $("#hc_nombre").val(hora_clase.hc_nombre);
                    $("#hc_hora_inicio").val(hora_clase.hora_inicio);
                    $("#hc_hora_fin").val(hora_clase.hora_fin);
                    $("#text_message").html("");
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
        });

        $('#lista_items').on('click', '.item-delete', function(e) {
            e.preventDefault();
            let id = $(this).attr('data');
            $("#text_message").html("<img src='<?php echo base_url(); ?>/public/images/ajax-loader-blue.GIF' alt='procesando...' />");
            $.ajax({
                url: "<?= base_url(route_to('horas_clase_delete')) ?>",
                method: "post",
                data: {
                    id: id
                },
                dataType: "json",
                success: function(response) {
                    $("#text_message").html("");
                    swal({
                        title: response.titulo,
                        text: response.mensaje,
                        type: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    listarHorasClase();
                },
                error: function(jqXHR, textStatus) {
                    alert(jqXHR.responseText);
                }
            });
        });

        $('table tbody').sortable({
            update: function(event, ui) {
                $(this).children().each(function(index) {
                    if ($(this).attr('data-orden') != (index + 1)) {
                        $(this).attr('data-orden', (index + 1)).addClass('updated');
                    }
                });
                saveNewPositions();
            }
        });
    });

    function cancelarEdicion() {
        $("#botones_edicion").hide();
        $("#botones_insercion").show();
        $("#form_id")[0].reset();
    }

    function listarHorasClase() {
        var request = $.ajax({
            url: "<?= base_url(route_to('horas_clase_listar')) ?>",
            method: "get",
            dataType: "html"
        });

        request.done(function(data) {
            $("#lista_items").html(data);
        });

        request.fail(function(jqXHR, textStatus) {
            //console.log(jqXHR.responseText);
            alert("Requerimiento fallido: " + jqXHR.responseText);
        });
    }

    function insertarHoraClase() {
        // Procedimiento para insertar horas clase
        // Recolección de datos
        var cont_errores = 0;
        var nombre = $("#hc_nombre").val();
        var hora_inicio = $("#hc_hora_inicio").val();
        var hora_fin = $("#hc_hora_fin").val();

        // Expresiones regulares para la validación de datos
        var reg_hora = /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/i;

        // Validación de ingreso de datos
        if (nombre.trim() == "") {
            $("#mensaje1").html("Debe ingresar el nombre de la hora clase.");
            $("#mensaje1").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje1").fadeOut("slow");
        }

        if (hora_inicio.trim() == "") {
            $("#mensaje2").html("Debe ingresar la hora de inicio de la hora clase.");
            $("#mensaje2").fadeIn("slow");
            cont_errores++;
        } else if (!reg_hora.test(hora_inicio)) {
            $("#mensaje2").html("Debe ingresar la hora en el formato hh:mm de 24 horas.");
            $("#mensaje2").fadeIn("slow");
            cont_errores++;
        } else {
            $("#mensaje2").fadeOut();
        }

        if (hora_fin.trim() == "") {
            $("#mensaje3").html("Debe ingresar la hora de fin de la hora clase.");
            $("#mensaje3").fadeIn("slow");
            cont_errores++;
        } else if (!reg_hora.test(hora_fin)) {
            $("#mensaje3").html("Debe ingresar la hora en el formato hh:mm de 24 horas.");
            $("#mensaje3").fadeIn("slow");
            cont_errores++;
        } else {
            $("#mensaje3").fadeOut();
        }

        if (cont_errores == 0) {
            $.ajax({
                url: "<?= base_url(route_to('horas_clase_store')) ?>",
                method: "POST",
                data: {
                    hc_nombre: nombre,
                    hc_hora_inicio: hora_inicio,
                    hc_hora_fin: hora_fin
                },
                dataType: "json",
                success: function(response) {
                    swal({
                        title: response.titulo,
                        text: response.mensaje,
                        type: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    listarHorasClase();
                    $("#form_id")[0].reset();
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
        }
    }

    function actualizarHoraClase() {
        // Recolección de datos
        var cont_errores = 0;
        var id_hora_clase = $("#id_hora_clase").val();
        var nombre = $("#hc_nombre").val();
        var hora_inicio = $("#hc_hora_inicio").val();
        var hora_fin = $("#hc_hora_fin").val();

        // Expresiones regulares para la validación de datos
        var reg_hora = /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/i;

        // Validación de ingreso de datos
        if (nombre.trim() == "") {
            $("#mensaje1").html("Debe ingresar el nombre de la hora clase.");
            $("#mensaje1").fadeIn();
            cont_errores++;
        } else {
            $("#mensaje1").fadeOut("slow");
        } 
        
        if(hora_inicio.trim()==""){
            $("#mensaje2").html("Debe ingresar la hora de inicio de la hora clase.");
            $("#mensaje2").fadeIn("slow");
            cont_errores++;
        }else if(!reg_hora.test(hora_inicio)){
            $("#mensaje2").html("Debe ingresar la hora en el formato hh:mm de 24 horas.");
            $("#mensaje2").fadeIn("slow");
            cont_errores++;
        }else {
            $("#mensaje2").fadeOut();
        }

        if(hora_fin.trim()==""){
            $("#mensaje3").html("Debe ingresar la hora de fin de la hora clase.");
            $("#mensaje3").fadeIn("slow");
            cont_errores++;
        }else if(!reg_hora.test(hora_fin)){
            $("#mensaje3").html("Debe ingresar la hora en el formato hh:mm de 24 horas.");
            $("#mensaje3").fadeIn("slow");
            cont_errores++;
        }else {
            $("#mensaje3").fadeOut();
        }

        if (cont_errores == 0) {
            // Se procede a la actualización de la hora clase
            $.ajax({
                url: "<?= base_url(route_to('horas_clase_update')) ?>",
                method: "POST",
                dataType: "json",
                data: {
                    id_hora_clase: id_hora_clase,
                    hc_nombre: nombre,
                    hc_hora_inicio: hora_inicio,
                    hc_hora_fin: hora_fin
                },
                success: function(response) {
                    swal({
                        title: response.titulo,
                        text: response.mensaje,
                        type: response.tipo_mensaje,
                        confirmButtonText: 'Aceptar'
                    });
                    listarHorasClase();
                    cancelarEdicion();
                    $("#text_message").html("");
                },
                error: function(xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
        }
    }

    function saveNewPositions() {
        var positions = [];
        $('.updated').each(function() {
            positions.push([$(this).attr('data-index'), $(this).attr('data-orden')]);
            $(this).removeClass('updated');
        });

        $.ajax({
            url: "<?= base_url(route_to('horas_clase_saveNewPositions')) ?>",
            method: 'POST',
            dataType: 'text',
            data: {
                positions: positions
            },
            success: function(response) {
                listarHorasClase();
            }
        });
    }
</script>
<?= $this->endsection('scripts') ?>