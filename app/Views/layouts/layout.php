<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIAE Web | <?= $this->renderSection('titulo') ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/public/bootstrap/css/bootstrap.css">
    <!-- jquery-ui -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/public/jquery-ui/jquery-ui.css">
    <!-- Sweetalert -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/public/sweetalert/dist/sweetalert.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/public/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/public/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/public/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/public/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/public/dist/css/skins/skin-blue.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/public/select2/select2.min.css">
    <!-- jQuery 3 -->
    <script src="<?php echo base_url(); ?>/public/js/jquery-3.5.1.min.js"></script>
    <?= $this->renderSection('styles') ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Inicio Header -->
        <?= $this->include('layouts/header') ?>
        <!-- Fin Header -->
        <!-- Inicio Aside -->
        <?= $this->include('layouts/aside') ?>
        <!-- Fin Aside -->
        <div class="content-wrapper">
            <section class="content">
                <?= $this->renderSection('contenido') ?>
            </section>
        </div>
        <!-- /.content-wrapper -->
        <!-- Inicio Footer -->
        <?= $this->include('layouts/footer') ?>
        <!-- Fin Footer -->
    </div>
    <!-- ./wrapper -->

    <!-- Alertify -->
    <script src="<?php echo base_url(); ?>/public/alertify/lib/alertify.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url(); ?>/public/bootstrap/js/bootstrap.min.js"></script>
    <!-- Sweetalert -->
    <script src="<?php echo base_url(); ?>/public/sweetalert/dist/sweetalert.js"></script>
    <!-- jquery-ui -->
    <script src="<?php echo base_url(); ?>/public/jquery-ui/jquery-ui.js"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url(); ?>/public/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>/public/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>/public/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url(); ?>/public/dist/js/demo.js"></script>
    <!-- plotly -->
    <!-- <script src="<?php echo base_url(); ?>js/plotly-latest.min.js"></script> -->

    <?= $this->renderSection('scriptsPlugins') ?>

    <script>
        $(document).ready(function() {
            $('.sidebar-menu').tree();
        });
    </script>

    <script src="<?php echo base_url(); ?>/public/js/scripts.js"></script>

    <?= $this->renderSection('scripts') ?>

</body>

</html>