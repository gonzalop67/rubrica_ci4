<?php

namespace App\Models;

use CodeIgniter\Model;

class Escalas_calificaciones_model extends Model
{

    protected $table      = 'sw_escala_calificaciones';
    protected $primaryKey = 'id_escala_calificaciones';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_periodo_lectivo', 'ec_cualitativa', 'ec_cuantitativa', 'ec_nota_minima', 'ec_nota_maxima', 'ec_orden', 'ec_equivalencia'];

    public function getMaxId($id_periodo_lectivo)
    {
        $maxId = $this->db->query("
            SELECT MAX(ec_orden) AS maxId
            FROM sw_escala_calificaciones
            WHERE id_periodo_lectivo = $id_periodo_lectivo
        ");
        
        $max = $maxId->getRow();
        
        return $max->maxId;
    }
}
