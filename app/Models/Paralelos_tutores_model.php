<?php

namespace App\Models;

use CodeIgniter\Model;

class Paralelos_tutores_model extends Model
{

    protected $table      = 'sw_paralelo_tutor';
    protected $primaryKey = 'id_paralelo_tutor';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_periodo_lectivo', 'id_paralelo', 'id_usuario'];

    public function listarParalelosTutores($id_periodo_lectivo)
    {
        $query = $this->db->query("
            SELECT id_paralelo_tutor,
                   cu_nombre, 
                   es_figura, 
                   pa_nombre, 
                   us_titulo, 
                   us_fullname 
              FROM sw_paralelo_tutor pt,
                   sw_paralelo p, 
                   sw_curso c, 
                   sw_especialidad e, 
                   sw_usuario u 
             WHERE p.id_paralelo = pt.id_paralelo 
               AND p.id_curso = c.id_curso 
               AND e.id_especialidad = c.id_especialidad 
               AND pt.id_usuario = u.id_usuario 
               AND pt.id_periodo_lectivo = $id_periodo_lectivo 
             ORDER BY c.id_especialidad, c.id_curso, pa_nombre
        ");

        return $query->getResultObject();

    }

    public function existeAsociacion($id_paralelo, $id_usuario)
    {
        $paralelo_tutor = $this->db->query("
            SELECT * 
            FROM sw_paralelo_tutor 
            WHERE id_paralelo = $id_paralelo
            AND id_usuario = $id_usuario 
        ");

        $num_rows = count($paralelo_tutor->getResultObject());

        return $num_rows > 0;
    }

    public function existeAsociacionParaleloTutor($id_paralelo)
    {
        $paralelo_tutor = $this->db->query("
            SELECT * 
            FROM sw_paralelo_tutor 
            WHERE id_paralelo = $id_paralelo
        ");

        $num_rows = count($paralelo_tutor->getResultObject());

        return $num_rows > 0;
    }

}
