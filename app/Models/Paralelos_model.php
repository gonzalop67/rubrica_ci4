<?php

namespace App\Models;

use CodeIgniter\Model;

class Paralelos_model extends Model
{
    protected $table      = 'sw_paralelo';
    protected $primaryKey = 'id_paralelo';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_periodo_lectivo', 'id_curso', 'id_jornada', 'pa_nombre', 'pa_orden'];

    public function actualizarOrden($id_paralelo, $pa_orden)
    {
        $this->db->query("UPDATE sw_paralelo SET pa_orden = $pa_orden WHERE id_paralelo = $id_paralelo");
    }

    public function getCursoId($id_paralelo)
    {
        $CursoId = $this->db->query("
            SELECT id_curso 
            FROM sw_paralelo
            WHERE id_paralelo = $id_paralelo
        ");

        $cursoId = $CursoId->getRow();

        return $cursoId->id_curso;
    }

    public function getAll($id_periodo_lectivo)
    {
        $sql = "
            SELECT p.*, 
                   cu_nombre, 
                   es_figura, 
                   jo_nombre 
              FROM sw_paralelo p,
                   sw_curso c, 
                   sw_especialidad e, 
                   sw_jornada j  
             WHERE c.id_curso = p.id_curso 
               AND e.id_especialidad = c.id_especialidad 
               AND j.id_jornada = p.id_jornada 
               AND p.id_periodo_lectivo = $id_periodo_lectivo 
             ORDER BY pa_orden              
        ";
        $query = $this->db->query($sql);
        $num_rows = count($query->getResultObject());
        $cadena = "";
        if ($num_rows > 0) {
            foreach ($query->getResult() as $row) {
                $cadena .= "<option value='" . $row->id_paralelo . "'>" .
                    "[" . $row->es_figura . "] " . $row->cu_nombre . " " . $row->pa_nombre . " (" . $row->jo_nombre . ")" . "</option>";
            }
        }
        return $cadena;
    }

    public function existeParaleloPeriodoLectivo($pa_nombre, $id_curso, $id_periodo_lectivo)
    {
        $query = $this->db->query("
            SELECT * FROM sw_paralelo WHERE pa_nombre = '$pa_nombre' AND id_curso = $id_curso AND id_periodo_lectivo = $id_periodo_lectivo
        ");

        $num_rows = count($query->getResultObject());

        return $num_rows > 0;
    }

    public function listarParalelos($id_periodo_lectivo)
    {
        $query = $this->db->query("
            SELECT p.*, 
                   cu_nombre, 
                   es_figura, 
                   jo_nombre 
              FROM sw_paralelo p,
                   sw_curso c, 
                   sw_especialidad e, 
                   sw_jornada j  
             WHERE c.id_curso = p.id_curso 
               AND e.id_especialidad = c.id_especialidad 
               AND j.id_jornada = p.id_jornada 
               AND p.id_periodo_lectivo = $id_periodo_lectivo 
             ORDER BY pa_orden              
        ");

        $num_rows = count($query->getResultObject());

        $cadena = "";

        if ($num_rows > 0) {
            $contador = 0;
            foreach ($query->getResult() as $row) {
                $contador++;
                $id = $row->id_paralelo;
                $cadena .= "<tr data-index='$id' data-orden='" . $row->pa_orden . "'>\n";
                $cadena .= "<td>" . $contador . "</td>\n";
                $cadena .= "<td>" . $id . "</td>\n";
                $cadena .= "<td>" . $row->es_figura . "</td>\n";
                $cadena .= "<td>" . $row->cu_nombre . "</td>\n";
                $cadena .= "<td>" . $row->pa_nombre . "</td>\n";
                $cadena .= "<td>" . $row->jo_nombre . "</td>\n";
                $cadena .= "<td>\n";
                $cadena .= "<div class=\"btn-group\">\n";
                $cadena .= "<a href=\"javascript:;\" class=\"btn btn-warning btn-sm item-edit\" data=\"$id\" title=\"Editar\"><span class=\"fa fa-pencil\"></span></a>\n";
                $cadena .= "<a href=\"javascript:;\" class=\"btn btn-danger btn-sm item-delete\" data=\"$id\" title=\"Eliminar\"><span class=\"fa fa-remove\"></span></a>\n";
                $cadena .= "</div>\n";
                $cadena .= "</td>\n";
                $cadena .= "</tr>\n";
            }
        } else {
            $cadena .= "<tr>\n";
            $cadena .= "<td colspan='8' align='center'>No se han creado paralelos todavía...</td>\n";
            $cadena .= "</tr>\n";
        }

        return $cadena;
    }

    public function getParalelosById($id_paralelo)
    {
        $paralelos = $this->db->query("
            SELECT id_paralelo, 
                   p.id_curso, 
                   es_figura, 
                   cu_nombre, 
                   pa_nombre, 
                   id_jornada 
              FROM sw_paralelo p, 
                   sw_curso c, 
                   sw_especialidad e 
             WHERE c.id_curso = p.id_curso 
               AND e.id_especialidad = c.id_especialidad 
               AND id_paralelo =  $id_paralelo
        ");

        return $paralelos->getRow();
    }

    public function crearCierresPeriodoLectivo($id_periodo_lectivo)
    {
        $sql = "CALL sp_crear_cierres_periodo_lectivo(?)";
        $result = $this->db->query($sql, [$id_periodo_lectivo]);

        if ($result) {
            return true;
        }

        return false;
    }

    public function eliminarCierresParaleloPeriodoLectivo($id_paralelo, $id_periodo_lectivo)
    {
        $sql = "CALL sp_eliminar_cierres_paralelo_periodo_lectivo(?, ?)";
        $result = $this->db->query($sql, [$id_paralelo, $id_periodo_lectivo]);

        if ($result) {
            return true;
        }

        return false;
    }

    public function getNivelEducacionByIdParalelo($id_paralelo)
    {
        $paralelos = $this->db->query("
            SELECT te_nombre, 
                   te_bachillerato 
              FROM sw_tipo_educacion t, 
                   sw_especialidad e, 
                   sw_curso c, 
                   sw_paralelo p 
             WHERE t.id_tipo_educacion = e.id_tipo_educacion 
               AND e.id_especialidad = c.id_especialidad 
               AND c.id_curso = p.id_curso 
               AND id_paralelo = $id_paralelo
        ");

        return $paralelos->getRow();
    }

    public function getJornadaByIdParalelo($id_paralelo)
    {
        $paralelos = $this->db->query("
            SELECT jo_nombre, 
                   cu_nombre,
                   pa_nombre
              FROM sw_jornada j, 
                   sw_curso c, 
                   sw_paralelo p
             WHERE c.id_curso = p.id_curso 
               AND j.id_jornada = p.id_jornada 
               AND id_paralelo = $id_paralelo
        ");

        return $paralelos->getRow();
    }
}
