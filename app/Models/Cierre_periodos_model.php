<?php

namespace App\Models;

use CodeIgniter\Model;

class Cierre_periodos_model extends Model
{
    protected $table      = 'sw_aporte_paralelo_cierre';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id_aporte_paralelo_cierre';
    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_aporte_evaluacion', 'id_paralelo', 'ap_fecha_apertura', 'ap_fecha_cierre', 'ap_estado'];

    public function getCierresPeriodosById($id_aporte_paralelo_cierre)
    {
        $cierre_periodo = $this->db->query("
            SELECT * 
              FROM sw_aporte_paralelo_cierre
             WHERE id_aporte_paralelo_cierre =  $id_aporte_paralelo_cierre
        ");

        return $cierre_periodo->getRow();
    }

    public function getCierresPeriodos()
    {
        $query = $this->db->query("
            SELECT ac.*, 
                   ap_nombre, 
                   cu_nombre,
                   pa_nombre,
                   es_figura,
                   jo_nombre 
              FROM sw_aporte_paralelo_cierre ac,
                   sw_aporte_evaluacion ap,
                   sw_periodo_evaluacion pe,
                   sw_tipo_aporte ta, 
                   sw_paralelo pa,
                   sw_curso cu,
                   sw_especialidad es,
                   sw_jornada jo 
             WHERE ap.id_aporte_evaluacion = ac.id_aporte_evaluacion 
               AND pe.id_periodo_evaluacion = ap.id_periodo_evaluacion
               AND ta.id_tipo_aporte = ap.id_tipo_aporte
               AND pa.id_paralelo = ac.id_paralelo
               AND cu.id_curso = pa.id_curso
               AND es.id_especialidad = cu.id_especialidad
               AND jo.id_jornada = pa.id_jornada 
             ORDER BY pa_orden, 
                      pe.id_periodo_evaluacion, 
                      ta.id_tipo_aporte, 
                      ap.id_aporte_evaluacion
        ");

        return $query->getResultObject();
    }

    public function buscarCierresPeriodos($patron)
    {
        $query = $this->db->query("
            SELECT ac.*, 
                   ap_nombre, 
                   cu_nombre,
                   pa_nombre,
                   es_figura,
                   jo_nombre 
              FROM sw_aporte_paralelo_cierre ac,
                   sw_aporte_evaluacion ap,
                   sw_periodo_evaluacion pe,
                   sw_tipo_aporte ta, 
                   sw_paralelo pa,
                   sw_curso cu,
                   sw_especialidad es,
                   sw_jornada jo 
             WHERE ap.id_aporte_evaluacion = ac.id_aporte_evaluacion 
               AND pe.id_periodo_evaluacion = ap.id_periodo_evaluacion
               AND ta.id_tipo_aporte = ap.id_tipo_aporte
               AND pa.id_paralelo = ac.id_paralelo
               AND cu.id_curso = pa.id_curso
               AND es.id_especialidad = cu.id_especialidad
               AND jo.id_jornada = pa.id_jornada 
               AND (ap.ap_nombre LIKE '%" . $patron . "%'
                    OR cu_nombre LIKE '%" . $patron . "%'
                    OR es_figura LIKE '%" . $patron . "%')
             ORDER BY pa_orden, 
                      pe.id_periodo_evaluacion, 
                      ta.id_tipo_aporte, 
                      ap.id_aporte_evaluacion
        ");

        return $query->getResultObject();
    }

    public function existeCierrePeriodo($id_aporte_evaluacion, $id_paralelo)
    {
        $cierre_periodo = $this->db->query("
            SELECT * 
            FROM sw_aporte_paralelo_cierre 
            WHERE id_aporte_evaluacion = $id_aporte_evaluacion
            AND id_paralelo = $id_paralelo 
        ");

        $num_rows = count($cierre_periodo->getResultObject());

        return $num_rows > 0;
    }
}
