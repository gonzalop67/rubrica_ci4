<?php

namespace App\Models;

use CodeIgniter\Model;

class Tipos_asignatura_model extends Model
{

    protected $table      = 'sw_tipo_asignatura';
    protected $primaryKey = 'id_tipo_asignatura';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['ta_descripcion'];

    public function getAll()
    {
        $query = $this->db->query("SELECT * FROM sw_tipo_asignatura ORDER BY id_tipo_asignatura");
        $num_rows = count($query->getResultObject());
        $cadena = "";
        if ($num_rows > 0) {
            foreach ($query->getResult() as $row) {
                $cadena .= "<option value='". $row->id_tipo_asignatura . "'>" . $row->ta_descripcion . "</option>";
            }
        }
        return $cadena;
    }

}
