<?php

namespace App\Models;

use CodeIgniter\Model;

class Asignaturas_cursos_model extends Model
{

    protected $table      = 'sw_asignatura_curso';
    protected $primaryKey = 'id_asignatura_curso';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_periodo_lectivo', 'id_curso', 'id_asignatura', 'ac_orden'];

    public function listarAsignaturasAsociadas($id_curso)
    {
        $asignaturas_cursos = $this->db->query("
            SELECT *
            FROM sw_asignatura_curso ac,
                 sw_curso c,
                 sw_especialidad e,   
                 sw_asignatura a 
            WHERE c.id_curso = ac.id_curso
              AND e.id_especialidad = c.id_especialidad 
              AND a.id_asignatura = ac.id_asignatura
              AND ac.id_curso = $id_curso 
            ORDER BY ac_orden              
        ");

        return $asignaturas_cursos->getResultObject();
    }

    public function AsignaturasAsociadasParalelo($id_paralelo)
    {
        $asignaturas_paralelo = $this->db->query("
            SELECT a.*
            FROM sw_asignatura a,
                 sw_asignatura_curso ac, 
                 sw_paralelo p 
            WHERE a.id_asignatura = ac.id_asignatura
              AND p.id_curso = ac.id_curso
              AND p.id_paralelo = $id_paralelo 
            ORDER BY ac_orden              
        ");

        return $asignaturas_paralelo->getResultObject();
    }

    public function getMaxId($id_curso)
    {
        $maxId = $this->db->query("
            SELECT MAX(ac_orden) AS maxId
            FROM sw_asignatura_curso
            WHERE id_curso = $id_curso
        ");
        
        $max = $maxId->getRow();
        
        return $max->maxId;
    }

    public function existeAsociacion($id_curso, $id_asignatura)
    {
        $asignaturas_cursos = $this->db->query("
            SELECT * 
            FROM sw_asignatura_curso 
            WHERE id_curso = $id_curso
            AND id_asignatura = $id_asignatura 
        ");

        $num_rows = count($asignaturas_cursos->getResultObject());

        return $num_rows > 0;
    }

    public function actualizarOrden($id_asignatura_curso, $ac_orden)
    {
        $this->db->query("UPDATE sw_asignatura_curso SET ac_orden = $ac_orden WHERE id_asignatura_curso = $id_asignatura_curso");
    }
}
