<?php

namespace App\Models;

use CodeIgniter\Model;

class Horas_clase_model extends Model
{
    protected $table      = 'sw_hora_clase';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id_hora_clase';
    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_periodo_lectivo', 'hc_nombre', 'hc_hora_inicio', 'hc_hora_fin', 'hc_ordinal', 'hc_tipo'];

    public function secuencialHoraClase($id_periodo_lectivo)
    {
        $query = $this->db->query("SELECT * FROM sw_hora_clase WHERE id_periodo_lectivo = $id_periodo_lectivo");

        return count($query->getResultObject());
    }

    public function getHoraClaseById($id_hora_clase)
    {
        $hora_clase = $this->db->query("
            SELECT id_hora_clase,
                   hc_nombre,
                   DATE_FORMAT(hc_hora_inicio,'%H:%i') AS hora_inicio,
                   DATE_FORMAT(hc_hora_fin,'%H:%i') AS hora_fin,
                   hc_ordinal 
              FROM sw_hora_clase
             WHERE id_hora_clase =  $id_hora_clase
        ");

        return $hora_clase->getRow();
    }

    public function getHorasClaseByPeriodoId($id_periodo_lectivo)
    {
        $query = $this->db->query("
            SELECT id_hora_clase,
                   hc_nombre,
                   DATE_FORMAT(hc_hora_inicio,'%H:%i') AS hc_hora_inicio,
                   DATE_FORMAT(hc_hora_fin,'%H:%i') AS hc_hora_fin
              FROM sw_hora_clase
             WHERE id_periodo_lectivo =  $id_periodo_lectivo
             ORDER BY hc_ordinal
        ");

        return $query->getResultObject();
    }

    public function getHoraClaseByParaleloId($id_paralelo)
    {
        $query = $this->db->query("
            SELECT DISTINCT(hc.id_hora_clase)
              FROM sw_horario ho, 
                   sw_hora_clase hc
             WHERE ho.id_hora_clase = hc.id_hora_clase
               AND id_paralelo = $id_paralelo
             ORDER BY hc_ordinal
        ");
        $cadena = "";
        foreach ($query->getResult() as $hora) {
            $cadena .= "<tr>\n";
            $id_hora_clase = $hora->id_hora_clase;
            // Consulto el nombre de la hora clase
            $consulta = $this->db->query("SELECT hc_nombre FROM sw_hora_clase WHERE id_hora_clase = $id_hora_clase");
            $hora_clase = $consulta->getRow();
            $cadena .= "<td class='text-center'><span style='font-size: 12pt'><strong>$hora_clase->hc_nombre</strong></span></td>\n";
            // Acá obtengo los dias de la semana asociados al paralelo
            $dias = $this->db->query("
                SELECT DISTINCT(d.id_dia_semana) 
                  FROM sw_horario h,
                       sw_dia_semana d
                 WHERE d.id_dia_semana = h.id_dia_semana
                   AND id_paralelo = $id_paralelo
                 ORDER BY ds_ordinal
            ");
            foreach ($dias->getResult() as $dia) {
                $id_dia_semana = $dia->id_dia_semana;
                // Consulto la asignatura del dia y hora correspondientes
                $consulta = $this->db->query("
                        SELECT a.id_asignatura,
                               as_nombre
                          FROM sw_horario ho, 
                               sw_hora_clase hc, 
                               sw_asignatura a
                         WHERE ho.id_hora_clase = hc.id_hora_clase 
                           AND ho.id_asignatura = a.id_asignatura 
                           AND ho.id_dia_semana = $id_dia_semana 
                           AND ho.id_hora_clase = $id_hora_clase 
                           AND id_paralelo = $id_paralelo
                    ");
                $asignatura = $consulta->getRow();
                if (isset($asignatura)) {
                    $id_asignatura = $asignatura->id_asignatura;
                    $cadena .= "<td>\n";
                    $cadena .= "<p>$asignatura->as_nombre</p>\n";
                    // Obtengo el docente que imparte esta asignatura
                    $query = $this->db->query("
                        SELECT us_shortname 
                          FROM sw_asignatura a, 
                               sw_distributivo d,
                               sw_usuario u
                         WHERE a.id_asignatura = d.id_asignatura
                           AND u.id_usuario = d.id_usuario
                           AND d.id_asignatura = $id_asignatura
                           AND d.id_paralelo = $id_paralelo
                    ");
                    $docente = $query->getRow();
                    $cadena .= "<p><em>$docente->us_shortname</em></p>\n";
                    $cadena .= "</td>\n";
                } else {
                    $cadena .= "<td>&nbsp;</td>\n";
                }
            }
            $cadena .= "</tr>\n";
        }
        return $cadena;
    }

    public function getHorasClase($id_periodo_lectivo)
    {
        $query = $this->db->query("
            SELECT * 
              FROM sw_hora_clase 
             WHERE id_periodo_lectivo = $id_periodo_lectivo
             ORDER BY hc_ordinal
        ");
        $num_rows = count($query->getResultObject());
        $cadena = "";
        if ($num_rows > 0) {
            foreach ($query->getResult() as $row) {
                $id = $row->id_hora_clase;
                $nombre = $row->hc_nombre;
                $orden = $row->hc_ordinal;
                $hora_inicio = $row->hc_hora_inicio;
                $hora_fin = $row->hc_hora_fin;
                $cadena .= "<tr data-index='$id' data-orden='$orden'>\n";
                $cadena .= "<td>$id</td>\n";
                $cadena .= "<td>$nombre</td>\n";
                $cadena .= "<td>$hora_inicio</td>\n";
                $cadena .= "<td>$hora_fin</td>\n";
                $cadena .= "<td>$orden</td>\n";
                $cadena .= "<td>\n";
                $cadena .= "<div class=\"btn-group\">\n";
                $cadena .= "<a href=\"javascript:;\" class=\"btn btn-warning btn-sm item-edit\" data=\"$id\" title=\"Editar\"><span class=\"fa fa-pencil\"></span></a>\n";
                $cadena .= "<a href=\"javascript:;\" class=\"btn btn-danger btn-sm item-delete\" data=\"$id\" title=\"Eliminar\"><span class=\"fa fa-remove\"></span></a>\n";
                $cadena .= "</div>\n";
                $cadena .= "</td>\n";
                $cadena .= "</tr>\n";
            }
        } else {
            $cadena .= "<tr>\n";
            $cadena .= "<td colspan='6' align='center'>No se han definido horas clase para este periodo lectivo...</td>\n";
            $cadena .= "</tr>\n";
        }
        return $cadena;
    }

    public function existeNombreHoraClase($hc_nombre, $id_periodo_lectivo)
    {
        $query = $this->db->query("SELECT * FROM sw_hora_clase WHERE hc_nombre = '$hc_nombre' AND id_periodo_lectivo = $id_periodo_lectivo");

        return count($query->getResultObject()) > 0;
    }

    public function actualizarOrden($id_hora_clase, $hc_ordinal)
    {
        $this->db->query("UPDATE sw_hora_clase SET hc_ordinal = $hc_ordinal WHERE id_hora_clase = $id_hora_clase");
    }
}
