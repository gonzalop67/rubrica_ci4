<?php

namespace App\Models;

use CodeIgniter\Model;

class Paralelos_inspectores_model extends Model
{

    protected $table      = 'sw_paralelo_inspector';
    protected $primaryKey = 'id_paralelo_inspector';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_periodo_lectivo', 'id_paralelo', 'id_usuario'];

    public function listarParalelosInspectores($id_periodo_lectivo)
    {
        $query = $this->db->query("
            SELECT id_paralelo_inspector,
                   cu_nombre, 
                   es_figura, 
                   pa_nombre, 
                   us_titulo, 
                   us_fullname 
              FROM sw_paralelo_inspector pi,
                   sw_paralelo p, 
                   sw_curso c, 
                   sw_especialidad e, 
                   sw_usuario u 
             WHERE p.id_paralelo = pi.id_paralelo 
               AND p.id_curso = c.id_curso 
               AND e.id_especialidad = c.id_especialidad 
               AND pi.id_usuario = u.id_usuario 
               AND pi.id_periodo_lectivo = $id_periodo_lectivo 
             ORDER BY c.id_especialidad, c.id_curso, pa_nombre
        ");

        return $query->getResultObject();

    }

    public function existeAsociacion($id_paralelo, $id_usuario)
    {
        $paralelo_tutor = $this->db->query("
            SELECT * 
            FROM sw_paralelo_inspector 
            WHERE id_paralelo = $id_paralelo
            AND id_usuario = $id_usuario 
        ");

        $num_rows = count($paralelo_tutor->getResultObject());

        return $num_rows > 0;
    }

    public function existeAsociacionParaleloInspector($id_paralelo)
    {
        $paralelo_tutor = $this->db->query("
            SELECT * 
            FROM sw_paralelo_inspector 
            WHERE id_paralelo = $id_paralelo
        ");

        $num_rows = count($paralelo_tutor->getResultObject());

        return $num_rows > 0;
    }

}
