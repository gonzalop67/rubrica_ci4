<?php

namespace App\Models;

use CodeIgniter\Model;

class Dias_semana_model extends Model
{
    protected $table      = 'sw_dia_semana';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id_dia_semana';
    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_periodo_lectivo', 'ds_nombre', 'ds_ordinal'];

    public function secuencialDiaSemana($id_periodo_lectivo)
    {
        $query = $this->db->query("SELECT * FROM sw_dia_semana WHERE id_periodo_lectivo = $id_periodo_lectivo");

        return count($query->getResultObject());
    }

    public function getDiaSemanaById($id_dia_semana)
    {
        $dia_semana = $this->db->query("
            SELECT * 
              FROM sw_dia_semana
             WHERE id_dia_semana =  $id_dia_semana
        ");

        return $dia_semana->getRow();
    }

    public function getDiasSemanaByParaleloId($id_paralelo)
    {
        $query = $this->db->query("
            SELECT DISTINCT(d.id_dia_semana) 
              FROM sw_horario h,
                   sw_dia_semana d
             WHERE d.id_dia_semana = h.id_dia_semana
               AND id_paralelo = $id_paralelo
             ORDER BY ds_ordinal
        ");
        $num_rows = count($query->getResultObject());
        $cadena = "";
        if ($num_rows > 0) {
            $cadena = "<tr>\n";
            $cadena .= "<th>HORA</th>\n";
            foreach ($query->getResult() as $row) {
                $id_dia_semana = $row->id_dia_semana;
                $consulta = $this->db->query("SELECT ds_nombre FROM sw_dia_semana WHERE id_dia_semana = $id_dia_semana");
                $nombre_dia = $consulta->getRow();
                $cadena .= "<th>$nombre_dia->ds_nombre</th>\n";
            }
            $cadena .= "</tr>\n";
        } else {
            $cadena .= "<tr>\n";
            $cadena .= "<th align='center'>No se han asociado Dias de la semana a este Paralelo...</th>\n";
            $cadena .= "</tr>\n";
        }
        return $cadena;
    }

    public function getDiasSemana($id_periodo_lectivo)
    {
        $query = $this->db->query("
            SELECT * 
              FROM sw_dia_semana 
             WHERE id_periodo_lectivo = $id_periodo_lectivo
             ORDER BY ds_ordinal
        ");
        $num_rows = count($query->getResultObject());
        $cadena = "";
        if ($num_rows > 0) {
            foreach ($query->getResult() as $row) {
                $id = $row->id_dia_semana;
                $nombre = $row->ds_nombre;
                $orden = $row->ds_ordinal;
                $cadena .= "<tr data-index='$id' data-orden='$orden'>\n";
                $cadena .= "<td>$id</td>\n";
                $cadena .= "<td>$nombre</td>\n";
                $cadena .= "<td>\n";
                $cadena .= "<div class=\"btn-group\">\n";
                $cadena .= "<a href=\"javascript:;\" class=\"btn btn-warning btn-sm item-edit\" data=\"$id\" title=\"Editar\"><span class=\"fa fa-pencil\"></span></a>\n";
                $cadena .= "<a href=\"javascript:;\" class=\"btn btn-danger btn-sm item-delete\" data=\"$id\" title=\"Eliminar\"><span class=\"fa fa-remove\"></span></a>\n";
                $cadena .= "</div>\n";
                $cadena .= "</td>\n";
                $cadena .= "</tr>\n";
            }
        } else {
            $cadena .= "<tr>\n";
            $cadena .= "<td colspan='3' align='center'>No se han definido días de la semana para este periodo lectivo...</td>\n";
            $cadena .= "</tr>\n";
        }
        return $cadena;
    }

    public function existeNombreDiaSemana($ds_nombre, $id_periodo_lectivo)
    {
        $query = $this->db->query("SELECT * FROM sw_dia_semana WHERE ds_nombre = '$ds_nombre' AND id_periodo_lectivo = $id_periodo_lectivo");

        return count($query->getResultObject()) > 0;
    }

    public function actualizarOrden($id_dia_semana, $ds_ordinal)
    {
        $this->db->query("UPDATE sw_dia_semana SET ds_ordinal = $ds_ordinal WHERE id_dia_semana = $id_dia_semana");
    }
}
