<?php 
namespace App\Models;

use CodeIgniter\Model;

class Tipos_educacion_model extends Model{
    protected $table      = 'sw_tipo_educacion';
    protected $primaryKey = 'id_tipo_educacion';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_periodo_lectivo', 'te_nombre', 'te_bachillerato', 'te_orden'];

    public function listarNivelesEducacion($id_periodo_lectivo)
    {
        $nivelesEducacion = $this->db->query("
            SELECT *
              FROM sw_tipo_educacion 
             WHERE id_periodo_lectivo = $id_periodo_lectivo
             ORDER BY te_orden
            ");
        return $nivelesEducacion->getResult();
    }

    public function actualizarOrden($id_tipo_educacion, $te_orden)
    {
      $this->db->query("UPDATE sw_tipo_educacion SET te_orden = $te_orden WHERE id_tipo_educacion = $id_tipo_educacion");
    }
  
}