<?php

namespace App\Models;

use CodeIgniter\Model;

class Curso_superior_model extends Model
{

    protected $table      = 'sw_curso_superior';
    protected $primaryKey = 'id_curso_superior';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['cs_nombre'];

}