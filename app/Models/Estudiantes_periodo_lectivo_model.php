<?php

namespace App\Models;

use CodeIgniter\Model;

class Estudiantes_periodo_lectivo_model extends Model
{
    protected $table      = 'sw_estudiante_periodo_lectivo';
    protected $primaryKey = 'id_estudiante_periodo_lectivo';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_estudiante', 'id_periodo_lectivo', 'id_paralelo', 'es_estado', 'es_retirado', 'nro_matricula', 'activo'];

    public function getMaxNroMatricula($id_periodo_lectivo)
    {
        $maxNroMatricula = $this->db->query("
            SELECT MAX(nro_matricula) AS maxNroMatricula
            FROM sw_estudiante_periodo_lectivo
            WHERE id_periodo_lectivo = $id_periodo_lectivo
        ");

        if ($maxNroMatricula) {
            $max = $maxNroMatricula->getRow();
            $maxNroMatricula = $max->maxNroMatricula;
        } else {
            $maxNroMatricula = 0;
        }
        
        return $maxNroMatricula;
    }

    public function quitarEstudianteParalelo($id_estudiante, $id_paralelo)
    {
        $this->db->query("UPDATE sw_estudiante_periodo_lectivo SET activo = 0 WHERE id_estudiante = $id_estudiante AND id_paralelo = $id_paralelo");
    }
    
    public function undeleteEstudianteParalelo($id_estudiante_periodo_lectivo)
    {
        $this->db->query("UPDATE sw_estudiante_periodo_lectivo SET activo = 1 WHERE id_estudiante_periodo_lectivo = $id_estudiante_periodo_lectivo");
    }

    public function actualizarEstadoRetirado($datos)
    {
        $this->db->query("UPDATE sw_estudiante_periodo_lectivo SET es_retirado = '" . $datos['estado_retirado'] . "' WHERE id_estudiante = " . $datos['id_estudiante'] . " AND id_periodo_lectivo = " . $datos['id_periodo_lectivo']);
    }

    public function getPeriodoLectivoById($id_estudiante_periodo_lectivo)
    {
        $periodo_lectivo = $this->db->query("
            SELECT pe_anio_inicio, 
                   pe_anio_fin 
              FROM sw_periodo_lectivo pl, 
                   sw_estudiante_periodo_lectivo ep 
             WHERE pl.id_periodo_lectivo = ep.id_periodo_lectivo 
               AND id_estudiante_periodo_lectivo = $id_estudiante_periodo_lectivo
        ");

        return $periodo_lectivo->getRow();
    }
}