<?php

namespace App\Models;

use CodeIgniter\Model;

class Asignaturas_model extends Model
{

    protected $table      = 'sw_asignatura';
    protected $primaryKey = 'id_asignatura';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_area', 'id_tipo_asignatura', 'as_nombre', 'as_abreviatura', 'as_shortname', 'as_curricular'];

    public function listarAsignaturas()
    {
        $query = $this->db->query("
            SELECT a.*, 
                   ar_nombre, 
                   ta_descripcion 
              FROM sw_asignatura a,
                   sw_area ar, 
                   sw_tipo_asignatura ta  
             WHERE ar.id_area = a.id_area 
               AND ta.id_tipo_asignatura = a.id_tipo_asignatura 
             ORDER BY ar_nombre, as_nombre              
        ");

        return $query->getResultObject();
    }

    public function buscarAsignaturas($patron)
    {
        $query = $this->db->query("
            SELECT a.*, 
                   ar_nombre, 
                   ta_descripcion 
              FROM sw_asignatura a,
                   sw_area ar, 
                   sw_tipo_asignatura ta  
             WHERE ar.id_area = a.id_area 
               AND ta.id_tipo_asignatura = a.id_tipo_asignatura
               AND as_nombre LIKE '%" . $patron . "%' 
             ORDER BY ar_nombre, as_nombre              
        ");

        return $query->getResultObject();
    }

    public function getAsignaturasById($id_asignatura)
    {
        $asignatura = $this->db->query("
            SELECT id_asignatura, 
                   ar.id_area, 
                   ar_nombre, 
                   as_nombre, 
                   as_abreviatura, 
                   ta.id_tipo_asignatura 
              FROM sw_asignatura a, 
                   sw_area ar, 
                   sw_tipo_asignatura ta 
             WHERE ar.id_area = a.id_area 
               AND ta.id_tipo_asignatura = a.id_tipo_asignatura 
               AND id_asignatura =  $id_asignatura
        ");

        return $asignatura->getRow();
    }

    public function existeCampoAsignatura($nom_campo, $val_campo)
    {
        $query = $this->db->query("
            SELECT * FROM sw_asignatura WHERE $nom_campo = '$val_campo'
        ");

        $num_rows = count($query->getResultObject());

        return $num_rows > 0;
    }
}
