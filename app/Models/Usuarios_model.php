<?php

namespace App\Models;

use CodeIgniter\Model;

class Usuarios_model extends Model
{

    protected $table      = 'sw_usuario';
    protected $primaryKey = 'id_usuario';

    protected $useAutoIncrement = true;

    protected $returnType = 'object';

    protected $allowedFields = [
        'us_titulo',
        'us_apellidos',
        'us_nombres',
        'us_shortname',
        'us_fullname',
        'us_login',
        'us_password',
        'us_foto',
        'us_genero',
        'us_activo'
    ];

    public function listarUsuarios()
    {
        $query = $this->db->query("
            SELECT id_usuario, 
                   us_foto, 
                   us_shortname,
                   us_login,
                   us_activo 
              FROM sw_usuario  
             ORDER BY us_apellidos, us_nombres              
        ");

        return $query->getResultObject();
    }

    public function buscarUsuarios($patron)
    {
        $query = $this->db->query("
            SELECT a.*, 
                   ar_nombre, 
                   ta_descripcion 
              FROM sw_asignatura a,
                   sw_area ar, 
                   sw_tipo_asignatura ta  
             WHERE ar.id_area = a.id_area 
               AND ta.id_tipo_asignatura = a.id_tipo_asignatura
               AND as_nombre LIKE '%" . $patron . "%' 
             ORDER BY ar_nombre, as_nombre              
        ");

        return $query->getResultObject();
    }

    public function login($login, $clave, $id_perfil)
    {

        $usuario = $this->db->query("
            SELECT up.id_usuario,
                   SUBSTRING_INDEX(us_apellidos, ' ', 1) AS primer_apellido, 
                   SUBSTRING_INDEX(us_nombres, ' ', 1) AS primer_nombre,
                   us_foto,
                   us_genero,
                   us_password,
                   p.id_perfil,
                   pe_nombre
              FROM sw_usuario_perfil up, 
                   sw_usuario u,
                   sw_perfil p
             WHERE u.id_usuario = up.id_usuario
               AND p.id_perfil = up.id_perfil
               AND us_login = '$login'
               AND p.id_perfil = $id_perfil
            ");

        $resultado = $usuario->getResultArray();
        return $resultado;
    }

    public function getDocentesByParalelo($id_curso, $id_paralelo)
    {
        $query = $this->db->query("
            SELECT us_titulo, 
                   us_apellidos, 
                   us_nombres, 
                   as_nombre 
              FROM sw_distributivo di,
                   sw_asignatura_curso ac, 
                   sw_usuario u, 
                   sw_asignatura a 
             WHERE u.id_usuario = di.id_usuario 
               AND a.id_asignatura = di.id_asignatura
               AND ac.id_asignatura = di.id_asignatura
               AND ac.id_curso = $id_curso 
               AND id_paralelo = $id_paralelo
             ORDER BY ac_orden
        ");
        $num_rows = count($query->getResultObject());
        $cadena = "";
        if ($num_rows > 0) {
            $contador = 1;
            foreach ($query->getResult() as $row) {
                $asignatura = $row->as_nombre;
                $profesor = $row->us_titulo . " " . $row->us_apellidos . " " . $row->us_nombres;
                $cadena .= "<tr>\n";
                $cadena .= "<td>$contador</td>\n";
                $cadena .= "<td>$asignatura</td>\n";	
                $cadena .= "<td>$profesor</td>\n";
                $cadena .= "</tr>\n";
                $contador++;
            }
        } else {
            $cadena .= "<tr>\n";
        $cadena .= "<td colspan='3' align='center'>No se han asociado Docentes a este Paralelo...</td>\n";
        $cadena .= "</tr>\n";
        }
        return $cadena;
    }

}
