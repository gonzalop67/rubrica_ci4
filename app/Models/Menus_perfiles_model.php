<?php

namespace App\Models;

use CodeIgniter\Model;

class Menus_perfiles_model extends Model
{

    protected $table      = 'sw_menu_perfil';

    protected $returnType     = 'object';

    protected $allowedFields = ['id_perfil', 'id_menu'];

}
