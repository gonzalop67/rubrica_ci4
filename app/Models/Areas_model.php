<?php

namespace App\Models;

use CodeIgniter\Model;

class Areas_model extends Model
{

    protected $table      = 'sw_area';
    protected $primaryKey = 'id_area';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['ar_nombre'];

    public function getAll()
    {
        $query = $this->db->query("SELECT * FROM sw_area ORDER BY ar_nombre");
        $num_rows = count($query->getResultObject());
        $cadena = "";
        if ($num_rows > 0) {
            foreach ($query->getResult() as $row) {
                $cadena .= "<option value='". $row->id_area . "'>" . $row->ar_nombre . "</option>";
            }
        }
        return $cadena;
    }

}
