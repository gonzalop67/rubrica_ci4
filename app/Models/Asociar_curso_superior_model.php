<?php

namespace App\Models;

use CodeIgniter\Model;

class Asociar_curso_superior_model extends Model
{

    protected $table      = 'sw_asociar_curso_superior';
    protected $primaryKey = 'id_asociar_curso_superior';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_periodo_lectivo', 'id_curso_inferior', 'id_curso_superior'];

    public function listarCursosSuperiores($id_periodo_lectivo)
    {
        $query = $this->db->query("
            SELECT cs.*
              FROM sw_asociar_curso_superior cs,
                   sw_curso c, 
                   sw_especialidad e 
             WHERE c.id_curso = cs.id_curso_inferior
               AND e.id_especialidad = c.id_especialidad
               AND cs.id_periodo_lectivo = $id_periodo_lectivo 
             ORDER BY c.id_especialidad, id_curso              
        ");

        $num_rows = count($query->getResultObject());

        $cadena = "";

        if ($num_rows > 0) {
            foreach ($query->getResult() as $row) {
                $cadena .= "<tr>\n";
                $id = $row->id_asociar_curso_superior;
                $id_curso_inferior = $row->id_curso_inferior;
				$id_curso_superior = $row->id_curso_superior;
                //Consultar los nombres de los cursos inferior y superior
                $consulta = $this->db->query("
                    SELECT es_figura, 
                           cu_nombre 
                      FROM sw_curso c, 
                           sw_especialidad e, 
                           sw_tipo_educacion t 
                     WHERE c.id_especialidad = e.id_especialidad 
                       AND e.id_tipo_educacion = t.id_tipo_educacion 
                       AND c.id_curso = $id_curso_inferior
                ");
                $record = $consulta->getRow();
                $curso_inferior = "[" . $record->es_figura . "] " . $record->cu_nombre;
                $consulta = $this->db->query("
                    SELECT cs_nombre 
                      FROM sw_curso_superior 
                     WHERE id_curso_superior = $id_curso_superior
                ");
                $record = $consulta->getRow();
                $curso_superior = $record->cs_nombre;
                $cadena .= "<td>$id</td>\n";
                $cadena .= "<td>$curso_inferior</td>\n";
                $cadena .= "<td>$curso_superior</td>\n";
                $cadena .= "<td>\n";
                $cadena .= "<div class=\"btn-group\">\n";
                $cadena .= "<a href=\"javascript:;\" class=\"btn btn-danger btn-sm item-delete\" data=\"$id\" title=\"Eliminar\"><span class=\"fa fa-remove\"></span></a>\n";
                $cadena .= "</div>\n";
                $cadena .= "</td>\n";
                $cadena .= "</tr>\n";
            }
        } else {
            $cadena .= "<tr>\n";
            $cadena .= "<td colspan='4' align='center'>No se han asociado cursos superiores todavía...</td>\n";
			$cadena .= "</tr>\n";
        }

        return $cadena;
    }

    public function existeAsociacion($id_curso_inferior, $id_curso_superior)
    {
        $curso_superior = $this->db->query("
            SELECT * 
            FROM sw_asociar_curso_superior 
            WHERE id_curso_inferior = $id_curso_inferior
            AND id_curso_superior = $id_curso_superior 
        ");

        $num_rows = count($curso_superior->getResultObject());

        return $num_rows > 0;
    }

}
