<?php

namespace App\Models;

use CodeIgniter\Model;

class Horas_dia_model extends Model
{

    protected $table      = 'sw_hora_dia';
    protected $primaryKey = 'id_hora_dia';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_dia_semana', 'id_hora_clase'];

    public function existeAsociacion($id_dia_semana, $id_hora_clase)
    {
        $query = $this->db->query("SELECT * FROM sw_hora_dia WHERE id_dia_semana = $id_dia_semana AND id_hora_clase = $id_hora_clase");

        return count($query->getResultObject()) > 0;
    }

    public function getByDiaSemanaId($id_dia_semana)
    {
        $hora_dia = $this->db->query("
            SELECT id_hora_dia,
                   hc.id_hora_clase,
                   ds_nombre, 
                   hc_nombre,
                   DATE_FORMAT(hc_hora_inicio,'%H:%i') AS hc_hora_inicio,
                   DATE_FORMAT(hc_hora_fin,'%H:%i') AS hc_hora_fin
              FROM sw_hora_dia hd,
                   sw_dia_semana ds,
                   sw_hora_clase hc
             WHERE ds.id_dia_semana = hd.id_dia_semana
               AND hc.id_hora_clase = hd.id_hora_clase
               AND hd.id_dia_semana =  $id_dia_semana
             ORDER BY hc_ordinal
        ");

        return $hora_dia->getResultObject();
    }
}