<?php

namespace App\Models;

use CodeIgniter\Model;

class Institucion_model extends Model
{

    protected $table      = 'sw_institucion';
    protected $primaryKey = 'id_institucion';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['in_nombre', 'in_direccion', 'in_telefono', 'in_nom_rector', 'in_nom_vicerrector', 'in_nom_secretario', 'in_url', 'in_logo', 'in_amie', 'in_ciudad', 'in_copiar_y_pegar'];

    public function obtenerDatos()
    {
        $nombreInstitucion = $this->db->query('
            SELECT *
              FROM sw_institucion 
             WHERE id_institucion = 1
            ');

        return $nombreInstitucion->getResultObject();
    }

    public function truncateTable()
    {
        $this->db->table('sw_institucion')->truncate();
    }
}
