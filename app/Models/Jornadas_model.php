<?php

namespace App\Models;

use CodeIgniter\Model;

class Jornadas_model extends Model
{
    protected $table      = 'sw_jornada';
    protected $primaryKey = 'id_jornada';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['jo_nombre'];

    public function getAll()
    {
        $query = $this->db->query("SELECT * FROM sw_jornada ORDER BY id_jornada");
        $num_rows = count($query->getResultObject());
        $cadena = "";
        if ($num_rows > 0) {
            foreach ($query->getResult() as $row) {
                $cadena .= "<option value='". $row->id_jornada . "'>" . $row->jo_nombre . "</option>";
            }
        }
        return $cadena;
    }
}