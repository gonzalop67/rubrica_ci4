<?php

namespace App\Models;

use CodeIgniter\Model;

class Aportes_evaluacion_model extends Model
{
    protected $table      = 'sw_aporte_evaluacion';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id_aporte_evaluacion';
    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_periodo_evaluacion', 'id_tipo_aporte', 'ap_nombre', 'ap_abreviatura', 'ap_shortname', 'ap_fecha_apertura', 'ap_fecha_cierre'];

    public function getAll($id_periodo_lectivo)
    {
        $sql = "
            SELECT a.id_aporte_evaluacion,
                   a.ap_nombre, 
                   p.pe_nombre
              FROM sw_aporte_evaluacion a,
                   sw_periodo_evaluacion p 
             WHERE p.id_periodo_evaluacion = a.id_periodo_evaluacion 
               AND p.id_periodo_lectivo = $id_periodo_lectivo 
             ORDER BY p.id_periodo_evaluacion, a.id_aporte_evaluacion              
        ";
        $query = $this->db->query($sql);
        $num_rows = count($query->getResultObject());
        $cadena = "";
        if ($num_rows > 0) {
            foreach ($query->getResult() as $row) {
                $cadena .= "<option value='". $row->id_aporte_evaluacion . "'>" . 
                            "[" . $row->pe_nombre . "] " . $row->ap_nombre . "</option>";
            }
        }
        return $cadena;
    }

    public function listarAportesPorPeriodoId($id_periodo)
    {
        $aportes = $this->db->query("
            SELECT a.*
              FROM sw_aporte_evaluacion a,
                   sw_periodo_evaluacion p 
             WHERE p.id_periodo_evaluacion = a.id_periodo_evaluacion
               AND a.id_periodo_evaluacion = $id_periodo 
             ORDER BY id_aporte_evaluacion              
        ");

        return $aportes->getResultObject();
    }

    public function crearCierresPeriodoLectivo($id_periodo_lectivo)
    {
        $sql = "CALL sp_crear_cierres_periodo_lectivo(?)";
        $result = $this->db->query($sql, [$id_periodo_lectivo]);

        if ($result) {
            return true;
        }

        return false;
    }

    public function eliminarCierresAporteEvaluacionPeriodoLectivo($id_aporte_evaluacion, $id_periodo_lectivo)
    {
        $sql = "CALL sp_eliminar_cierres_aporte_periodo_lectivo(?, ?)";
        $result = $this->db->query($sql, [$id_aporte_evaluacion, $id_periodo_lectivo]);

        if ($result) {
            return true;
        }

        return false;
    }
}
