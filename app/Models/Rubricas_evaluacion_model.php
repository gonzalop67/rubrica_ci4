<?php 
namespace App\Models;

use CodeIgniter\Model;

class Rubricas_evaluacion_model extends Model{
    protected $table      = 'sw_rubrica_evaluacion';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id_rubrica_evaluacion';
    protected $useAutoIncrement = true;

    protected $returnType     = 'object';

    protected $allowedFields = ['id_aporte_evaluacion', 'id_tipo_asignatura', 'ru_nombre', 'ru_abreviatura'];
}