<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->group('/', function($routes){
	$routes->get('', 'Auth\Login::index', ['as' => 'home']);
});

$routes->group('auth',['namespace' => 'App\Controllers\Auth'], function($routes){
	$routes->post('check', 'Login::signin', ['as' => 'signin']);
	$routes->get('logout', 'Login::signout', ['as' => 'signout']);
	$routes->get('dashboard', 'Login::dashboard', ['as' => 'dashboard']);
});

$routes->group('admin',['namespace' => 'App\Controllers\Admin'], function($routes){
	//RUTAS PARA MODALIDADES
	$routes->get('modalidades', 'Modalidades::index', ['as' => 'modalidades']);
	$routes->get('modalidades/crear', 'Modalidades::create', ['as' => 'modalidades_create']);
	$routes->post('modalidades/guardar', 'Modalidades::store', ['as' => 'modalidades_store']);
	$routes->get('modalidades/editar/(:any)', 'Modalidades::edit/$1', ['as' => 'modalidades_edit']);
	$routes->post('modalidades/actualizar', 'Modalidades::update', ['as' => 'modalidades_update']);
	$routes->get('modalidades/eliminar/(:any)', 'Modalidades::delete/$1', ['as' => 'modalidades_delete']);
	//RUTAS PARA PERIODOS LECTIVOS
	$routes->get('periodos_lectivos', 'Periodos_lectivos::index', ['as' => 'periodos_lectivos']);
	$routes->get('periodos_lectivos/crear', 'Periodos_lectivos::create', ['as' => 'periodos_lectivos_create']);
	//RUTAS PARA PERFILES
	$routes->get('perfiles', 'Perfiles::index', ['as' => 'perfiles']);
	$routes->get('perfiles/crear', 'Perfiles::create', ['as' => 'perfiles_create']);
	$routes->post('perfiles/guardar', 'Perfiles::store', ['as' => 'perfiles_store']);
	$routes->get('perfiles/editar/(:any)', 'Perfiles::edit/$1', ['as' => 'perfiles_edit']);
	$routes->post('perfiles/actualizar', 'Perfiles::update', ['as' => 'perfiles_update']);
	$routes->get('perfiles/eliminar/(:any)', 'Perfiles::delete/$1', ['as' => 'perfiles_delete']);
	//RUTAS PARA MENUS
	$routes->get('menus', 'Menus::index', ['as' => 'menus']);
	$routes->get('menus/crear', 'Menus::create', ['as' => 'menus_create']);
	$routes->post('menus/guardar', 'Menus::store', ['as' => 'menus_store']);
	$routes->post('menus/getMenusByRoleId', 'Menus::getMenusByRoleId', ['as' => 'menus_getByRoleId']);
	$routes->post('menus/getMenusByParentId', 'Menus::getMenusByParentId', ['as' => 'menus_getByParentId']);
	$routes->post('menus/getMenusById', 'Menus::getMenusById', ['as' => 'menus_getById']);
	$routes->post('menus/actualizar', 'Menus::update', ['as' => 'menus_update']);
	$routes->post('menus/saveNewPositions', 'Menus::saveNewPositions', ['as' => 'menus_saveNewPositions']);
	$routes->get('menus/crearSubmenu/(:any)', 'Menus::crearSubmenu/$1', ['as' => 'menus_crearSubmenus']);
	//RUTAS PARA USUARIOS
	$routes->get('usuarios', 'Usuarios::index', ['as' => 'usuarios']);
	$routes->get('usuarios/crear', 'Usuarios::create', ['as' => 'usuarios_create']);
	$routes->post('usuarios/guardar', 'Usuarios::store', ['as' => 'usuarios_store']);
	$routes->get('usuarios/editar/(:any)', 'Usuarios::edit/$1', ['as' => 'usuarios_edit']);
	$routes->post('usuarios/actualizar', 'Usuarios::update', ['as' => 'usuarios_update']);
	$routes->get('usuarios/eliminar/(:any)', 'Usuarios::delete/$1', ['as' => 'usuarios_delete']);
	$routes->post('usuarios/buscar', 'Usuarios::search', ['as' => 'usuarios_search']);
	$routes->get('usuarios/listar', 'Usuarios::show', ['as' => 'usuarios_listar']);
	//RUTAS PARA NIVELES (TIPOS) DE EDUCACION
	$routes->get('tipos_educacion', 'Tipos_educacion::index', ['as' => 'tipos_educacion']);
	$routes->get('tipos_educacion/crear', 'Tipos_educacion::create', ['as' => 'tipos_educacion_create']);
	$routes->post('tipos_educacion/guardar', 'Tipos_educacion::store', ['as' => 'tipos_educacion_store']);
	$routes->get('tipos_educacion/editar/(:any)', 'Tipos_educacion::edit/$1', ['as' => 'tipos_educacion_edit']);
	$routes->post('tipos_educacion/actualizar', 'Tipos_educacion::update', ['as' => 'tipos_educacion_update']);
	$routes->get('tipos_educacion/eliminar/(:any)', 'Tipos_educacion::delete/$1', ['as' => 'tipos_educacion_delete']);
	$routes->post('tipos_educacion/saveNewPositions', 'Tipos_educacion::saveNewPositions', ['as' => 'tipos_educacion_saveNewPositions']);
	//RUTAS PARA ESPECIALIDADES
	$routes->get('especialidades', 'Especialidades::index', ['as' => 'especialidades']);
	$routes->get('especialidades/crear', 'Especialidades::create', ['as' => 'especialidades_create']);
	$routes->post('especialidades/guardar', 'Especialidades::store', ['as' => 'especialidades_store']);
	$routes->get('especialidades/editar/(:any)', 'Especialidades::edit/$1', ['as' => 'especialidades_edit']);
	$routes->post('especialidades/actualizar', 'Especialidades::update', ['as' => 'especialidades_update']);
	$routes->get('especialidades/eliminar/(:any)', 'Especialidades::delete/$1', ['as' => 'especialidades_delete']);
	$routes->post('especialidades/saveNewPositions', 'Especialidades::saveNewPositions', ['as' => 'especialidades_saveNewPositions']);
	//RUTAS PARA CURSOS
	$routes->get('cursos', 'Cursos::index', ['as' => 'cursos']);
	$routes->get('cursos/crear', 'Cursos::create', ['as' => 'cursos_create']);
	$routes->post('cursos/guardar', 'Cursos::store', ['as' => 'cursos_store']);
	$routes->get('cursos/editar/(:any)', 'Cursos::edit/$1', ['as' => 'cursos_edit']);
	$routes->post('cursos/actualizar', 'Cursos::update', ['as' => 'cursos_update']);
	$routes->get('cursos/eliminar/(:any)', 'Cursos::delete/$1', ['as' => 'cursos_delete']);
	$routes->post('cursos/saveNewPositions', 'Cursos::saveNewPositions', ['as' => 'cursos_saveNewPositions']);
	//RUTAS PARA PARALELOS
	$routes->get('paralelos', 'Paralelos::index', ['as' => 'paralelos']);
	$routes->get('paralelos/crear', 'Paralelos::create', ['as' => 'paralelos_create']);
	$routes->get('paralelos/listar', 'Paralelos::show', ['as' => 'paralelos_listar']);
	$routes->post('paralelos/getParalelosById', 'Paralelos::getParalelosById', ['as' => 'paralelos_getById']);
	$routes->post('paralelos/guardar', 'Paralelos::store', ['as' => 'paralelos_store']);
	$routes->get('paralelos/editar/(:any)', 'Paralelos::edit/$1', ['as' => 'paralelos_edit']);
	$routes->post('paralelos/actualizar', 'Paralelos::update', ['as' => 'paralelos_update']);
	$routes->post('paralelos/eliminar', 'Paralelos::delete', ['as' => 'paralelos_delete']);
	$routes->get('paralelos/obtener', 'Paralelos::getAll', ['as' => 'paralelos_obtener']);
	$routes->post('paralelos/saveNewPositions', 'Paralelos::saveNewPositions', ['as' => 'paralelos_saveNewPositions']);
	//RUTAS PARA JORNADAS
	$routes->get('jornadas/obtener', 'Jornadas::getAll', ['as' => 'jornadas_obtener']);
	//RUTAS PARA TIPOS DE ASIGNATURA
	$routes->get('tipos_asignatura/obtener', 'Tipos_asignatura::getAll', ['as' => 'tipos_asignatura_obtener']);
	//RUTAS PARA AREAS
	$routes->get('areas', 'Areas::index', ['as' => 'areas']);
	$routes->get('areas/obtener', 'Areas::getAll', ['as' => 'areas_obtener']);
	$routes->get('areas/crear', 'Areas::create', ['as' => 'areas_create']);
	$routes->post('areas/guardar', 'Areas::store', ['as' => 'areas_store']);
	$routes->get('areas/editar/(:any)', 'Areas::edit/$1', ['as' => 'areas_edit']);
	$routes->post('areas/actualizar', 'Areas::update', ['as' => 'areas_update']);
	$routes->get('areas/eliminar/(:any)', 'Areas::delete/$1', ['as' => 'areas_delete']);
	//RUTAS PARA ASIGNATURAS
	$routes->get('asignaturas', 'Asignaturas::index', ['as' => 'asignaturas']);
	$routes->get('asignaturas/crear', 'Asignaturas::create', ['as' => 'asignaturas_create']);
	$routes->get('asignaturas/listar', 'Asignaturas::show', ['as' => 'asignaturas_listar']);
	$routes->post('asignaturas/getAsignaturasById', 'Asignaturas::getAsignaturasById', ['as' => 'asignaturas_getById']);
	$routes->post('asignaturas/guardar', 'Asignaturas::store', ['as' => 'asignaturas_store']);
	$routes->get('asignaturas/editar/(:any)', 'Asignaturas::edit/$1', ['as' => 'asignaturas_edit']);
	$routes->post('asignaturas/actualizar', 'Asignaturas::update', ['as' => 'asignaturas_update']);
	$routes->post('asignaturas/eliminar', 'Asignaturas::delete', ['as' => 'asignaturas_delete']);
	$routes->post('asignaturas/buscar', 'Asignaturas::search', ['as' => 'asignaturas_search']);
	//RUTAS PARA PERIODOS DE EVALUACION
	$routes->get('periodos_evaluacion', 'Periodos_evaluacion::index', ['as' => 'periodos_evaluacion']);
	$routes->get('periodos_evaluacion/crear', 'Periodos_evaluacion::create', ['as' => 'periodos_evaluacion_create']);
	$routes->post('periodos_evaluacion/guardar', 'Periodos_evaluacion::store', ['as' => 'periodos_evaluacion_store']);
	$routes->get('periodos_evaluacion/editar/(:any)', 'Periodos_evaluacion::edit/$1', ['as' => 'periodos_evaluacion_edit']);
	$routes->post('periodos_evaluacion/actualizar', 'Periodos_evaluacion::update', ['as' => 'periodos_evaluacion_update']);
	$routes->get('periodos_evaluacion/eliminar/(:any)', 'Periodos_evaluacion::delete/$1', ['as' => 'periodos_evaluacion_delete']);
	//RUTAS PARA APORTES DE EVALUACION
	$routes->get('aportes_evaluacion', 'Aportes_evaluacion::index', ['as' => 'aportes_evaluacion']);
	$routes->get('aportes_evaluacion/crear', 'Aportes_evaluacion::create', ['as' => 'aportes_evaluacion_create']);
	$routes->post('aportes_evaluacion/guardar', 'Aportes_evaluacion::store', ['as' => 'aportes_evaluacion_store']);
	$routes->get('aportes_evaluacion/editar/(:any)', 'Aportes_evaluacion::edit/$1', ['as' => 'aportes_evaluacion_edit']);
	$routes->post('aportes_evaluacion/actualizar', 'Aportes_evaluacion::update', ['as' => 'aportes_evaluacion_update']);
	$routes->get('aportes_evaluacion/eliminar/(:any)', 'Aportes_evaluacion::delete/$1', ['as' => 'aportes_evaluacion_delete']);
	$routes->get('aportes_evaluacion/obtener', 'Aportes_evaluacion::getAll', ['as' => 'aportes_evaluacion_obtener']);
	$routes->post('aportes_evaluacion/getAportesByPeriodoId', 'Aportes_evaluacion::getAportesByPeriodoId', ['as' => 'aportes_getByPeriodoId']);
	//RUTAS PARA RUBRICAS DE EVALUACION
	$routes->get('rubricas_evaluacion', 'Rubricas_evaluacion::index', ['as' => 'rubricas_evaluacion']);
	$routes->get('rubricas_evaluacion/crear', 'Rubricas_evaluacion::create', ['as' => 'rubricas_evaluacion_create']);
	$routes->post('rubricas_evaluacion/guardar', 'Rubricas_evaluacion::store', ['as' => 'rubricas_evaluacion_store']);
	$routes->get('rubricas_evaluacion/editar/(:any)', 'Rubricas_evaluacion::edit/$1', ['as' => 'rubricas_evaluacion_edit']);
	$routes->post('rubricas_evaluacion/actualizar', 'Rubricas_evaluacion::update', ['as' => 'rubricas_evaluacion_update']);
	$routes->get('rubricas_evaluacion/eliminar/(:any)', 'Rubricas_evaluacion::delete/$1', ['as' => 'rubricas_evaluacion_delete']);
	//RUTAS PARA ASIGNATURAS_CURSOS
	$routes->get('asignaturas_cursos', 'Asignaturas_cursos::index', ['as' => 'asignaturas_cursos']);
	$routes->post('asignaturas_cursos/guardar', 'Asignaturas_cursos::store', ['as' => 'asignaturas_cursos_store']);
	$routes->post('asignaturas_cursos/getByCursoId', 'Asignaturas_cursos::getByCursoId', ['as' => 'asignaturas_cursos_getByCursoId']);
	$routes->post('asignaturas_cursos/getByParaleloId', 'Asignaturas_cursos::getByParaleloId', ['as' => 'asignaturas_cursos_getByParaleloId']);
	$routes->post('asignaturas_cursos/saveNewPositions', 'Asignaturas_cursos::saveNewPositions', ['as' => 'asignaturas_cursos_saveNewPositions']);
	$routes->post('asignaturas_cursos/eliminar', 'Asignaturas_cursos::delete', ['as' => 'asignaturas_cursos_delete']);
	//RUTAS PARA CURSOS SUPERIORES
	$routes->get('curso_superior', 'Curso_superior::index', ['as' => 'cursos_superiores']);
	$routes->get('curso_superior/list', 'Curso_superior::list', ['as' => 'cursos_superiores_list']);
	$routes->post('curso_superior/guardar', 'Curso_superior::store', ['as' => 'cursos_superiores_store']);
	$routes->post('curso_superior/eliminar', 'Curso_superior::delete', ['as' => 'cursos_superiores_delete']);
	//RUTAS PARA PARALELOS TUTORES
	$routes->get('paralelos_tutores', 'Paralelos_tutores::index', ['as' => 'paralelos_tutores']);
	$routes->get('paralelos_tutores/list', 'Paralelos_tutores::list', ['as' => 'paralelos_tutores_list']);
	$routes->post('paralelos_tutores/guardar', 'Paralelos_tutores::store', ['as' => 'paralelos_tutores_store']);
	$routes->post('paralelos_tutores/eliminar', 'Paralelos_tutores::delete', ['as' => 'paralelos_tutores_delete']);
	//RUTAS PARA PARALELOS INSPECTORES
	$routes->get('paralelos_inspectores', 'Paralelos_inspectores::index', ['as' => 'paralelos_inspectores']);
	$routes->get('paralelos_inspectores/list', 'Paralelos_inspectores::list', ['as' => 'paralelos_inspectores_list']);
	$routes->post('paralelos_inspectores/guardar', 'Paralelos_inspectores::store', ['as' => 'paralelos_inspectores_store']);
	$routes->post('paralelos_inspectores/eliminar', 'Paralelos_inspectores::delete', ['as' => 'paralelos_inspectores_delete']);
	//RUTAS PARA INSTITUCIONES
	$routes->get('institucion', 'Institucion::index', ['as' => 'instituciones']);
	$routes->get('institucion/getData', 'Institucion::getData', ['as' => 'instituciones_getData']);
	$routes->post('institucion/guardar', 'Institucion::store', ['as' => 'instituciones_store']);
	//RUTAS PARA ESCALAS DE CALIFICACIONES
	$routes->get('escalas_calificaciones', 'Escalas_calificaciones::index', ['as' => 'escalas_calificaciones']);
	$routes->get('escalas_calificaciones/crear', 'Escalas_calificaciones::create', ['as' => 'escalas_calificaciones_create']);
	$routes->post('escalas_calificaciones/guardar', 'Escalas_calificaciones::store', ['as' => 'escalas_calificaciones_store']);
	$routes->get('escalas_calificaciones/editar/(:any)', 'Escalas_calificaciones::edit/$1', ['as' => 'escalas_calificaciones_edit']);
	$routes->post('escalas_calificaciones/actualizar', 'Escalas_calificaciones::update', ['as' => 'escalas_calificaciones_update']);
	$routes->get('escalas_calificaciones/eliminar/(:any)', 'Escalas_calificaciones::delete/$1', ['as' => 'escalas_calificaciones_delete']);
	//RUTAS PARA CIERRES DE PERIODOS
	$routes->get('cierre_periodos', 'Cierre_periodos::index', ['as' => 'cierre_periodos']);
	$routes->get('cierre_periodos/crear', 'Cierre_periodos::create', ['as' => 'cierre_periodos_create']);
	$routes->post('cierre_periodos/guardar', 'Cierre_periodos::store', ['as' => 'cierre_periodos_store']);
	$routes->get('cierre_periodos/editar/(:any)', 'Cierre_periodos::edit/$1', ['as' => 'cierre_periodos_edit']);
	$routes->post('cierre_periodos/actualizar', 'Cierre_periodos::update', ['as' => 'cierre_periodos_update']);
	$routes->get('cierre_periodos/listar', 'Cierre_periodos::show', ['as' => 'cierre_periodos_listar']);
	$routes->post('cierre_periodos/buscar', 'Cierre_periodos::search', ['as' => 'cierre_periodos_search']);
	$routes->post('cierre_periodos/getCierresPeriodosById', 'Cierre_periodos::getCierresPeriodosById', ['as' => 'cierre_periodos_getById']);
});

$routes->group('autoridad',['namespace' => 'App\Controllers\Autoridad'], function($routes){
	//RUTAS PARA MALLAS CURRICULARES
	$routes->get('mallas_curriculares', 'Mallas_curriculares::index', ['as' => 'mallas_curriculares']);
	$routes->post('mallas_curriculares/getByCursoId', 'Mallas_curriculares::getByCursoId', ['as' => 'mallas_curriculares_getByCursoId']);
	$routes->post('mallas_curriculares/guardar', 'Mallas_curriculares::store', ['as' => 'mallas_curriculares_store']);
	$routes->post('mallas_curriculares/getCierresPeriodosById', 'Mallas_curriculares::getMallaCurricularById', ['as' => 'mallas_curriculares_getById']);
	$routes->post('mallas_curriculares/actualizar', 'Mallas_curriculares::update', ['as' => 'mallas_curriculares_update']);
	$routes->post('mallas_curriculares/eliminar', 'Mallas_curriculares::delete', ['as' => 'mallas_curriculares_delete']);
	//RUTAS PARA DISTRIBUTIVOS
	$routes->get('distributivos', 'Distributivos::index', ['as' => 'distributivos']);
	$routes->post('distributivos/guardar', 'Distributivos::store', ['as' => 'distributivos_store']);
	$routes->post('distributivos/getByUsuarioId', 'Distributivos::getByUsuarioId', ['as' => 'distributivos_getByUsuarioId']);
	$routes->post('distributivos/eliminar', 'Distributivos::delete', ['as' => 'distributivos_delete']);
	//RUTAS PARA DIAS DE LA SEMANA
	$routes->get('dias_semana', 'Dias_semana::index', ['as' => 'dias_semana']);
	$routes->post('dias_semana/guardar', 'Dias_semana::store', ['as' => 'dias_semana_store']);
	$routes->get('dias_semana/listar', 'Dias_semana::show', ['as' => 'dias_semana_listar']);
	$routes->post('dias_semana/saveNewPositions', 'Dias_semana::saveNewPositions', ['as' => 'dias_semana_saveNewPositions']);
	$routes->post('dias_semana/getDiaSemanaById', 'Dias_semana::getDiaSemanaById', ['as' => 'dias_semana_getById']);
	$routes->post('dias_semana/getDiasSemanaByParaleloId', 'Dias_semana::getDiasSemanaByParaleloId', ['as' => 'dias_semana_por_paraleloID']);
	$routes->post('dias_semana/actualizar', 'Dias_semana::update', ['as' => 'dias_semana_update']);
	$routes->post('dias_semana/eliminar', 'Dias_semana::delete', ['as' => 'dias_semana_delete']);
	//RUTAS PARA HORAS CLASE
	$routes->get('horas_clase', 'Horas_clase::index', ['as' => 'horas_clase']);
	$routes->get('horas_clase/listar', 'Horas_clase::show', ['as' => 'horas_clase_listar']);
	$routes->post('horas_clase/guardar', 'Horas_clase::store', ['as' => 'horas_clase_store']);
	$routes->post('horas_clase/saveNewPositions', 'Horas_clase::saveNewPositions', ['as' => 'horas_clase_saveNewPositions']);
	$routes->post('horas_clase/getHoraClaseById', 'Horas_clase::getHoraClaseById', ['as' => 'horas_clase_getById']);
	$routes->post('horas_clase/getHoraClaseByParaleloId', 'Horas_clase::getHoraClaseByParaleloId', ['as' => 'horas_clase_por_paraleloID']);
	$routes->post('horas_clase/actualizar', 'Horas_clase::update', ['as' => 'horas_clase_update']);
	$routes->post('horas_clase/eliminar', 'Horas_clase::delete', ['as' => 'horas_clase_delete']);
	//RUTAS PARA HORAS CLASE DIAS DE LA SEMANA
	$routes->get('horas_dia', 'Horas_dia::index', ['as' => 'horas_dia']);
	$routes->post('horas_dia/guardar', 'Horas_dia::store', ['as' => 'horas_dia_store']);	
	$routes->post('horas_dia/getByDiaSemanaId', 'Horas_dia::getByDiaSemanaId', ['as' => 'horas_dia_getByDiaSemanaId']);
	$routes->post('horas_dia/eliminar', 'Horas_dia::delete', ['as' => 'horas_dia_delete']);
	//RUTAS PARA HORARIOS
	$routes->get('horarios', 'Horarios::index', ['as' => 'horarios']);
	$routes->post('horarios/guardar', 'Horarios::store', ['as' => 'horarios_store']);
	$routes->post('horarios/listar', 'Horarios::show', ['as' => 'horarios_listar']);
	$routes->post('horarios/eliminar', 'Horarios::delete', ['as' => 'horarios_delete']);
	//RUTAS PARA LISTAS DE DOCENTES
	$routes->get('lista_docentes', 'Lista_docentes::index', ['as' => 'lista_docentes']);
	$routes->post('lista_docentes/listar', 'Lista_docentes::show', ['as' => 'lista_docentes_por_paralelo']);
});

$routes->group('secretaria',['namespace' => 'App\Controllers\Secretaria'], function($routes){
	//RUTAS PARA MATRICULACION
	$routes->get('matriculacion', 'Matriculacion::index', ['as' => 'matriculacion']);
	$routes->post('matriculacion/guardar', 'Matriculacion::store', ['as' => 'matriculacion_store']);	
	$routes->post('matriculacion/listar', 'Matriculacion::show', ['as' => 'matriculacion_listar']);
	$routes->post('matriculacion/getStudentById', 'Matriculacion::getStudentById', ['as' => 'matriculacion_getStudentById']);
	$routes->post('matriculacion/actualizar', 'Matriculacion::update', ['as' => 'matriculacion_update']);
	$routes->post('matriculacion/eliminar', 'Matriculacion::delete', ['as' => 'matriculacion_delete']);
	$routes->post('matriculacion/des_matricular', 'Matriculacion::undelete', ['as' => 'matriculacion_undelete']);
	$routes->post('matriculacion/actualizar_retirado', 'Matriculacion::actualizar_retirado', ['as' => 'matriculacion_actualizar_retirado']);
	$routes->post('matriculacion/eliminados', 'Matriculacion::eliminados', ['as' => 'matriculacion_des_matriculados']);
	$routes->post('matriculacion/certificado', 'Matriculacion::certificado', ['as' => 'matriculacion_certificado']);
	$routes->post('matriculacion/buscar', 'Matriculacion::search', ['as' => 'matriculacion_buscar']);
	//RUTAS PARA TIPOS DE DOCUMENTO
	$routes->get('tipos_documento/obtener', 'Tipos_documento::getAll', ['as' => 'tipos_documento_obtener']);
	//RUTAS PARA DEFINICION DE GENEROS
	$routes->get('def_generos/obtener', 'Def_generos::getAll', ['as' => 'def_generos_obtener']);
	//RUTAS PARA DEFINICION DE NACIONALIDADES
	$routes->get('def_nacionalidades/obtener', 'Def_nacionalidades::getAll', ['as' => 'def_nacionalidades_obtener']);
});

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
