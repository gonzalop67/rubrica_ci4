<?php 
namespace App\Controllers\Secretaria;

use CodeIgniter\Controller;
use App\Models\Def_nacionalidades_model;

class Def_nacionalidades extends Controller
{
    protected $def_nacionalidades;

    public function __construct()
    {
        $this->def_nacionalidades = new Def_nacionalidades_model();
    }

    public function getAll()
    {
        echo $this->def_nacionalidades->getAll();
    }
}