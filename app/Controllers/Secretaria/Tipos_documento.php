<?php 
namespace App\Controllers\Secretaria;

use CodeIgniter\Controller;
use App\Models\Tipos_documento_model;

class Tipos_documento extends Controller
{
    protected $tipos_documento;

    public function __construct()
    {
        $this->tipos_documento = new Tipos_documento_model();
    }

    public function getAll()
    {
        echo $this->tipos_documento->getAll();
    }
}