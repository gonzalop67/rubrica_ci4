<?php

namespace App\Controllers\Secretaria;

use CodeIgniter\Controller;
use App\Models\Paralelos_model;
use App\Models\Estudiantes_model;
use App\Models\Institucion_model;
use App\Models\Estudiantes_periodo_lectivo_model;

class Matriculacion extends Controller
{
    protected $estudiantes, $estudiantes_periodo_lectivo, $institucion, $paralelos;

    protected $meses = array(0, "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

    protected $dias = array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");

    public function fecha_actual($ciudad)
    {
        $mes = $this->meses[date("n")];
        $dia = $this->dias[date("w")];
        $fecha_string = "$ciudad,  " . date("j") . " de $mes, " . date("Y");
        return $fecha_string;
    }

    public function __construct()
    {
        $this->paralelos = new Paralelos_model();
        $this->estudiantes = new Estudiantes_model();
        $this->estudiantes_periodo_lectivo = new Estudiantes_periodo_lectivo_model();
    }

    public function index()
    {
        return view('Secretaria/Matriculacion/index', [
            'paralelos' => $this->paralelos
                ->join(
                    'sw_curso',
                    'sw_curso.id_curso = sw_paralelo.id_curso'
                )
                ->join(
                    'sw_especialidad',
                    'sw_especialidad.id_especialidad = sw_curso.id_especialidad'
                )
                ->join(
                    'sw_tipo_educacion',
                    'sw_tipo_educacion.id_tipo_educacion = sw_especialidad.id_tipo_educacion'
                )
                ->join(
                    'sw_jornada',
                    'sw_jornada.id_jornada = sw_paralelo.id_jornada'
                )
                ->where('sw_paralelo.id_periodo_lectivo', session()->id_periodo_lectivo)
                ->orderBy('pa_orden')
                ->findAll()
        ]);
    }

    public function show()
    {
        $id_paralelo = $_POST['id_paralelo'];

        echo $this->estudiantes->listarEstudiantesParalelo($id_paralelo);
    }

    public function search()
    {
        $patron = $_POST['valor'];
        echo $this->estudiantes->getStudentsByName($patron);
    }

    public function getStudentById()
    {
        $id_estudiante = $_POST['id_estudiante'];
        echo json_encode($this->estudiantes->getStudentById($id_estudiante));
    }

    public function store()
    {
        $id_tipo_documento = trim($_POST['id_tipo_documento']);
        $id_def_genero = trim($_POST['id_def_genero']);
        $id_def_nacionalidad = trim($_POST['id_def_nacionalidad']);
        $es_apellidos = strtoupper(trim($_POST['es_apellidos']));
        $es_nombres = strtoupper(trim($_POST['es_nombres']));
        $es_nombre_completo = $es_apellidos . " " . $es_nombres;
        $es_cedula = strtoupper(trim($_POST['es_cedula']));
        $es_email = trim($_POST['es_email']);
        $es_sector = trim($_POST['es_sector']);
        $es_direccion = trim($_POST['es_direccion']);
        $es_telefono = trim($_POST['es_telefono']);
        $es_fec_nacim = trim($_POST['es_fec_nacim']);

        $id_periodo_lectivo = session()->id_periodo_lectivo;

        // Primero comprobar si ya existen los nombres y apellidos del estudiante
        if ($this->estudiantes->existeNombreEstudiante($es_apellidos, $es_nombres)) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "Ya existe el estudiante en la base de datos...",
                "tipo_mensaje" => "error"
            );

            echo json_encode($data);
        } elseif ($this->estudiantes->existeNroCedula($es_cedula)) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "Ya existe el número de cédula en la base de datos...",
                "tipo_mensaje" => "error"
            );

            echo json_encode($data);
        } else {
            try {
                $datos = [
                    'id_tipo_documento' => $id_tipo_documento,
                    'id_def_genero' => $id_def_genero,
                    'id_def_nacionalidad' => $id_def_nacionalidad,
                    'es_apellidos' => $es_apellidos,
                    'es_nombres' => $es_nombres,
                    'es_nombre_completo' => $es_nombre_completo,
                    'es_cedula' => $es_cedula,
                    'es_email' => $es_email,
                    'es_sector' => $es_sector,
                    'es_direccion' => $es_direccion,
                    'es_telefono' => $es_telefono,
                    'es_fec_nacim' => $es_fec_nacim
                ];
                $this->estudiantes->insert($datos);
                //Insertar en la tabla sw_estudiante_periodo_lectivo
                $id_estudiante = $this->estudiantes->insertID;
                $datos = [
                    'id_estudiante' => $id_estudiante,
                    'id_periodo_lectivo' => $id_periodo_lectivo,
                    'id_paralelo' => $_POST['id_paralelo'],
                    'es_estado' => 'N',
                    'es_retirado' => 'N',
                    'nro_matricula' => $this->estudiantes_periodo_lectivo->getMaxNroMatricula($id_periodo_lectivo) + 1,
                    'activo' => 1
                ];
                $this->estudiantes_periodo_lectivo->insert($datos);

                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "El estudiante fue insertado exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            } catch (\Exception $e) {
                $data = array(
                    "titulo"       => "Ocurrió un error al tratar de insertar al estudiante.",
                    "mensaje"      => "Error...: " . $e->getMessage(),
                    "tipo_mensaje" => "error"
                );
            }
        }
    }

    public function update()
    {
        $id_estudiante = trim($_POST['id_estudiante']);
        $id_tipo_documento = trim($_POST['id_tipo_documento']);
        $id_def_genero = trim($_POST['id_def_genero']);
        $id_def_nacionalidad = trim($_POST['id_def_nacionalidad']);
        $es_apellidos = strtoupper(trim($_POST['es_apellidos']));
        $es_nombres = strtoupper(trim($_POST['es_nombres']));
        $es_nombre_completo = $es_apellidos . " " . $es_nombres;
        $es_cedula = strtoupper(trim($_POST['es_cedula']));
        $es_email = trim($_POST['es_email']);
        $es_sector = trim($_POST['es_sector']);
        $es_direccion = trim($_POST['es_direccion']);
        $es_telefono = trim($_POST['es_telefono']);
        $es_fec_nacim = trim($_POST['es_fec_nacim']);

        $estudiante_actual = $this->estudiantes->find($id_estudiante);

        // Primero comprobar si ya existen los nombres y apellidos del estudiante
        if (
            $estudiante_actual->es_apellidos != $es_apellidos &&
            $estudiante_actual->es_nombres != $es_nombres &&
            $this->estudiantes->existeNombreEstudiante($es_apellidos, $es_nombres)
        ) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "Ya existe el estudiante en la base de datos...",
                "tipo_mensaje" => "error"
            );

            echo json_encode($data);
        } elseif ($estudiante_actual->es_cedula != $es_cedula && $this->estudiantes->existeNroCedula($es_cedula)) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "Ya existe el número de cédula en la base de datos...",
                "tipo_mensaje" => "error"
            );

            echo json_encode($data);
        } else {
            try {
                $datos = [
                    'id_estudiante' => $id_estudiante,
                    'id_tipo_documento' => $id_tipo_documento,
                    'id_def_genero' => $id_def_genero,
                    'id_def_nacionalidad' => $id_def_nacionalidad,
                    'es_apellidos' => $es_apellidos,
                    'es_nombres' => $es_nombres,
                    'es_nombre_completo' => $es_nombre_completo,
                    'es_cedula' => $es_cedula,
                    'es_email' => $es_email,
                    'es_sector' => $es_sector,
                    'es_direccion' => $es_direccion,
                    'es_telefono' => $es_telefono,
                    'es_fec_nacim' => $es_fec_nacim
                ];
                $this->estudiantes->save($datos);

                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "El estudiante fue actualizado exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            } catch (\Exception $e) {
                $data = array(
                    "titulo"       => "Ocurrió un error al tratar de actualizar el estudiante.",
                    "mensaje"      => "Error...: " . $e->getMessage(),
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

    public function delete()
    {
        $id_estudiante = $_POST['id_estudiante'];
        $id_paralelo = $_POST['id_paralelo'];

        try {
            $this->estudiantes_periodo_lectivo->quitarEstudianteParalelo($id_estudiante, $id_paralelo);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El estudiante fue des-matriculado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        } catch (\Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error al tratar de des-matricular el estudiante.",
                "mensaje"      => "Error...: " . $e->getMessage(),
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

    public function undelete()
    {
        $id_estudiante_periodo_lectivo = $_POST['id_estudiante_periodo_lectivo'];

        try {
            $this->estudiantes_periodo_lectivo->undeleteEstudianteParalelo($id_estudiante_periodo_lectivo);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El estudiante fue matriculado nuevamente de manera exitosa.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        } catch (\Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error al tratar de volver a matricular al estudiante.",
                "mensaje"      => "Error...: " . $e->getMessage(),
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

    public function actualizar_retirado()
    {
        $id_periodo_lectivo = session()->id_periodo_lectivo;
        $id_estudiante = $_POST["id_estudiante"];
        $estado_retirado = $_POST["es_retirado"];

        $datos = [
            'id_estudiante' => $id_estudiante,
            'estado_retirado' => $estado_retirado,
            'id_periodo_lectivo' => $id_periodo_lectivo
        ];

        try {
            $this->estudiantes_periodo_lectivo->actualizarEstadoRetirado($datos);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "Se actualizó el estado de retirado de manera exitosa.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        } catch (\Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error al tratar de actualizar el estado de retirado.",
                "mensaje"      => "Error...: " . $e->getMessage(),
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

    public function eliminados()
    {
        $id_paralelo = $_POST['id_paralelo'];

        echo $this->estudiantes->listarEstudiantesDesMatriculados($id_paralelo);
    }

    public function certificado($id_estudiante_periodo_lectivo)
    {
        $datos = $this->estudiantes_periodo_lectivo->where('id_estudiante_periodo_lectivo', $id_estudiante_periodo_lectivo)->first();

        $id_estudiante = $datos->id_estudiante;
        $id_paralelo = $datos->id_paralelo;

        $pdf = new \FPDF();
        $pdf->AddPage();

        $this->institucion = new Institucion_model();
        $datosInstitucion = $this->institucion->where('id_institucion', 1)->first();

        //Logo Izquierda
        $pdf->Image(base_url() . '/public/images/ministerio.png', 10, 18, 33);

        //Logo Derecha
        $pdf->Image(base_url() . '/public/images/' . $datosInstitucion->in_logo, 210 - 40, 5, 23);

        $pdf->SetFont('Times', 'B', 14);
        $title = utf8_decode($datosInstitucion->in_nombre);
        $w = $pdf->GetStringWidth($title);
        $pdf->SetX((210 - $w) / 2);
        $pdf->Cell($w, 10, $title, 0, 0, 'C');
        $pdf->Ln(5);

        $pdf->SetFont('Arial', 'I', 12);
        $title = utf8_decode($datosInstitucion->in_direccion);
        $w = $pdf->GetStringWidth($title);
        $pdf->SetX((210 - $w) / 2);
        $pdf->Cell($w, 10, $title, 0, 0, 'C');
        $pdf->Ln(5);

        $pdf->SetFont('Arial', 'I', 11);
        $title = utf8_decode("Teléfono: ") . utf8_decode($datosInstitucion->in_telefono);
        $w = $pdf->GetStringWidth($title);
        $pdf->SetX((210 - $w) / 2);
        $pdf->Cell($w, 10, $title, 0, 0, 'C');
        $pdf->Ln(5);

        $pdf->SetFont('Arial', 'I', 11);
        $title = "AMIE: " . $datosInstitucion->in_amie;
        $w = $pdf->GetStringWidth($title);
        $pdf->SetX((210 - $w) / 2);
        $pdf->Cell($w, 10, $title, 0, 0, 'C');
        $pdf->Ln(15);

        $pdf->Line(10, 35, 210 - 10, 35); // 20mm from each edge

        $pdf->SetFont('Times', 'B', 14);
        $title = "CERTIFICADO DE MATRICULA";
        $w = $pdf->GetStringWidth($title);
        $pdf->SetX((210 - $w) / 2);
        $pdf->Cell($w, 10, $title, 0, 0, 'C');
        $pdf->Ln();

        $pdf->SetFont('Times', '', 12);
        $text = utf8_decode("El Rector(a) de la Institución, previo cumplimiento de las respectivas disposiciones de la Ley Orgánica");
        $pdf->Cell(200, 10, $text, 0, 0, 'L');
        $pdf->Ln(5);

        $text = utf8_decode("de Educación Intercultural y su Reglamento vigentes.");
        $pdf->Cell(200, 10, $text, 0, 0, 'L');
        $pdf->Ln();

        $pdf->SetFont('Times', 'B', 12);
        $pdf->Cell(200, 10, "CERTIFICA:", 0, 0, 'L');
        $pdf->Ln();

        $pdf->SetFont('Times', '', 12);
        $pdf->Cell(200, 10, utf8_decode("Que en los archivos de Secretaría, está inscrita la siguiente MATRICULA:"), 0, 0, 'L');
        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Times', '', 12);
        $pdf->Cell(50, 10, "ESTUDIANTE:", 0, 0, 'L');

        $datosEstudiante = $this->estudiantes->where('id_estudiante', $id_estudiante)->first();
        $nombreEstudiante = $datosEstudiante->es_apellidos . " " . $datosEstudiante->es_nombres;
        $matricula = str_pad($datos->nro_matricula, 4, "0", STR_PAD_LEFT);

        $pdf->SetFont('Times', 'B', 12);
        $pdf->Cell(150, 10, $nombreEstudiante, 0, 0, 'L');
        $pdf->Ln();

        $pdf->SetFont('Times', '', 12);
        $pdf->Cell(50, 10, "NIVEL DE EDUCACION:", 0, 0, 'L');

        //Aqui obtengo el nivel de educación del estudiante
        $datosNivelEducacion = $this->paralelos->getNivelEducacionByIdParalelo($id_paralelo);

        $nivelEducacion = utf8_decode($datosNivelEducacion->te_nombre);

        $pdf->SetFont('Times', 'B', 12);
        $pdf->Cell(150, 10, $nivelEducacion, 0, 0, 'L');
        $pdf->Ln();

        $pdf->SetFont('Times', '', 12);
        $pdf->Cell(50, 10, "JORNADA:", 0, 0, 'L');

        //Aqui obtengo la jornada
        $datosJornada = $this->paralelos->getJornadaByIdParalelo($id_paralelo);

        $jornada = $datosJornada->jo_nombre;
        $curso = $datosJornada->cu_nombre;
        $paralelo = $datosJornada->pa_nombre;

        $pdf->SetFont('Times', 'B', 12);
        $pdf->Cell(30, 10, $jornada, 0, 0, 'L');

        $pdf->SetFont('Times', '', 12);
        $pdf->Cell(30, 10, utf8_decode("AÑO LECTIVO:"), 0, 0, 'L');

        //Aqui obtengo el año lectivo
        $datosPeriodoLectivo = $this->estudiantes_periodo_lectivo->getPeriodoLectivoById($id_estudiante_periodo_lectivo);
        $anio_lectivo = $datosPeriodoLectivo->pe_anio_inicio . "-" . $datosPeriodoLectivo->pe_anio_fin;

        $pdf->SetFont('Times', 'B', 12);
        $pdf->Cell(30, 10, $anio_lectivo, 0, 0, 'L');
        $pdf->Ln();

        $pdf->SetFont('Times', '', 12);
        $pdf->Cell(50, 10, utf8_decode("MATRÍCULA:"), 0, 0, 'L');

        $pdf->SetFont('Times', 'B', 12);
        $pdf->Cell(30, 10, $matricula, 0, 0, 'L');
        $pdf->Ln();

        $pdf->SetFont('Times', '', 12);
        $pdf->Cell(50, 10, utf8_decode("GRADO/CURSO:"), 0, 0, 'L');

        $pdf->SetFont('Times', 'B', 12);
        $pdf->Cell(30, 10, $curso, 0, 0, 'L');
        $pdf->Ln();

        $pdf->SetFont('Times', '', 12);
        $pdf->Cell(50, 10, utf8_decode("PARALELO:"), 0, 0, 'L');

        $pdf->SetFont('Times', 'B', 12);
        $pdf->Cell(30, 10, "\"" . $paralelo . "\"", 0, 0, 'L');
        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Times', '', 12);
        $title = $this->fecha_actual($datosInstitucion->in_ciudad);
        $w = $pdf->GetStringWidth($title);
        $pdf->SetX((210 - $w) / 2);
        $pdf->Cell($w, 10, $title, 0, 0, 'C');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Times', '', 14);
        $title = utf8_decode($datosInstitucion->in_nom_rector);
        $w = $pdf->GetStringWidth($title);
        $pdf->SetX((210 - $w) / 2);
        $pdf->Cell($w, 10, $title, 0, 0, 'C');
        $pdf->Ln(5);

        $pdf->SetFont('Times', 'B', 14);
        $title = "RECTOR(A) DEL PLANTEL";
        $w = $pdf->GetStringWidth($title);
        $pdf->SetX((210 - $w) / 2);
        $pdf->Cell($w, 10, $title, 0, 0, 'C');
        $pdf->Ln();

        $this->response->setHeader('Content-Type', 'application/pdf');
        $pdf->Output("certificado_matricula.pdf", "I");
    }
}
