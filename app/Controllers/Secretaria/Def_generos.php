<?php 
namespace App\Controllers\Secretaria;

use CodeIgniter\Controller;
use App\Models\Def_generos_model;

class Def_generos extends Controller
{
    protected $def_generos;

    public function __construct()
    {
        $this->def_generos = new Def_generos_model();
    }

    public function getAll()
    {
        echo $this->def_generos->getAll();
    }
}