<?php

namespace App\Controllers\Autoridad;

use CodeIgniter\Controller;
use App\Models\Horas_clase_model;

class Horas_clase extends Controller
{

    protected $horas_clase;

    public function __construct()
    {
        $this->horas_clase = new Horas_clase_model();
    }

    public function index()
    {
        return view('Autoridad/Horas_clase/index');
    }

    public function show()
    {
        $id_periodo_lectivo = session()->id_periodo_lectivo;
        echo $this->horas_clase->getHorasClase($id_periodo_lectivo);
    }

    public function getHoraClaseById()
    {
        $id_hora_clase = $_POST['id_hora_clase'];
        echo json_encode($this->horas_clase->getHoraClaseById($id_hora_clase));
    }

    public function getHoraClaseByParaleloId()
    {
        $id_paralelo = $_POST['id_paralelo'];
        echo $this->horas_clase->getHoraClaseByParaleloId($id_paralelo);
    }

    public function store()
    {
        try {
            $hc_nombre = trim($_POST['hc_nombre']);
            $hc_hora_inicio = trim($_POST['hc_hora_inicio']);
            $hc_hora_fin = trim($_POST['hc_hora_fin']);
            $id_periodo_lectivo = session()->id_periodo_lectivo;

            if ($this->horas_clase->existeNombreHoraClase($hc_nombre, $id_periodo_lectivo)) {
                $data = array(
                    "titulo" => "Ocurrió un error inesperado.",
                    "mensaje"  => "Ya se ha definido la hora de clase $hc_nombre para este periodo lectivo",
                    "tipo_mensaje"  => "error"
                );

                echo json_encode($data);
            } else {

                $secuencial = $this->horas_clase->secuencialHoraClase($id_periodo_lectivo);

                $this->horas_clase->save([
                    'id_periodo_lectivo' => $id_periodo_lectivo,
                    'hc_nombre'          => $hc_nombre,
                    'hc_hora_inicio'     => $hc_hora_inicio,
                    'hc_hora_fin'        => $hc_hora_fin,
                    'hc_ordinal'         => $secuencial + 1
                ]);

                $data = array(
                    "titulo" => "Inserción exitosa.",
                    "mensaje"  => 'La Hora de Clase se insertó correctamente',
                    "tipo_mensaje"  => "success"
                );

                echo json_encode($data);
            }
        } catch (\Exception $e) {
            $data = array(
                "titulo" => "Ocurrió un error inesperado.",
                "mensaje"  => 'La Hora de Clase no se pudo insertar correctamente...Error: ' . $e->getMessage(),
                "tipo_mensaje"  => "error"
            );

            echo json_encode($data);
        }
    }

    public function update()
    {
        try {
            $hora_clase = $this->horas_clase->find($_POST['id_hora_clase']);
            $id_periodo_lectivo = session()->id_periodo_lectivo;

            if($hora_clase->hc_nombre != $_POST['hc_nombre']) {
                if($this->horas_clase->existeNombreHoraClase($_POST['hc_nombre'], $id_periodo_lectivo)) {
                    $data = array(
                        "titulo"       => "Ocurrió un error inesperado.",
                        "mensaje"      => "El Nombre de la Hora Clase ya existe en la base de datos.",
                        "tipo_mensaje" => "error"
                    );
                    echo json_encode($data);
                } else {
                    $this->horas_clase->save([
                        'id_hora_clase'  => $_POST['id_hora_clase'],
                        'hc_nombre'      => $_POST['hc_nombre'],
                        'hc_hora_inicio' => $_POST['hc_hora_inicio'],
                        'hc_hora_fin'    => $_POST['hc_hora_fin']
                    ]);
        
                    $data = array(
                        "titulo"       => "Operación exitosa.",
                        "mensaje"      => "La Hora Clase fue actualizada exitosamente.",
                        "tipo_mensaje" => "success"
                    );
                    echo json_encode($data);
                }
            } else {
                $this->horas_clase->save([
                    'id_hora_clase'  => $_POST['id_hora_clase'],
                    'hc_nombre'      => $_POST['hc_nombre'],
                    'hc_hora_inicio' => $_POST['hc_hora_inicio'],
                    'hc_hora_fin'    => $_POST['hc_hora_fin']
                ]);
    
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "La Hora Clase fue actualizada exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }
        } catch (\Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "La Hora Clase no se pudo actualizar...Error: " . $e->getMessage(),
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

    public function delete()
    {
        try {
            $this->horas_clase->delete($_POST['id']);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "La Hora Clase fue eliminada exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        } catch (\Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "La Hora Clase no se pudo eliminar...Error: " . $e->getMessage(),
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

    public function saveNewPositions()
    {
        foreach($_POST['positions'] as $position) {
            $index = $position[0];
            $newPosition = $position[1];

            $this->horas_clase->actualizarOrden($index, $newPosition);
        }
    }
}
