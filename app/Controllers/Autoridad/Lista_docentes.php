<?php

namespace App\Controllers\Autoridad;

use CodeIgniter\Controller;
use App\Models\Cursos_model;
use App\Models\Usuarios_model;
use App\Models\Paralelos_model;

class Lista_docentes extends Controller
{

    protected $paralelos, $cursos, $usuarios, $reglas;

    public function __construct()
    {
        $this->cursos = new Cursos_model();
        $this->usuarios = new Usuarios_model();
        $this->paralelos = new Paralelos_model();
    }

    public function index()
    {
        return view('Autoridad/Lista_docentes/index', [
            'paralelos' => $this->paralelos
                ->join(
                    'sw_curso',
                    'sw_curso.id_curso = sw_paralelo.id_curso'
                )
                ->join(
                    'sw_especialidad',
                    'sw_especialidad.id_especialidad = sw_curso.id_especialidad'
                )
                ->join(
                    'sw_tipo_educacion',
                    'sw_tipo_educacion.id_tipo_educacion = sw_especialidad.id_tipo_educacion'
                )
                ->join(
                    'sw_jornada',
                    'sw_jornada.id_jornada = sw_paralelo.id_jornada'
                )
                ->where('sw_paralelo.id_periodo_lectivo', session()->id_periodo_lectivo)
                ->orderBy('pa_orden')
                ->findAll()
        ]);
    }

    public function show()
    {
        $id_paralelo = $_POST['id_paralelo'];
        $id_curso = $this->cursos->getCursoIdByParalelo($id_paralelo);

        echo $this->usuarios->getDocentesByParalelo($id_curso, $id_paralelo);
    }
}