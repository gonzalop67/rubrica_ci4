<?php

namespace App\Controllers\Autoridad;

use CodeIgniter\Controller;
use App\Models\Horas_dia_model;
use App\Models\Dias_semana_model;
use App\Models\Horas_clase_model;

class Horas_dia extends Controller
{

    protected $horas_dia, $dias_semana, $horas_clase;

    public function __construct()
    {
        $this->horas_dia = new Horas_dia_model();
        $this->dias_semana = new Dias_semana_model();
        $this->horas_clase = new Horas_clase_model();
    }

    public function index()
    {
        $id_periodo_lectivo = session()->id_periodo_lectivo;

        return view('Autoridad/Horas_dia/index', [
            'dias_semana' => $this->dias_semana->orderBy('ds_ordinal')->findAll(),
            'horas_clase' => $this->horas_clase->getHorasClaseByPeriodoId($id_periodo_lectivo)
        ]);
    }

    public function getByDiaSemanaId()
    {
        $id_dia_semana = $_POST['id_dia_semana'];
        echo json_encode($this->horas_dia->getByDiaSemanaId($id_dia_semana));
    }

    public function store()
    {
        try {
            $id_dia_semana = trim($_POST['id_dia_semana']);
            $id_hora_clase = trim($_POST['id_hora_clase']);
            $id_periodo_lectivo = session()->id_periodo_lectivo;

            if ($this->horas_dia->existeAsociacion($id_dia_semana, $id_hora_clase)) {
                $data = array(
                    "titulo" => "Ocurrió un error inesperado.",
                    "mensaje"  => "Ya existe la asociación entre el día y la hora clase para este periodo lectivo",
                    "tipo_mensaje"  => "error"
                );

                echo json_encode($data);
            } else {

                $this->horas_dia->save([
                    'id_periodo_lectivo' => $id_periodo_lectivo,
                    'id_dia_semana'      => $id_dia_semana,
                    'id_hora_clase'      => $id_hora_clase, 
                ]);

                $data = array(
                    "titulo" => "Inserción exitosa.",
                    "mensaje"  => "La Asociación entre Hora Clase y Día de la Semana se insertó correctamente",
                    "tipo_mensaje"  => "success"
                );

                echo json_encode($data);
            }
        } catch (\Exception $e) {
            $data = array(
                "titulo" => "Ocurrió un error inesperado.",
                "mensaje"  => "La Asociación entre Hora Clase y Día de la Semana no se pudo insertar correctamente...Error: " . $e->getMessage(),
                "tipo_mensaje"  => "error"
            );

            echo json_encode($data);
        }
    }

    public function delete()
    {
        try {
            $this->horas_dia->delete($_POST['id']);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "La Asociación entre Día de la Semana y Hora Clase fue eliminada exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        } catch (\Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "La Asociación entre Día de la Semana y Hora Clase no se pudo eliminar...Error: " . $e->getMessage(),
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

}
