<?php

namespace App\Controllers\Autoridad;

use CodeIgniter\Controller;
use App\Models\Dias_semana_model;

class Dias_semana extends Controller
{

    protected $dias_semana;

    public function __construct()
    {
        $this->dias_semana = new Dias_semana_model();
    }

    public function index()
    {
        return view('Autoridad/Dias_semana/index');
    }

    public function show()
    {
        $id_periodo_lectivo = session()->id_periodo_lectivo;
        echo $this->dias_semana->getDiasSemana($id_periodo_lectivo);
    }

    public function getDiaSemanaById()
    {
        $id_dia_semana = $_POST['id_dia_semana'];
        echo json_encode($this->dias_semana->getDiaSemanaById($id_dia_semana));
    }

    public function getDiasSemanaByParaleloId()
    {
        $id_paralelo = $_POST['id_paralelo'];
        echo $this->dias_semana->getDiasSemanaByParaleloId($id_paralelo);
    }

    public function store()
    {
        try {
            $ds_nombre = trim($_POST['ds_nombre']);
            $id_periodo_lectivo = session()->id_periodo_lectivo;

            if ($this->dias_semana->existeNombreDiaSemana($ds_nombre, $id_periodo_lectivo)) {
                $data = array(
                    "titulo" => "Ocurrió un error inesperado.",
                    "mensaje"  => "Ya se ha definido el Día de la Semana $ds_nombre para este periodo lectivo",
                    "tipo_mensaje"  => "error"
                );

                echo json_encode($data);
            } else {

                $secuencial = $this->dias_semana->secuencialDiaSemana($id_periodo_lectivo);

                $this->dias_semana->save([
                    'id_periodo_lectivo' => $id_periodo_lectivo,
                    'ds_nombre'          => strtoupper($ds_nombre),
                    'ds_ordinal'         => $secuencial + 1
                ]);

                $data = array(
                    "titulo" => "Inserción exitosa.",
                    "mensaje"  => 'El Día de la Semana se insertó correctamente',
                    "tipo_mensaje"  => "success"
                );

                echo json_encode($data);
            }
        } catch (\Exception $e) {
            $data = array(
                "titulo" => "Ocurrió un error inesperado.",
                "mensaje"  => 'El Día de la Semana no se pudo insertar correctamente...Error: ' . $e->getMessage(),
                "tipo_mensaje"  => "error"
            );

            echo json_encode($data);
        }
    }

    public function update()
    {
        try {
            $dia_semana = $this->dias_semana->find($_POST['id_dia_semana']);
            $id_periodo_lectivo = session()->id_periodo_lectivo;

            if($dia_semana->ds_nombre != $_POST['ds_nombre']) {
                if($this->dias_semana->existeNombreDiaSemana($_POST['ds_nombre'], $id_periodo_lectivo)) {
                    $data = array(
                        "titulo"       => "Ocurrió un error inesperado.",
                        "mensaje"      => "El Nombre del Día de la Semana ya existe en la base de datos.",
                        "tipo_mensaje" => "error"
                    );
                    echo json_encode($data);
                } else {
                    $this->dias_semana->save([
                        'id_dia_semana' => $_POST['id_dia_semana'],
                        'ds_nombre'     => strtoupper($_POST['ds_nombre']),
                    ]);
        
                    $data = array(
                        "titulo"       => "Operación exitosa.",
                        "mensaje"      => "El Día de la Semana fue actualizado exitosamente.",
                        "tipo_mensaje" => "success"
                    );
                    echo json_encode($data);
                }
            } else {
                $this->dias_semana->save([
                    'id_dia_semana' => $_POST['id_dia_semana'],
                    'ds_nombre'     => strtoupper($_POST['ds_nombre']),
                ]);
    
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "El Día de la Semana fue actualizado exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            }
        } catch (\Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El Día de la Semana no se pudo actualizar...Error: " . $e->getMessage(),
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

    public function delete()
    {
        try {
            $this->dias_semana->delete($_POST['id']);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El Día de la Semana fue eliminado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        } catch (\Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El Día de la Semana no se pudo eliminar...Error: " . $e->getMessage(),
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

    public function saveNewPositions()
    {
        foreach($_POST['positions'] as $position) {
            $index = $position[0];
            $newPosition = $position[1];

            $this->dias_semana->actualizarOrden($index, $newPosition);
        }
    }
}
