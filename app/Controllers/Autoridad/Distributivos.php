<?php

namespace App\Controllers\Autoridad;

use CodeIgniter\Controller;
use App\Models\Usuarios_model;
use App\Models\Paralelos_model;
use App\Models\Asignaturas_model;
use App\Models\Distributivos_model;
use App\Models\Mallas_curriculares_model;

class Distributivos extends Controller
{

    protected $paralelos, $asignaturas, $distributivos, $mallas_curriculares, $usuarios;

    public function __construct()
    {
        $this->usuarios = new Usuarios_model();
        $this->paralelos = new Paralelos_model();
        $this->asignaturas = new Asignaturas_model();
        $this->distributivos = new Distributivos_model();
        $this->mallas_curriculares = new Mallas_curriculares_model();
    }

    public function index()
    {
        return view('Autoridad/Distributivos/index', [
            'paralelos' => $this->paralelos
                ->join(
                    'sw_curso',
                    'sw_curso.id_curso = sw_paralelo.id_curso'
                )
                ->join(
                    'sw_especialidad',
                    'sw_especialidad.id_especialidad = sw_curso.id_especialidad'
                )
                ->orderBy('pa_orden')
                ->findAll(),
            'usuarios' => $this->usuarios
                ->join(
                    'sw_usuario_perfil',
                    'sw_usuario.id_usuario = sw_usuario_perfil.id_usuario'
                )
                ->join(
                    'sw_perfil',
                    'sw_perfil.id_perfil = sw_usuario_perfil.id_perfil'
                )
                ->where('sw_perfil.pe_nombre = "Docente"')
                ->orderBy('us_apellidos')
                ->orderBy('us_nombres')
                ->findAll()
        ]);
    }

    public function store()
    {
        try {
            $id_usuario = $_POST['id_usuario'];
            $id_paralelo = $_POST['id_paralelo'];
            $id_asignatura = $_POST['id_asignatura'];

            if ($this->distributivos->existeAsociacion($id_paralelo, $id_asignatura)) {
                $data = array(
                    "titulo"        => "Ocurrió un error inesperado.",
                    "mensaje"       => 'Ya existe la asociación entre la asignatura y el paralelo en el distributivo.',
                    "tipo_mensaje"  => "error"
                );

                echo json_encode($data);
            } else {
                $id_periodo_lectivo = session()->id_periodo_lectivo;

                //Recupero el id_curso asociado con el id_paralelo
                $id_curso = $this->paralelos->getCursoId($id_paralelo);

                if ($this->mallas_curriculares->existeAsociacion($id_curso, $id_asignatura)) {
                    //Ahora recupero el id_malla_curricular asociado con el id_curso y el id_asignatura
                    $id_malla_curricular = $this->mallas_curriculares->getMallaIdCursoAsignatura($id_curso, $id_asignatura);

                    $this->distributivos->save([
                        'id_periodo_lectivo'  => $id_periodo_lectivo,
                        'id_malla_curricular' => $id_malla_curricular,
                        'id_paralelo'         => $id_paralelo,
                        'id_asignatura'       => $id_asignatura,
                        'id_usuario'          => $id_usuario
                    ]);

                    $data = array(
                        "titulo" => "Inserción exitosa.",
                        "mensaje"  => 'El Item del Distributivo se insertó correctamente',
                        "tipo_mensaje"  => "success"
                    );

                    echo json_encode($data);
                } else {
                    $data = array(
                        "titulo" => "Ocurrió un error inesperado.",
                        "mensaje"  => "No se han asociado items a la malla con el paralelo y la asignatura seleccionados...",
                        "tipo_mensaje"  => "error"
                    );

                    echo json_encode($data);
                }
            }
        } catch (\Exception $e) {
            $data = array(
                "titulo" => "Ocurrió un error inesperado.",
                "mensaje"  => 'El Item del Distributivo no se pudo insertar correctamente...Error: ' . $e->getMessage(),
                "tipo_mensaje"  => "error"
            );

            echo json_encode($data);
        }
    }

    public function getByUsuarioId()
    {
        $id_usuario = $_POST['id_usuario'];
        $id_periodo_lectivo = session()->id_periodo_lectivo;
        echo json_encode($this->distributivos->listarAsignaturasAsociadas($id_usuario, $id_periodo_lectivo));
    }

    public function delete()
    {
        try {
            $this->distributivos->delete($_POST['id']);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El Item del Distributivo fue eliminado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        } catch (\Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El Item del Distributivo no se pudo eliminar...Error: " . $e->getMessage(),
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }
}
