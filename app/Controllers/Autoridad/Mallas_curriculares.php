<?php

namespace App\Controllers\Autoridad;

use CodeIgniter\Controller;
use App\Models\Cursos_model;
use App\Models\Asignaturas_model;
use App\Models\Mallas_curriculares_model;

class Mallas_curriculares extends Controller
{

    protected $cursos, $asignaturas, $mallas_curriculares;

    public function __construct()
    {
        $this->cursos = new Cursos_model();
        $this->asignaturas = new Asignaturas_model();
        $this->mallas_curriculares = new Mallas_curriculares_model();
    }

    public function index()
    {
        return view('Autoridad/Mallas_curriculares/index', [
            'cursos' => $this->cursos
                ->join(
                    'sw_especialidad',
                    'sw_especialidad.id_especialidad = sw_curso.id_especialidad'
                )
                ->join(
                    'sw_tipo_educacion',
                    'sw_tipo_educacion.id_tipo_educacion = sw_especialidad.id_tipo_educacion'
                )->orderBy('cu_orden')
                ->findAll(),
            'asignaturas' => $this->asignaturas
                ->join(
                    'sw_area',
                    'sw_area.id_area = sw_asignatura.id_area'
                )->findAll()
        ]);
    }

    public function getByCursoId()
    {
        $id_curso = $_POST['id_curso'];
        echo json_encode($this->mallas_curriculares->listarAsignaturasAsociadas($id_curso));
    }

    public function getMallaCurricularById()
    {
        $id_malla_curricular = $_POST['id_malla_curricular'];
        echo json_encode($this->mallas_curriculares->getMallaCurricularById($id_malla_curricular));
    }

    public function store()
    {
        try {
            $id_curso = $_POST['id_curso'];
            $id_asignatura = $_POST['id_asignatura'];

            if ($this->mallas_curriculares->existeAsociacion($id_curso, $id_asignatura)) {
                $data = array(
                    "titulo"        => "Ocurrió un error inesperado.",
                    "mensaje"       => 'Ya existe la asociación entre la asignatura y el curso en la malla curricular.',
                    "tipo_mensaje"  => "error"
                );

                echo json_encode($data);
            } else {
                $id_periodo_lectivo = session()->id_periodo_lectivo;

                $ma_subtotal = $_POST['ma_horas_presenciales'] + $_POST['ma_horas_autonomas'] + $_POST['ma_horas_tutorias'];

                $this->mallas_curriculares->save([
                    'id_periodo_lectivo'    => $id_periodo_lectivo,
                    'id_curso'              => $id_curso,
                    'id_asignatura'         => $id_asignatura,
                    'ma_horas_presenciales' => $_POST['ma_horas_presenciales'],
                    'ma_horas_autonomas'    => $_POST['ma_horas_autonomas'],
                    'ma_horas_tutorias'     => $_POST['ma_horas_tutorias'],
                    'ma_subtotal'           => $ma_subtotal
                ]);

                $data = array(
                    "titulo" => "Inserción exitosa.",
                    "mensaje"  => 'El Item de la Malla Curricular se insertó correctamente',
                    "tipo_mensaje"  => "success"
                );

                echo json_encode($data);
            }
        } catch (\Exception $e) {
            $data = array(
                "titulo" => "Ocurrió un error inesperado.",
                "mensaje"  => 'El Item de la Malla Curricular no se pudo insertar correctamente...Error: ' . $e->getMessage(),
                "tipo_mensaje"  => "error"
            );

            echo json_encode($data);
        }
    }

    public function update()
    {
        try {
            $this->mallas_curriculares->save([
                'id_malla_curricular'   => $_POST['id_malla_curricular'],
                'ma_horas_presenciales' => $_POST['ma_horas_presenciales'],
                'ma_horas_autonomas'    => $_POST['ma_horas_autonomas'],
                'ma_horas_tutorias'     => $_POST['ma_horas_tutorias'],
            ]);
    
            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El Item de la Malla Curricular fue actualizado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);    
        } catch (\Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El Item de la Malla Curricular no se pudo actualizar...Error: " . $e->getMessage(),
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

    public function delete()
    {
        try {
            $this->mallas_curriculares->delete($_POST['id']);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El Item de la Malla Curricular fue eliminado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        } catch (\Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El Item de la Malla Curricular no se pudo eliminar...Error: " . $e->getMessage(),
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }
}
