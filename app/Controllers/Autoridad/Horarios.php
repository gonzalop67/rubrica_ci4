<?php

namespace App\Controllers\Autoridad;

use CodeIgniter\Controller;
use App\Models\Horarios_model;
use App\Models\Paralelos_model;
use App\Models\Dias_semana_model;
use App\Models\Distributivos_model;

class Horarios extends Controller
{

    protected $dias_semana, $paralelos, $horarios, $distributivos;

    public function __construct()
    {
        $this->horarios = new Horarios_model();
        $this->paralelos = new Paralelos_model();
        $this->dias_semana = new Dias_semana_model();
        $this->distributivos = new Distributivos_model();
    }

    public function index()
    {
        return view('Autoridad/Horarios/index', [
            'paralelos'   => $this->paralelos
                                ->join(
                                    'sw_curso',
                                    'sw_curso.id_curso = sw_paralelo.id_curso'
                                )
                                ->join(
                                    'sw_especialidad',
                                    'sw_especialidad.id_especialidad = sw_curso.id_especialidad'
                                )
                                ->join(
                                    'sw_jornada',
                                    'sw_jornada.id_jornada = sw_paralelo.id_jornada'
                                )
                                ->orderBy('pa_orden')
                                ->findAll(),
            'dias_semana' => $this->dias_semana
                                ->orderBy('ds_ordinal')
                                ->findAll()
        ]);
    }

    public function show()
    {
        $id_paralelo = $_POST['id_paralelo'];
        $id_dia_semana = $_POST['id_dia_semana'];

        echo $this->horarios->listarHorario($id_paralelo, $id_dia_semana);
    }

    public function store()
    {
        try {
            $id_paralelo = trim($_POST['id_paralelo']);
            $id_asignatura = trim($_POST['id_asignatura']);
            $id_dia_semana = trim($_POST['id_dia_semana']);
            $id_hora_clase = trim($_POST['id_hora_clase']);

            if ($this->horarios->existeAsociacion($id_paralelo, $id_dia_semana, $id_hora_clase)) {
                $data = array(
                    "titulo" => "Ocurrió un error inesperado.",
                    "mensaje"  => "Ya existe una Asignatura asociada en esta Hora Clase...",
                    "tipo_mensaje"  => "error"
                );

                echo json_encode($data);
            } elseif ($this->horarios->comprobarCruce($id_paralelo, $id_asignatura, $id_dia_semana, $id_hora_clase)) {
                $data = array(
                    "titulo" => "Ocurrió un error inesperado.",
                    "mensaje"  => "Existe cruce de horario asociado con esta Hora Clase...",
                    "tipo_mensaje"  => "error"
                );

                echo json_encode($data);
            } else {

                //Primero obtengo el id_usuario para luego verificar si existe otra hora con el mismo docente
		        $id_usuario = $this->distributivos->getIdUsuario($id_paralelo, $id_asignatura); 

                $this->horarios->save([
                    'id_paralelo'   => $id_paralelo,
                    'id_asignatura' => $id_asignatura,
                    'id_dia_semana' => $id_dia_semana,
                    'id_hora_clase' => $id_hora_clase,
                    'id_usuario'    => $id_usuario
                ]);

                $data = array(
                    "titulo" => "Inserción exitosa.",
                    "mensaje"  => 'El Item del Horario se insertó correctamente',
                    "tipo_mensaje"  => "success"
                );

                echo json_encode($data);
            }
        } catch (\Exception $e) {
            $data = array(
                "titulo" => "Ocurrió un error inesperado.",
                "mensaje"  => 'El Item del Horario no se pudo insertar correctamente...Error: ' . $e->getMessage(),
                "tipo_mensaje"  => "error"
            );

            echo json_encode($data);
        }
    }

    public function delete()
    {
        try {
            $this->horarios->delete($_POST['id']);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El Item del Horario fue eliminado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);
        } catch (\Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El Item del Horario no se pudo eliminar...Error: " . $e->getMessage(),
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

}
