<?php

namespace App\Controllers\Admin;

use CodeIgniter\Controller;
use App\Models\Paralelos_model;
use App\Models\Cierre_periodos_model;
use App\Models\Aportes_evaluacion_model;
use CodeIgniter\Exceptions\PageNotFoundException;
use Exception;

class Cierre_periodos extends Controller
{

    protected $aportes_evaluacion, $paralelos, $cierre_periodos;

    public function __construct()
    {
        $this->paralelos = new Paralelos_model();
        $this->cierre_periodos = new Cierre_periodos_model();
        $this->aportes_evaluacion = new Aportes_evaluacion_model();

        $this->reglas = [
            'id_paralelo' => [
                'rules' => 'required|is_not_unique[sw_paralelo.id_paralelo]',
                'errors' => [
                    'required'   => 'El campo Paralelo es obligatorio.',
                    'is_not_unique' => 'No existe la opción elegida en la base de datos.'
                ]
            ],
            'id_aporte_evaluacion' => [
                'rules' => 'required|is_not_unique[sw_aporte_evaluacion.id_aporte_evaluacion]',
                'errors' => [
                    'required'   => 'El campo Aporte Evaluación es obligatorio.',
                    'is_not_unique' => 'No existe la opción elegida en la base de datos.'
                ]
            ],
            'ap_fecha_apertura' => [
                'rules' => 'required|valid_date',
                'errors' => [
                    'required'   => 'El campo Fecha de apertura es obligatorio.',
                    'valid_date' => 'El campo Fecha de apertura no contiene un formato válido de fecha.'
                ]
            ],
            'ap_fecha_cierre' => [
                'rules' => 'required|valid_date',
                'errors' => [
                    'required'   => 'El campo Fecha de cierre es obligatorio.',
                    'valid_date' => 'El campo Fecha de cierre no contiene un formato válido de fecha.'
                ]
            ],
        ];
    }

    public function index()
    {
        return view('Admin/Cierre_periodos/index');
    }

    public function show()
    {
        echo json_encode($this->cierre_periodos->getCierresPeriodos());
    }

    public function getCierresPeriodosById()
    {
        $id_aporte_paralelo_cierre = $_POST['id_aporte_paralelo_cierre'];
        echo json_encode($this->cierre_periodos->getCierresPeriodosById($id_aporte_paralelo_cierre));
    }

    public function search()
    {
        $patron = trim($_POST['patron']);
        echo json_encode($this->cierre_periodos->buscarCierresPeriodos($patron));
    }

    public function create()
    {
        return view('Admin/Cierre_periodos/create', [
            'paralelos' => $this->paralelos
                ->join(
                    'sw_curso',
                    'sw_curso.id_curso = sw_paralelo.id_curso'
                )
                ->join(
                    'sw_especialidad',
                    'sw_especialidad.id_especialidad = sw_curso.id_especialidad'
                )
                ->join(
                    'sw_jornada',
                    'sw_jornada.id_jornada = sw_paralelo.id_jornada'
                )
                ->findAll(),
            'aportes_evaluacion' => $this->aportes_evaluacion->findAll()
        ]);
    }

    public function store()
    {
        if (!$this->validate($this->reglas)) {
            return redirect()->back()->withInput()
                ->with('msg', [
                    'type' => 'danger',
                    'icon' => 'ban',
                    'body' => 'Tienes campos incorrectos.'
                ])
                ->with('errors', $this->validator->getErrors());
        }

        // Primero debo comprobar que no exista el registro en la base de datos
        if ($this->cierre_periodos->existeCierrePeriodo($_POST['id_aporte_evaluacion'], $_POST['id_paralelo'])) {
            return redirect()->route('cierre_periodos')->with('msg', [
                'type' => 'danger',
                'icon' => 'ban',
                'body' => 'Ya se encuentra registrado el Cierre de Periodo en la Base de Datos.'
            ]);
        } else {
            try {
                $this->cierre_periodos->save([
                    'id_aporte_evaluacion' => $_POST['id_aporte_evaluacion'],
                    'id_paralelo'          => $_POST['id_paralelo'],
                    'ap_fecha_apertura'    => $_POST['ap_fecha_apertura'],
                    'ap_fecha_cierre'      => $_POST['ap_fecha_cierre'],
                    'ap_estado'            => 'C'
                ]);

                return redirect()->route('cierre_periodos')->with('msg', [
                    'type' => 'success',
                    'icon' => 'check',
                    'body' => 'El Cierre de Periodo fue guardado correctamente.'
                ]);
            } catch (\Exception $e) {
                return redirect()->route('cierre_periodos')->with('msg', [
                    'type' => 'danger',
                    'icon' => 'ban',
                    'body' => 'El Cierre de Periodo no se pudo insertar correctamente...Error: ' . $e->getMessage()
                ]);
            }
        }
    }

    public function edit(string $id)
    {
        if (!$cierre_periodo = $this->cierre_periodos->find($id)) {
            throw PageNotFoundException::forPageNotFound();
        }

        return view('Admin/Cierre_periodos/edit', [
            'cierre_periodo' => $cierre_periodo,
            'paralelos' => $this->paralelos
                ->join(
                    'sw_curso',
                    'sw_curso.id_curso = sw_paralelo.id_curso'
                )
                ->join(
                    'sw_especialidad',
                    'sw_especialidad.id_especialidad = sw_curso.id_especialidad'
                )
                ->join(
                    'sw_jornada',
                    'sw_jornada.id_jornada = sw_paralelo.id_jornada'
                )
                ->findAll(),
            'aportes_evaluacion' => $this->aportes_evaluacion->findAll()
        ]);
    }

    public function update()
    {
        try {
            $this->cierre_periodos->save([
                'id_aporte_paralelo_cierre' => $_POST['id_aporte_paralelo_cierre'],
                'ap_fecha_apertura'         => $_POST['ap_fecha_apertura'],
                'ap_fecha_cierre'           => $_POST['ap_fecha_cierre']
            ]);

            $fechaactual = Date("Y-m-d H:i:s");

            if ($fechaactual > $_POST['ap_fecha_apertura']) { 
                // Si la fecha actual es mayor a la fecha de apertura, actualizo el estado en [A]bierto
                $this->cierre_periodos->save([
                    'id_aporte_paralelo_cierre' => $_POST['id_aporte_paralelo_cierre'],
                    'ap_estado'                 => 'A'
                ]);
            } else {
                // Si la fecha actual es menor a la fecha de cierre, actualizo el estado en [C]errado
                $this->cierre_periodos->save([
                    'id_aporte_paralelo_cierre' => $_POST['id_aporte_paralelo_cierre'],
                    'ap_estado'                 => 'C'
                ]);
            }

            if ($fechaactual > $_POST['ap_fecha_cierre']) { 
                // Si la fecha actual es mayor a la fecha de cierre, actualizo el estado en [A]bierto
                $this->cierre_periodos->save([
                    'id_aporte_paralelo_cierre' => $_POST['id_aporte_paralelo_cierre'],
                    'ap_estado'                 => 'C'
                ]);
            } else {
                // Si la fecha actual es menor a la fecha de cierre, actualizo el estado en [C]errado
                $this->cierre_periodos->save([
                    'id_aporte_paralelo_cierre' => $_POST['id_aporte_paralelo_cierre'],
                    'ap_estado'                 => 'A'
                ]);
            }
    
            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El Cierre de Periodo fue actualizado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);    
        } catch (\Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El Cierre de Periodo no se pudo actualizar...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

    public function delete()
    {
        try {
            $this->cierre_periodos->save([
                'id_aporte_paralelo_cierre' => $_POST['id_aporte_paralelo_cierre'],
                'ap_fecha_apertura'         => $_POST['ap_fecha_apertura'],
                'ap_fecha_cierre'           => $_POST['ap_fecha_cierre']
            ]);

            $fechaactual = Date("Y-m-d H:i:s");

            if ($fechaactual > $_POST['ap_fecha_apertura']) { 
                // Si la fecha actual es mayor a la fecha de apertura, actualizo el estado en [A]bierto
                $this->cierre_periodos->save([
                    'id_aporte_paralelo_cierre' => $_POST['id_aporte_paralelo_cierre'],
                    'ap_estado'                 => 'A'
                ]);
            }

            if ($fechaactual > $_POST['ap_fecha_cierre']) { 
                // Si la fecha actual es mayor a la fecha de cierre, actualizo el estado en [A]bierto
                $this->cierre_periodos->save([
                    'id_aporte_paralelo_cierre' => $_POST['id_aporte_paralelo_cierre'],
                    'ap_estado'                 => 'C'
                ]);
            }
    
            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El Cierre de Periodo fue actualizado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);    
        } catch (\Exception $e) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El Cierre de Periodo no se pudo actualizar...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }
}
