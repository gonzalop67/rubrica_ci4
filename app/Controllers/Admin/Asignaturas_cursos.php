<?php

namespace App\Controllers\Admin;

use CodeIgniter\Controller;
use App\Models\Cursos_model;
use App\Models\Asignaturas_model;
use App\Models\Asignaturas_cursos_model;

class Asignaturas_cursos extends Controller
{

    protected $cursos, $asignaturas, $reglas, $asignaturas_cursos;

    public function __construct()
    {
        $this->cursos = new Cursos_model();
        $this->asignaturas = new Asignaturas_model();
        $this->asignaturas_cursos = new Asignaturas_cursos_model();

        $this->reglas = [
            'id_curso' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'El campo Curso es obligatorio.',
                ]
            ],
            'id_asignatura' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'El campo Asignatura es obligatorio.',
                ]
            ],
        ];
    }

    public function index()
    {
        return view('Admin/Asignaturas_cursos/index', [
            'cursos' => $this->cursos
                ->join(
                    'sw_especialidad',
                    'sw_especialidad.id_especialidad = sw_curso.id_especialidad'
                )
                ->join(
                    'sw_tipo_educacion',
                    'sw_tipo_educacion.id_tipo_educacion = sw_especialidad.id_tipo_educacion'
                )->orderBy('cu_orden')
                ->findAll(),
            'asignaturas' => $this->asignaturas
                ->join(
                    'sw_area',
                    'sw_area.id_area = sw_asignatura.id_area'
                )->findAll()
        ]);
    }

    public function store()
    {
        $id_curso = $_POST['id_curso'];
        $id_asignatura = $_POST['id_asignatura'];

        // Primero comprobar si ya existe la asociación en la bd
        if ($this->asignaturas_cursos->existeAsociacion($id_curso, $id_asignatura)) {
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "Ya existe la asociacion entre el curso y la asignatura seleccionados.",
				"tipo_mensaje" => "error"
			);

			echo json_encode($data);
        } else {
            $this->asignaturas_cursos->save([
                'id_curso' => $_POST['id_curso'],
                'id_asignatura' => $_POST['id_asignatura'],
                'id_periodo_lectivo' => session()->id_periodo_lectivo,
            ]);

            $id_asignatura_curso = $this->asignaturas_cursos->insertID;

            $max = $this->asignaturas_cursos->getMaxId($_POST['id_curso']);

            $this->asignaturas_cursos->save([
                'id_asignatura_curso' => $id_asignatura_curso,
                'ac_orden' => $max + 1
            ]);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "La Asociación Asignatura Curso fue guardada correctamente.",
                "tipo_mensaje" => "success"
            );

            echo json_encode($data);
        }
    }

    public function getByCursoId()
    {
        $id_curso = $_POST['id_curso'];
        echo json_encode($this->asignaturas_cursos->listarAsignaturasAsociadas($id_curso));
    }

    public function getByParaleloId()
    {
        $id_paralelo = $_POST['id_paralelo'];
        echo json_encode($this->asignaturas_cursos->AsignaturasAsociadasParalelo($id_paralelo));
    }

    public function delete()
    {
        try {
            $this->asignaturas_cursos->delete($_POST['id']);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "La Asociación Asignatura Curso fue eliminada correctamente.",
                "tipo_mensaje" => "success"
            );

            echo json_encode($data);
        } catch (\Exception $e) {
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "No se pudo eliminar la Asociación Asignatura Curso.",
				"tipo_mensaje" => "error"
			);

			echo json_encode($data);
        }
    }

    public function saveNewPositions()
    {
        foreach($_POST['positions'] as $position) {
            $index = $position[0];
            $newPosition = $position[1];

            $this->asignaturas_cursos->actualizarOrden($index, $newPosition);
        }
    }
}
