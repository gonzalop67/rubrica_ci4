<?php
namespace App\Controllers\Admin;

use App\Models\Perfiles_model;
use App\Models\Usuarios_model;
use App\Controllers\BaseController;
use App\Models\Usuarios_perfiles_model;
use CodeIgniter\Exceptions\PageNotFoundException;

class Usuarios extends BaseController
{
    protected $usuarios, $perfiles, $usuarios_perfiles;

    public function __construct()
    {
        $this->usuarios = new Usuarios_model();
        $this->perfiles = new Perfiles_model();
        $this->usuarios_perfiles = new Usuarios_perfiles_model();
    }

    public function index()
    {
        return view('Admin/Usuarios/index');
    }

    public function create()
    {
        $datos['perfiles'] = $this->perfiles->orderBy('pe_nombre', 'ASC')->findAll();

        return view('Admin/Usuarios/create', $datos);
    }

    public function search()
    {
        $patron = trim($_POST['patron']);
        echo json_encode($this->usuarios->buscarUsuarios($patron));
    }

    public function show()
    {
        echo json_encode($this->usuarios->listarUsuarios());
    }

    public function store()
    {
        $reglas = [
            'us_titulo' => [
                'rules' => 'required|max_length[7]',
                'errors' => [
                    'required'   => 'El campo Título es obligatorio.',
                    'max_length' => 'El campo Título no debe exceder los 7 caracteres.'
                ]
            ],
            'us_apellidos' => [
                'rules' => 'required|max_length[24]',
                'errors' => [
                    'required'   => 'El campo Apellidos es obligatorio.',
                    'max_length' => 'El campo Apellidos no debe exceder los 24 caracteres.'
                ]
            ],
            'us_nombres' => [
                'rules' => 'required|max_length[24]',
                'errors' => [
                    'required'   => 'El campo Nombres es obligatorio.',
                    'max_length' => 'El campo Nombres no debe exceder los 24 caracteres.'
                ]
            ],
            'us_login' => [
                'rules' => 'required|max_length[16]|is_unique[sw_usuario.us_login]',
                'errors' => [
                    'required'   => 'El campo Usuario es obligatorio.',
                    'max_length' => 'El campo Usuario no debe exceder los 16 caracteres.',
                    'is_unique'  => 'Ya existe este usuario en la base de datos.'
                ]
            ],
            'us_password' => [
                'rules' => 'required|min_length[8]|max_length[16]',
                'errors' => [
                    'required'   => 'El campo Password es obligatorio.',
                    'min_length' => 'El campo Password debe tener como mínimo 8 caracteres.',
                    'max_length' => 'El campo Password no debe exceder los 12 caracteres.'
                ]
            ],
            'us_genero' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'El campo Género es obligatorio.'
                ]
            ],
            'us_activo' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'El campo Activo es obligatorio.'
                ]
            ],
            'id_perfil' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'El campo Perfil es obligatorio.'
                ]
            ],
            'us_foto' => [
                'rules' => 'uploaded[us_foto]|mime_in[us_foto,image/jpg,image/jpeg,image/png]|max_size[us_foto,1024]',
                'errors' => [
                    'uploaded' => 'El campo Imagen es obligatorio.',
                    'mime_in'  => 'Debe cargar un archivo .jpg o .jpeg o .png',
                    'max_size' => 'El archivo de imagen debe tener un tamaño máximo de 1 Mb.'
                ]
            ] 
        ];

        if (!$this->validate($reglas)) 
        {
            return redirect()->back()->withInput()
                ->with('msg', [
                    'type' => 'danger',
                    'icon' => 'ban',
                    'body' => 'Tienes campos incorrectos.'
                ])
                ->with('errors', $this->validator->getErrors());
        }

        $us_password = $this->request->getVar('us_password');
        $us_password = password_hash($us_password, PASSWORD_DEFAULT);

        $us_titulo = $this->request->getVar('us_titulo');
        $us_apellidos = $this->request->getVar('us_apellidos');
        $us_nombres = $this->request->getVar('us_nombres');

        $apellidos = explode(" ", $us_apellidos);
        $nombres = explode(" ", $this->request->getVar('us_nombres'));

        if ($imagen = $this->request->getFile('us_foto')) {
            $nuevoNombre = $imagen->getRandomName();
            $imagen->move('public/uploads/', $nuevoNombre);
            $datos = [
                'us_titulo' => $us_titulo,
                'us_apellidos' => $us_apellidos,
                'us_nombres' => $us_nombres,
                'us_shortname' => $us_titulo . " " . $nombres[0] . " " . $apellidos[0],
                'us_fullname' => $us_apellidos . " " . $us_nombres,
                'us_login' => $this->request->getVar('us_login'),
                'us_password' => $us_password,
                'us_genero' => $this->request->getVar('us_genero'),
                'us_activo' => $this->request->getVar('us_activo'),
                'us_foto' => $nuevoNombre
            ];
            $this->usuarios->insert($datos);
            //Insertar en la tabla sw_usuario_perfil
            $id_usuario = $this->usuarios->insertID;
            foreach ($_POST['id_perfil'] as $id_perfil) {
                $datos = [
                    'id_usuario' => $id_usuario,
                    'id_perfil' => $id_perfil
                ];
                $this->usuarios_perfiles->insert($datos);
            }
        }

        return redirect()->route('usuarios')->with('msg', [
            'type' => 'success',
            'icon' => 'check',
            'body' => 'El Usuario fue creado correctamente.'
        ]);
    }

    public function edit(string $id)
    {
        $model = model('Usuarios_model');
        
        if (!$usuario = $model->find($id)) {
            throw PageNotFoundException::forPageNotFound();
        }

        $datos['usuario'] = $usuario;
        $datos['perfiles'] = $this->perfiles->orderBy('pe_nombre', 'ASC')->findAll();

        //Obtener los perfiles del usuario
        $datos['perfilesUsuario'] = $this->usuarios_perfiles->where('id_usuario', $id)->findAll();

        return view('Admin/Usuarios/edit', $datos);
    }

    public function update()
    {
        $usuario = $this->usuarios->find($_POST['id_usuario']);

        if (trim($_POST['us_login']) != $usuario->us_login) {
            $is_unique = '|is_unique[sw_usuario.us_login]';
        } else {
            $is_unique = '';
        }

        $reglas = [
            'us_titulo' => [
                'rules' => 'required|max_length[7]',
                'errors' => [
                    'required'   => 'El campo Título es obligatorio.',
                    'max_length' => 'El campo Título no debe exceder los 7 caracteres.'
                ]
            ],
            'us_apellidos' => [
                'rules' => 'required|max_length[24]',
                'errors' => [
                    'required'   => 'El campo Apellidos es obligatorio.',
                    'max_length' => 'El campo Apellidos no debe exceder los 24 caracteres.'
                ]
            ],
            'us_nombres' => [
                'rules' => 'required|max_length[24]',
                'errors' => [
                    'required'   => 'El campo Nombres es obligatorio.',
                    'max_length' => 'El campo Nombres no debe exceder los 24 caracteres.'
                ]
            ],
            'us_login' => [
                'rules' => 'required|max_length[16]'.$is_unique,
                'errors' => [
                    'required'   => 'El campo Usuario es obligatorio.',
                    'max_length' => 'El campo Usuario no debe exceder los 16 caracteres.',
                    'is_unique'  => 'Ya existe este usuario en la base de datos.'
                ]
            ],
            'us_genero' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'El campo Género es obligatorio.'
                ]
            ],
            'us_activo' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'El campo Activo es obligatorio.'
                ]
            ],
            'id_perfil' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'El campo Perfil es obligatorio.'
                ]
            ],
            'us_foto' => [
                'rules' => 'uploaded[us_foto]|mime_in[us_foto,image/jpg,image/jpeg,image/png]|max_size[us_foto,1024]',
                'errors' => [
                    'uploaded' => 'El campo Imagen es obligatorio.',
                    'mime_in'  => 'Debe cargar un archivo .jpg o .jpeg o .png',
                    'max_size' => 'El archivo de imagen debe tener un tamaño máximo de 1 Mb.'
                ]
            ] 
        ];

        if (!$this->validate($reglas)) 
        {
            return redirect()->back()->withInput()
                ->with('msg', [
                    'type' => 'danger',
                    'icon' => 'ban',
                    'body' => 'Tienes campos incorrectos.'
                ])
                ->with('errors', $this->validator->getErrors());
        }

        $id_usuario = $this->request->getVar('id_usuario');

        $us_titulo = $this->request->getVar('us_titulo');
        $us_apellidos = $this->request->getVar('us_apellidos');
        $us_nombres = $this->request->getVar('us_nombres');

        $apellidos = explode(" ", $us_apellidos);
        $nombres = explode(" ", $this->request->getVar('us_nombres'));

        if ($imagen = $this->request->getFile('us_foto')) {
            // Primero eliminar el archivo de imagen actual subido al servidor
            $usuario = $this->usuarios->find($id_usuario);
            $us_foto = $usuario->us_foto;

            $ruta = 'public/uploads/' . $us_foto;
            unlink($ruta);

            $nuevoNombre = $imagen->getRandomName();
            $imagen->move('public/uploads/', $nuevoNombre);
            $datos = [
                'id_usuario' => $id_usuario,
                'us_titulo' => $us_titulo,
                'us_apellidos' => $us_apellidos,
                'us_nombres' => $us_nombres,
                'us_shortname' => $us_titulo . " " . $nombres[0] . " " . $apellidos[0],
                'us_fullname' => $us_apellidos . " " . $us_nombres,
                'us_login' => $this->request->getVar('us_login'),
                'us_genero' => $this->request->getVar('us_genero'),
                'us_activo' => $this->request->getVar('us_activo'),
                'us_foto' => $nuevoNombre
            ];

            $this->usuarios->save($datos);

            //Actualizar el password si fue pasado mediante POST
            $us_password = $this->request->getVar('us_password');
            if ($us_password != '') {
                $reglas = [
                    'us_password' => [
                        'rules' => 'min_length[8]|max_length[16]',
                        'errors' => [
                            'min_length' => 'El campo Password debe tener como mínimo 8 caracteres.',
                            'max_length' => 'El campo Password no debe exceder los 12 caracteres.'
                        ]
                    ],
                ];
                if (!$this->validate($reglas)) 
                {
                    return redirect()->back()->withInput()
                        ->with('msg', [
                            'type' => 'danger',
                            'icon' => 'ban',
                            'body' => 'Tienes campos incorrectos.'
                        ])
                        ->with('errors', $this->validator->getErrors());
                }
                $us_password = password_hash($us_password, PASSWORD_DEFAULT);
                $datos = [
                    'id_usuario' => $id_usuario,
                    'us_password' => $us_password
                ];
                $this->usuarios->save($datos);
            }

            //Actualizar los perfiles asociados
            //Primero eliminar los perfiles asociados actualmente
            $this->usuarios_perfiles->where('id_usuario', $id_usuario)->delete();
            //Ahora insertar los perfiles enviados mediante POST
            foreach ($_POST['id_perfil'] as $id_perfil) {
                $datos = [
                    'id_usuario' => $id_usuario,
                    'id_perfil' => $id_perfil
                ];
                $this->usuarios_perfiles->insert($datos);
            } 
        }

        return redirect()->route('usuarios')->with('msg', [
            'type' => 'success',
            'icon' => 'check',
            'body' => 'El Usuario fue actualizado correctamente.'
        ]);
    }
}
