<?php 
namespace App\Controllers\Admin;

use CodeIgniter\Controller;
use App\Models\Aportes_evaluacion_model;
use App\Models\Rubricas_evaluacion_model;
use App\Models\Tipos_asignatura_model;
use CodeIgniter\Exceptions\PageNotFoundException;

class Rubricas_evaluacion extends Controller
{

    protected $aportes_evaluacion, $periodos_evaluacion, $reglas, $rubricas_evaluacion;

    public function __construct()
    {
        $this->aportes_evaluacion = new Aportes_evaluacion_model();
        $this->rubricas_evaluacion = new Rubricas_evaluacion_model();
        $this->tipos_asignatura = new Tipos_asignatura_model();

        $this->reglas = [
            'ru_nombre' => [
                'rules' => 'required|max_length[24]',
                'errors' => [
                    'required'   => 'El campo Nombre es obligatorio.',
                    'max_length' => 'El campo Nombre no debe exceder los 24 caracteres.'
                ]
            ],
            'ru_abreviatura' => [
                'rules' => 'required|max_length[6]',
                'errors' => [
                    'required'   => 'El campo Abreviatura es obligatorio.',
                    'max_length' => 'El campo Abreviatura no debe exceder los 6 caracteres.'
                ]
            ],
            'id_tipo_asignatura' => [
                'rules' => 'required|is_not_unique[sw_tipo_asignatura.id_tipo_asignatura]',
                'errors' => [
                    'required' => 'El campo Tipo de Asignatura es obligatorio.',
                    'is_not_unique' => 'No existe la opción elegida en la base de datos.'
                ]
            ],
            'id_aporte_evaluacion' => [
                'rules' => 'required|is_not_unique[sw_aporte_evaluacion.id_aporte_evaluacion]',
                'errors' => [
                    'required' => 'El campo Aporte de Evaluación es obligatorio.',
                    'is_not_unique' => 'No existe la opción elegida en la base de datos.'
                ]
            ]
        ];

    }

    public function index()
    {
        return view('Admin/Rubricas_evaluacion/index', [
            'rubricas_evaluacion' => $this->rubricas_evaluacion
                                    ->join(
                                        'sw_aporte_evaluacion',
                                        'sw_aporte_evaluacion.id_aporte_evaluacion = sw_rubrica_evaluacion.id_aporte_evaluacion'
                                    )
                                    ->join(
                                        'sw_periodo_evaluacion',
                                        'sw_periodo_evaluacion.id_periodo_evaluacion = sw_aporte_evaluacion.id_periodo_evaluacion'
                                    )
                                    ->orderby('sw_periodo_evaluacion.id_periodo_evaluacion')
                                    ->orderBy('sw_aporte_evaluacion.id_aporte_evaluacion')
                                    ->orderBy('sw_rubrica_evaluacion.id_rubrica_evaluacion')
                                    ->paginate(config('Blog')->regPerPage),
            'pager' => $this->rubricas_evaluacion->pager
        ]);
    }

    public function create()
    {
        $datos['aportes_evaluacion'] = $this->aportes_evaluacion
                                            ->join(
                                                'sw_periodo_evaluacion', 
                                                'sw_periodo_evaluacion.id_periodo_evaluacion = sw_aporte_evaluacion.id_periodo_evaluacion'
                                            )
                                            ->findAll();
        $datos['tipos_asignatura'] = $this->tipos_asignatura->findAll();
        return view('Admin/Rubricas_evaluacion/create', $datos);
    }

    public function store()
    {
        if (!$this->validate($this->reglas)) {
            return redirect()->back()->withInput()
                ->with('msg', [
                    'type' => 'danger',
                    'icon' => 'ban',
                    'body' => 'Tienes campos incorrectos.'
                ])
                ->with('errors', $this->validator->getErrors());
        }

        $this->rubricas_evaluacion->save([
            'ru_nombre' => trim($_POST['ru_nombre']),
            'ru_abreviatura' => trim($_POST['ru_abreviatura']),
            'id_tipo_asignatura' => trim($_POST['id_tipo_asignatura']),
            'id_aporte_evaluacion' => trim($_POST['id_aporte_evaluacion']),
        ]);

        return redirect()->route('rubricas_evaluacion')->with('msg', [
            'type' => 'success',
            'icon' => 'check',
            'body' => 'El Insumo de Evaluación fue guardado correctamente.'
        ]);
    }

    public function edit(string $id)
    {
        if (!$rubrica_evaluacion = $this->rubricas_evaluacion->find($id)) {
            throw PageNotFoundException::forPageNotFound();
        }

        return view('Admin/Rubricas_evaluacion/edit', [
            'rubrica_evaluacion' => $rubrica_evaluacion,
            'tipos_asignatura' => $this->tipos_asignatura->findAll(),
            'aportes_evaluacion' => $this->aportes_evaluacion
                                         ->join(
                                            'sw_periodo_evaluacion', 
                                            'sw_periodo_evaluacion.id_periodo_evaluacion = sw_aporte_evaluacion.id_periodo_evaluacion'
                                         )
                                         ->findAll()
        ]);
    }

    public function update()
    {
        if (!$this->validate($this->reglas)) 
        {
            return redirect()->back()->withInput()
                ->with('msg', [
                    'type' => 'danger',
                    'icon' => 'ban',
                    'body' => 'Tienes campos incorrectos.'
                ])
                ->with('errors', $this->validator->getErrors());
        }

        $this->rubricas_evaluacion->save([
            'id_rubrica_evaluacion' => trim($_POST['id_rubrica_evaluacion']),
            'ru_nombre' => trim($_POST['ru_nombre']),
            'ru_abreviatura' => trim($_POST['ru_abreviatura']),
            'id_tipo_asignatura' => trim($_POST['id_tipo_asignatura']),
            'id_aporte_evaluacion' => trim($_POST['id_aporte_evaluacion']),
        ]);

        return redirect()->route('rubricas_evaluacion')->with('msg', [
            'type' => 'success',
            'icon' => 'check',
            'body' => 'El Aporte de Evaluación fue actualizado correctamente.'
        ]);
    }

    public function delete(string $id)
    {
        try {
            $this->rubricas_evaluacion->delete($id);
    
            return redirect('rubricas_evaluacion')->with('msg', [
                'type' => 'success',
                'icon' => 'check',
                'body' => 'El Aporte de Evaluación fue eliminado correctamente.'
            ]);
        } catch (\Exception $e) {
            return redirect('rubricas_evaluacion')->with('msg', [
                'type' => 'danger',
                'icon' => 'ban',
                'body' => 'El Aporte de Evaluación no se pudo eliminar correctamente...Error: ' . $e->getMessage()
            ]);
        }
    }

}