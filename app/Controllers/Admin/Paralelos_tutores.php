<?php

namespace App\Controllers\Admin;

use CodeIgniter\Controller;
use App\Models\Paralelos_model;
use App\Models\Usuarios_model;
use App\Models\Paralelos_tutores_model;

class Paralelos_tutores extends Controller
{

    protected $paralelos, $usuarios, $reglas, $paralelos_tutores;

    public function __construct()
    {
        $this->paralelos = new Paralelos_model();
        $this->usuarios = new Usuarios_model();
        $this->paralelos_tutores = new Paralelos_tutores_model();

        $this->reglas = [
            'id_paralelo' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'El campo Paralelo es obligatorio.',
                ]
            ],
            'id_usuario' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'El campo Tutor es obligatorio.',
                ]
            ],
        ];
    }

    public function index()
    {
        return view('Admin/Paralelos_tutores/index', [
            'paralelos' => $this->paralelos
                ->join(
                    'sw_curso',
                    'sw_curso.id_curso = sw_paralelo.id_curso'
                )
                ->join(
                    'sw_especialidad',
                    'sw_especialidad.id_especialidad = sw_curso.id_especialidad'
                )
                ->join(
                    'sw_tipo_educacion',
                    'sw_tipo_educacion.id_tipo_educacion = sw_especialidad.id_tipo_educacion'
                )
                ->join(
                    'sw_jornada',
                    'sw_jornada.id_jornada = sw_paralelo.id_jornada'
                )
                ->where('sw_paralelo.id_periodo_lectivo', session()->id_periodo_lectivo)
                ->orderBy('pa_orden')
                ->findAll(),
            'usuarios' => $this->usuarios
                ->join(
                    'sw_usuario_perfil',
                    'sw_usuario.id_usuario = sw_usuario_perfil.id_usuario'
                )
                ->join(
                    'sw_perfil',
                    'sw_perfil.id_perfil = sw_usuario_perfil.id_perfil'
                )
                ->where('sw_perfil.pe_nombre', 'TUTOR')
                ->where('sw_usuario.us_activo', 1)
                ->orderBy('us_apellidos')
                ->findAll()
        ]);
    }

    public function store()
    {
        $id_paralelo = $_POST['id_paralelo'];
        $id_usuario = $_POST['id_usuario'];

        // Primero comprobar si ya existe la asociación en la bd
        if ($this->paralelos_tutores->existeAsociacion($id_paralelo, $id_usuario)) {
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "Ya existe la asociacion entre el paralelo y el tutor seleccionados.",
				"tipo_mensaje" => "error"
			);

			echo json_encode($data);
        } else if ($this->paralelos_tutores->existeAsociacionParaleloTutor($id_paralelo)) {
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "Ya se ha asociado un tutor al paralelo seleccionado.",
				"tipo_mensaje" => "error"
			);

			echo json_encode($data);
        } else {
            $this->paralelos_tutores->save([
                'id_paralelo' => $_POST['id_paralelo'],
                'id_usuario' => $_POST['id_usuario'],
                'id_periodo_lectivo' => session()->id_periodo_lectivo,
            ]);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "La Asociación Paralelo Tutor fue guardada correctamente.",
                "tipo_mensaje" => "success"
            );

            echo json_encode($data);
        }
    }

    public function list()
    {
        $id_periodo_lectivo = session()->id_periodo_lectivo;
        echo json_encode($this->paralelos_tutores->listarParalelosTutores($id_periodo_lectivo));
    }

    public function delete()
    {
        try {
            $this->paralelos_tutores->delete($_POST['id']);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "La Asociación Paralelo Tutor fue eliminada correctamente.",
                "tipo_mensaje" => "success"
            );

            echo json_encode($data);
        } catch (\Exception $e) {
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "No se pudo eliminar la Asociación Paralelo Tutor.",
				"tipo_mensaje" => "error"
			);

			echo json_encode($data);
        }
    }

}
