<?php

namespace App\Controllers\Admin;

use App\Models\Cursos_model;
use App\Models\Jornadas_model;
use App\Models\Paralelos_model;
use App\Controllers\BaseController;
use App\Models\Especialidades_model;
use App\Models\Cierre_periodos_model;
use App\Models\Aportes_evaluacion_model;
use CodeIgniter\Exceptions\PageNotFoundException;

class Paralelos extends BaseController
{
    protected $reglas, $reglas2, $jornadas, $cursos, $especialidades, $paralelos;
    protected $aportes, $aportes_paralelos;

    public function __construct()
    {
        $this->cursos = new Cursos_model();
        $this->jornadas = new Jornadas_model();
        $this->paralelos = new Paralelos_model();
        $this->aportes = new Aportes_evaluacion_model();
        $this->especialidades = new Especialidades_model();
        $this->aportes_paralelos = new Cierre_periodos_model();

        $this->reglas = [
            'pa_nombre' => [
                'rules' => 'required|max_length[5]',
                'errors' => [
                    'required'   => 'El campo Nombre es obligatorio.',
                    'max_length' => 'El campo Nombre no debe exceder los 5 caracteres.'
                ]
            ],
            'id_curso' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'El campo Curso es obligatorio.'
                ]
            ],
            'id_jornada' => [
                'rules' => 'required|is_not_unique[sw_jornada.id_jornada]',
                'errors' => [
                    'required' => 'El campo Jornada es obligatorio.',
                    'is_not_unique' => 'No existe la opción elegida en la base de datos.'
                ]
            ]
        ];
    }

    public function index()
    {
        return view('Admin/Paralelos/index');
    }

    public function getAll()
    {
        $id_periodo_lectivo = session()->id_periodo_lectivo;
        echo $this->paralelos->getAll($id_periodo_lectivo);
    }

    public function show()
    {
        $id_periodo_lectivo = session()->id_periodo_lectivo;
        echo $this->paralelos->listarParalelos($id_periodo_lectivo);
    }

    public function getParalelosById()
    {
        $id_paralelo = $_POST['id_paralelo'];
        echo json_encode($this->paralelos->getParalelosById($id_paralelo));
    }

    public function create()
    {
        $datos['cursos'] = $this->cursos
            ->join(
                'sw_especialidad',
                'sw_especialidad.id_especialidad = sw_curso.id_especialidad'
            )
            ->orderBy('cu_orden', 'ASC')->findAll();

        $datos['jornadas'] = $this->jornadas->orderBy('id_jornada', 'ASC')->findAll();

        return view('Admin/Paralelos/create', $datos);
    }

    public function store()
    {
        if (!$this->validate($this->reglas)) {
            return redirect()->back()->withInput()
                ->with('msg', [
                    'type' => 'danger',
                    'icon' => 'ban',
                    'body' => 'Tienes campos incorrectos.'
                ])
                ->with('errors', $this->validator->getErrors());
        }

        try {

            $id_periodo_lectivo = session()->id_periodo_lectivo;

            $this->paralelos->save([
                'pa_nombre'          => trim($_POST['pa_nombre']),
                'id_curso'           => $_POST['id_curso'],
                'id_jornada'         => $_POST['id_jornada'],
                'id_periodo_lectivo' => $id_periodo_lectivo
            ]);

            $id_paralelo = $this->paralelos->insertID;

            $this->paralelos->save([
                'id_paralelo' => $id_paralelo,
                'pa_orden' => $id_paralelo
            ]);

            // Asociar el paralelo creado con los aportes de evaluación creados para el cierre de periodos...
            $this->paralelos->crearCierresPeriodoLectivo($id_periodo_lectivo);

            return redirect()->route('paralelos')->with('msg', [
                'type' => 'success',
                'icon' => 'check',
                'body' => 'El Paralelo fue guardado correctamente.'
            ]);
        } catch (\Exception $e) {

            return redirect('paralelos')->with('msg', [
                'type' => 'danger',
                'icon' => 'ban',
                'body' => 'El Paralelo no se pudo insertar correctamente...Error: ' . $e->getMessage()
            ]);
        }
    }

    public function edit(string $id)
    {
        if (!$this->paralelos->find($id)) {
            throw PageNotFoundException::forPageNotFound();
        }

        return view('Admin/Paralelos/edit', [
            'paralelo' => $this->paralelos
                ->join(
                    'sw_curso',
                    'sw_curso.id_curso = sw_paralelo.id_curso'
                )
                ->join(
                    'sw_especialidad',
                    'sw_especialidad.id_especialidad = sw_curso.id_especialidad'
                )
                ->find($id),
            'cursos'   => $this->cursos
                ->join(
                    'sw_especialidad',
                    'sw_especialidad.id_especialidad = sw_curso.id_especialidad'
                )
                ->orderBy('cu_orden', 'ASC')->findAll(),
            'jornadas' => $this->jornadas->orderBy('id_jornada', 'ASC')->findAll()
        ]);
    }

    public function update()
    {
        $id_paralelo = $_POST['id_paralelo'];
        $paralelo = $this->paralelos->find($id_paralelo);

        $id_periodo_lectivo = session()->id_periodo_lectivo;

        if ($paralelo->pa_nombre != trim($_POST['pa_nombre']) && 
            $this->paralelos
                 ->existeParaleloPeriodoLectivo(trim($_POST['pa_nombre']), $_POST['id_curso'], $id_periodo_lectivo)) {
            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El nombre del paralelo debe ser único...",
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        } else {
            if ($this->paralelos->save([
                'id_paralelo' => $id_paralelo,
                'pa_nombre'   => trim($_POST['pa_nombre']),
                'id_curso'    => $_POST['id_curso'],
                'id_jornada'  => $_POST['id_jornada'],
            ])) {
                $data = array(
                    "titulo"       => "Operación exitosa.",
                    "mensaje"      => "El paralelo fue actualizado exitosamente.",
                    "tipo_mensaje" => "success"
                );
                echo json_encode($data);
            } else {
                $data = array(
                    "titulo"       => "Ocurrió un error inesperado.",
                    "mensaje"      => "El paralelo no se pudo actualizar...",
                    "tipo_mensaje" => "error"
                );
                echo json_encode($data);
            }
        }
    }

    public function delete()
    {
        try {
                       
            $id_periodo_lectivo = session()->id_periodo_lectivo;
            
            // Des-Asociar el paralelo eliminado con los aportes de evaluación creados para el cierre de periodos...
            $this->paralelos->eliminarCierresParaleloPeriodoLectivo($_POST['id_paralelo'], $id_periodo_lectivo);
            
            $this->paralelos->delete($_POST['id_paralelo']);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "El paralelo fue eliminado exitosamente.",
                "tipo_mensaje" => "success"
            );
            echo json_encode($data);

        } catch (\Exception $e) {

            $data = array(
                "titulo"       => "Ocurrió un error inesperado.",
                "mensaje"      => "El paralelo no se pudo eliminar...Error: " . $e->getMessage(),
                "tipo_mensaje" => "error"
            );
            echo json_encode($data);
        }
    }

    public function saveNewPositions()
    {
        foreach ($_POST['positions'] as $position) {
            $index = $position[0];
            $newPosition = $position[1];

            $this->paralelos->actualizarOrden($index, $newPosition);
        }
    }
}
