<?php

namespace App\Controllers\Admin;

use CodeIgniter\Controller;
use App\Models\Cursos_model;
use App\Models\Curso_superior_model;
use App\Models\Asociar_curso_superior_model;

class Curso_superior extends Controller
{

    protected $cursos, $asociar_curso_superior, $cursos_superiores, $reglas;

    public function __construct()
    {
        $this->cursos = new Cursos_model();
        $this->cursos_superiores = new Curso_superior_model();
        $this->asociar_curso_superior = new Asociar_curso_superior_model();

        $this->reglas = [
            'id_curso_inferior' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'El campo Curso Inferior es obligatorio.',
                ]
            ],
            'id_curso_superior' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'El campo Curso Superior es obligatorio.',
                ]
            ],
        ];
    }

    public function index()
    {
        return view('Admin/Curso_superior/index', [
            'cursos' => $this->cursos
                ->join(
                    'sw_especialidad',
                    'sw_especialidad.id_especialidad = sw_curso.id_especialidad'
                )
                ->join(
                    'sw_tipo_educacion',
                    'sw_tipo_educacion.id_tipo_educacion = sw_especialidad.id_tipo_educacion'
                )->orderBy('cu_orden')
                ->findAll(),
            'cursos_superiores' => $this->cursos_superiores->orderBy('id_curso_superior')->findAll()
        ]);
    }

    public function store()
    {
        $id_curso_superior = $_POST['id_curso_superior'];
        $id_curso_inferior = $_POST['id_curso_inferior'];

        // Primero comprobar si ya existe la asociación en la bd
        if ($this->asociar_curso_superior->existeAsociacion($id_curso_inferior, $id_curso_superior)) {
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "Ya existe la asociacion entre los cursos inferior y superior seleccionados.",
				"tipo_mensaje" => "error"
			);

			echo json_encode($data);
        } else {
            $this->asociar_curso_superior->save([
                'id_curso_inferior' => $_POST['id_curso_inferior'],
                'id_curso_superior' => $_POST['id_curso_superior'],
                'id_periodo_lectivo' => session()->id_periodo_lectivo,
            ]);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "La Asociación Curso Inferior y Superior fue guardada correctamente.",
                "tipo_mensaje" => "success"
            );

            echo json_encode($data);
        }
    }

    public function list()
    {
        $id_periodo_lectivo = session()->id_periodo_lectivo;
        echo $this->asociar_curso_superior->listarCursosSuperiores($id_periodo_lectivo);
    }

    public function delete()
    {
        try {
            $this->asociar_curso_superior->delete($_POST['id']);

            $data = array(
                "titulo"       => "Operación exitosa.",
                "mensaje"      => "La Asociación Curso Inferior y Superior fue eliminada correctamente.",
                "tipo_mensaje" => "success"
            );

            echo json_encode($data);
        } catch (\Exception $e) {
            $data = array(
				"titulo"       => "Ocurrió un error inesperado.",
				"mensaje"      => "No se pudo eliminar la Asociación Curso Inferior y Superior.",
				"tipo_mensaje" => "error"
			);

			echo json_encode($data);
        }
    }

    public function saveNewPositions()
    {
        foreach($_POST['positions'] as $position) {
            $index = $position[0];
            $newPosition = $position[1];

            $this->asociar_curso_superior->actualizarOrden($index, $newPosition);
        }
    }
}
