<?php

namespace App\Controllers\Admin;

use App\Models\Institucion_model;
use App\Controllers\BaseController;
use CodeIgniter\Exceptions\PageNotFoundException;

class Institucion extends BaseController
{

    protected $instituciones;

    public function __construct()
    {
        $this->instituciones = new Institucion_model();
    }

    public function index()
    {
        return view('Admin/Institucion/index');
    }

    public function getData()
    {
        echo json_encode($this->instituciones->find(1));
    }

    public function store()
    {
        if ($this->instituciones->find(1)) {
            $this->instituciones->save([
                'id_institucion' => 1,
                'in_nombre' => trim($_POST['in_nombre']),
                'in_direccion' => trim($_POST['in_direccion']),
                'in_telefono' => trim($_POST['in_telefono']),
                'in_nom_rector' => trim($_POST['in_nom_rector']),
                'in_nom_vicerrector' => trim($_POST['in_nom_vicerrector']),
                'in_nom_secretario' => trim($_POST['in_nom_secretario']),
                'in_url' => trim($_POST['in_url']),
                'in_amie' => trim($_POST['in_amie']),
                'in_ciudad' => trim($_POST['in_ciudad']),
                'in_copiar_y_pegar' => trim($_POST['in_copiar_y_pegar'])
            ]);
        } else {
            $this->instituciones->truncateTable();
            $this->instituciones->save([
                'in_nombre' => trim($_POST['in_nombre']),
                'in_direccion' => trim($_POST['in_direccion']),
                'in_telefono' => trim($_POST['in_telefono']),
                'in_nom_rector' => trim($_POST['in_nom_rector']),
                'in_nom_vicerrector' => trim($_POST['in_nom_vicerrector']),
                'in_nom_secretario' => trim($_POST['in_nom_secretario']),
                'in_url' => trim($_POST['in_url']),
                'in_amie' => trim($_POST['in_amie']),
                'in_ciudad' => trim($_POST['in_ciudad']),
                'in_copiar_y_pegar' => trim($_POST['in_copiar_y_pegar'])
            ]);
        }

        $data = array(
            "titulo"       => "Operación exitosa.",
            "mensaje"      => "Datos de la Institución procesados correctamente.",
            "tipo_mensaje" => "success"
        );

        echo json_encode($data); 
    }

}
