<?php 
namespace App\Controllers\Admin;

use App\Models\Tipos_asignatura_model;
use App\Controllers\BaseController;

class Tipos_asignatura extends BaseController
{
    protected $tipos_asignatura;

    public function __construct()
    {
        $this->tipos_asignatura = new Tipos_asignatura_model();
    }

    public function getAll()
    {
        echo $this->tipos_asignatura->getAll();
    }
}