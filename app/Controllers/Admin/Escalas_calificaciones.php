<?php 
namespace App\Controllers\Admin;

use CodeIgniter\Controller;
use App\Models\Escalas_calificaciones_model;
use CodeIgniter\Exceptions\PageNotFoundException;

class Escalas_calificaciones extends Controller
{

    protected $escalas_calificaciones, $reglas;

    public function __construct()
    {
        $this->escalas_calificaciones = new Escalas_calificaciones_model();

        $this->reglas = [
            'ec_cualitativa' => [
                'rules' => 'required|max_length[64]',
                'errors' => [
                    'required'   => 'El campo Escala Cualitativa es obligatorio.',
                    'max_length' => 'El campo Escala Cualitativa no debe exceder los 64 caracteres.'
                ]
            ],
            'ec_cuantitativa' => [
                'rules' => 'required|max_length[16]',
                'errors' => [
                    'required'   => 'El campo Escala Cuantitativa es obligatorio.',
                    'max_length' => 'El campo Escala Cuantitativa no debe exceder los 16 caracteres.'
                ]
            ],
            'ec_nota_minima' => [
                'rules' => 'required|numeric',
                'errors' => [
                    'required'   => 'El campo Nota Mínima es obligatorio.',
                    'numeric' => 'El campo Nota Mínima no contiene un formato válido numérico.'
                ]
            ],
            'ec_nota_maxima' => [
                'rules' => 'required|numeric',
                'errors' => [
                    'required'   => 'El campo Nota Máxima es obligatorio.',
                    'numeric' => 'El campo Nota Máxima no contiene un formato válido numérico.'
                ]
            ],
            'ec_equivalencia' => [
                'rules' => 'required|max_length[2]',
                'errors' => [
                    'required'   => 'El campo Equivalencia es obligatorio.',
                    'max_length' => 'El campo Equivalencia no debe exceder los 2 caracteres.'
                ]
            ],
        ];
    }

    public function index()
    {
        return view('Admin/Escalas_calificaciones/index', [
            'escalas_calificaciones' => $this->escalas_calificaciones
                                        ->where('id_periodo_lectivo', session()->id_periodo_lectivo)
                                        ->orderBy('ec_orden')
                                        ->findAll()
        ]);
    }

    public function create()
    {
        return view('Admin/Escalas_calificaciones/create');
    }

    public function store()
    {
        if (!$this->validate($this->reglas)) {
            return redirect()->back()->withInput()
                ->with('msg', [
                    'type' => 'danger',
                    'icon' => 'ban',
                    'body' => 'Tienes campos incorrectos.'
                ])
                ->with('errors', $this->validator->getErrors());
        }

        $this->escalas_calificaciones->save([
            'ec_cualitativa' => trim($_POST['ec_cualitativa']),
            'ec_cuantitativa' => trim($_POST['ec_cuantitativa']),
            'ec_nota_minima' => trim($_POST['ec_nota_minima']),
            'ec_nota_maxima' => trim($_POST['ec_nota_maxima']),
            'ec_equivalencia' => trim($_POST['ec_equivalencia']),
            'id_periodo_lectivo' => session()->id_periodo_lectivo,
        ]);

        $id_escala_calificaciones = $this->escalas_calificaciones->insertID;

        $max = $this->escalas_calificaciones->getMaxId(session()->id_periodo_lectivo);

        $this->escalas_calificaciones->save([
            'id_escala_calificaciones' => $id_escala_calificaciones,
            'ec_orden' => $max + 1
        ]);

        return redirect()->route('escalas_calificaciones')->with('msg', [
            'type' => 'success',
            'icon' => 'check',
            'body' => 'El Aporte de Evaluación fue guardado correctamente.'
        ]);
    }

    public function edit(string $id)
    {
        if (!$escala_calificaciones = $this->escalas_calificaciones->find($id)) {
            throw PageNotFoundException::forPageNotFound();
        }

        return view('Admin/Escalas_calificaciones/edit', [
            'escala_calificaciones' => $escala_calificaciones
        ]);
    }

    public function update()
    {
        if (!$this->validate($this->reglas)) 
        {
            return redirect()->back()->withInput()
                ->with('msg', [
                    'type' => 'danger',
                    'icon' => 'ban',
                    'body' => 'Tienes campos incorrectos.'
                ])
                ->with('errors', $this->validator->getErrors());
        }

        $this->escalas_calificaciones->save([
            'id_escala_calificaciones' => $_POST['id_escala_calificaciones'],
            'ec_cualitativa' => trim($_POST['ec_cualitativa']),
            'ec_cuantitativa' => trim($_POST['ec_cuantitativa']),
            'ec_nota_minima' => trim($_POST['ec_nota_minima']),
            'ec_nota_maxima' => trim($_POST['ec_nota_maxima']),
            'ec_equivalencia' => trim($_POST['ec_equivalencia']),
            'id_periodo_lectivo' => session()->id_periodo_lectivo,
        ]);

        return redirect()->route('escalas_calificaciones')->with('msg', [
            'type' => 'success',
            'icon' => 'check',
            'body' => 'La Escala de Calificaciones fue actualizada correctamente.'
        ]);
    }

    public function delete(string $id)
    {
        try {
            $this->escalas_calificaciones->delete($id);
    
            return redirect('escalas_calificaciones')->with('msg', [
                'type' => 'success',
                'icon' => 'check',
                'body' => 'La Escala de Calificaciones fue eliminada correctamente.'
            ]);
        } catch (\Exception $e) {
            return redirect('escalas_calificaciones')->with('msg', [
                'type' => 'danger',
                'icon' => 'ban',
                'body' => 'La Escala de Calificaciones no se pudo eliminar correctamente...Error: ' . $e->getMessage()
            ]);
        }
    }

    public function getAportesByPeriodoId()
    {
        $id_periodo = $_POST['id_periodo'];
        echo json_encode($this->aportes_evaluacion->listarAportesPorPeriodoId($id_periodo));
    }
}