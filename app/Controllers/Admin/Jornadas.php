<?php 
namespace App\Controllers\Admin;

use App\Models\Jornadas_model;
use App\Controllers\BaseController;

class Jornadas extends BaseController
{
    protected $jornadas;

    public function __construct()
    {
        $this->jornadas = new Jornadas_model();
    }

    public function getAll()
    {
        echo $this->jornadas->getAll();
    }
}