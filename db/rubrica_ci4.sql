-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-10-2021 a las 00:17:33
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `rubrica_ci4`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_crear_cierres_periodo_lectivo` (IN `IdPeriodoLectivo` INT)  BEGIN
  DECLARE paralelos_done INT DEFAULT 0; 
  DECLARE IdParalelo INT;
  DECLARE IdAporteEvaluacion INT;
  DECLARE ApEstado VARCHAR(1);
  DECLARE ApFechaApertura DATE;
  DECLARE ApFechaCierre DATE;

  DECLARE cParalelos CURSOR FOR
  SELECT id_paralelo
    FROM sw_paralelo p, 
          sw_curso c, 
          sw_especialidad e, 
          sw_tipo_educacion t
    WHERE p.id_curso = c.id_curso 
      AND c.id_especialidad = e.id_especialidad 
      AND e.id_tipo_educacion = t.id_tipo_educacion  
      AND t.id_periodo_lectivo = IdPeriodoLectivo;

  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET paralelos_done = 1;
  OPEN cParalelos;

  outer_loop: LOOP
    
    FETCH FROM cParalelos INTO IdParalelo;

    IF paralelos_done THEN
        CLOSE cParalelos;
        LEAVE outer_loop;
    END IF;

    INNER_BLOCK: BEGIN
      DECLARE aportes_done INT DEFAULT 0;
      DECLARE cAportesEvaluacion CURSOR FOR
      SELECT a.id_aporte_evaluacion
        FROM sw_aporte_evaluacion a,
              sw_periodo_evaluacion p,
              sw_periodo_lectivo pl
        WHERE a.id_periodo_evaluacion = p.id_periodo_evaluacion
          AND p.id_periodo_lectivo = pl.id_periodo_lectivo
          AND p.id_periodo_lectivo = IdPeriodoLectivo;

      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET aportes_done = 1;
      OPEN cAportesEvaluacion;

      inner_loop: LOOP
        FETCH FROM cAportesEvaluacion INTO IdAporteEvaluacion;

		    IF aportes_done THEN
		      CLOSE cAportesEvaluacion;			
		      LEAVE inner_loop;
		    END IF;

		    SET ApFechaApertura = (
		    SELECT ap_fecha_apertura
		      FROM sw_aporte_evaluacion
		     WHERE id_aporte_evaluacion = IdAporteEvaluacion);

		    SET ApFechaCierre = (
		    SELECT ap_fecha_cierre
		      FROM sw_aporte_evaluacion
		     WHERE id_aporte_evaluacion = IdAporteEvaluacion);

		    IF (NOT EXISTS (SELECT * FROM sw_aporte_paralelo_cierre WHERE id_paralelo = IdParalelo AND id_aporte_evaluacion = IdAporteEvaluacion)) THEN
          INSERT INTO sw_aporte_paralelo_cierre(id_aporte_evaluacion, id_paralelo, ap_estado, ap_fecha_apertura, ap_fecha_cierre)
          values(IdAporteEvaluacion, IdParalelo, 'C', ApFechaApertura, ApFechaCierre); 
        END IF;
            
      END LOOP inner_loop;

    END INNER_BLOCK;

  END LOOP outer_loop;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_eliminar_cierres_aporte_periodo_lectivo` (IN `IdAporteEvaluacion` INT, IN `IdPeriodoLectivo` INT)  BEGIN
  DECLARE aportes_done INT DEFAULT 0; 
  DECLARE IdParalelo INT;

  DECLARE cParalelos CURSOR FOR
    SELECT id_paralelo
      FROM sw_paralelo 
     WHERE id_periodo_lectivo = IdPeriodoLectivo;

  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET aportes_done = 1;
  OPEN cParalelos;

  outer_loop: LOOP
    
    FETCH FROM cParalelos INTO IdParalelo;

    IF aportes_done THEN
        CLOSE cParalelos;
        LEAVE outer_loop;
    END IF;

    DELETE FROM sw_aporte_paralelo_cierre
     WHERE id_aporte_evaluacion = IdAporteEvaluacion
       AND id_paralelo = IdParalelo;
        
  END LOOP outer_loop;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_eliminar_cierres_paralelo_periodo_lectivo` (IN `IdParalelo` INT, IN `IdPeriodoLectivo` INT)  BEGIN
  DECLARE aportes_done INT DEFAULT 0; 
  DECLARE IdAporteEvaluacion INT;

  DECLARE cAportesEvaluacion CURSOR FOR
    SELECT a.id_aporte_evaluacion
      FROM sw_aporte_evaluacion a,
           sw_periodo_evaluacion p,
           sw_periodo_lectivo pl
     WHERE a.id_periodo_evaluacion = p.id_periodo_evaluacion
       AND p.id_periodo_lectivo = pl.id_periodo_lectivo
       AND p.id_periodo_lectivo = IdPeriodoLectivo;

  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET aportes_done = 1;
  OPEN cAportesEvaluacion;

  outer_loop: LOOP
    
    FETCH FROM cAportesEvaluacion INTO IdAporteEvaluacion;

    IF aportes_done THEN
        CLOSE cAportesEvaluacion;
        LEAVE outer_loop;
    END IF;

    DELETE FROM sw_aporte_paralelo_cierre
     WHERE id_aporte_evaluacion = IdAporteEvaluacion
       AND id_paralelo = IdParalelo;
        
  END LOOP outer_loop;

END$$

CREATE DEFINER=`rubrica_ci4`@`localhost` PROCEDURE `sp_insertar_institucion` (IN `In_nombre` VARCHAR(64), IN `In_direccion` VARCHAR(45), IN `In_telefono` VARCHAR(12), IN `In_nom_rector` VARCHAR(45), IN `In_nom_secretario` VARCHAR(45))  NO SQL
BEGIN
	IF (EXISTS (SELECT * FROM sw_institucion)) THEN
		UPDATE sw_institucion
		SET in_nombre = In_nombre,
		in_direccion = In_direccion,
		in_telefono = In_telefono,
		in_nom_rector = In_nom_rector,
		in_nom_secretario = In_nom_secretario;
	ELSE
		INSERT INTO sw_institucion
		SET in_nombre = In_nombre,
		in_direccion = In_direccion,
		in_telefono = In_telefono,
		in_nom_rector = In_nom_rector,
		in_nom_secretario = In_nom_secretario;
	END IF;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` text NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(87, '2021-02-28-001653', 'App\\Database\\Migrations\\SwUsuario', 'default', 'App', 1627246845, 1),
(88, '2021-02-28-015909', 'App\\Database\\Migrations\\SwPerfil', 'default', 'App', 1627246845, 1),
(89, '2021-02-28-021627', 'App\\Database\\Migrations\\SwUsuarioPerfil', 'default', 'App', 1627246845, 1),
(90, '2021-02-28-115231', 'App\\Database\\Migrations\\SwPeriodoEstado', 'default', 'App', 1627246845, 1),
(91, '2021-02-28-115605', 'App\\Database\\Migrations\\SwPeriodoLectivo', 'default', 'App', 1627246846, 1),
(92, '2021-02-28-121938', 'App\\Database\\Migrations\\SwInstitucion', 'default', 'App', 1627246846, 1),
(93, '2021-03-05-120352', 'App\\Database\\Migrations\\SwMenu', 'default', 'App', 1627246846, 1),
(94, '2021-03-05-131213', 'App\\Database\\Migrations\\SwMenuPerfil', 'default', 'App', 1627246846, 1),
(95, '2021-03-13-110840', 'App\\Database\\Migrations\\SwModalidad', 'default', 'App', 1627246846, 1),
(96, '2021-06-11-161524', 'App\\Database\\Migrations\\SwTipoEducacion', 'default', 'App', 1627246846, 1),
(97, '2021-06-12-150407', 'App\\Database\\Migrations\\SwEspecialidad', 'default', 'App', 1627246846, 1),
(98, '2021-06-14-143336', 'App\\Database\\Migrations\\SwCurso', 'default', 'App', 1627246846, 1),
(99, '2021-06-16-133510', 'App\\Database\\Migrations\\SwJornada', 'default', 'App', 1627246846, 1),
(100, '2021-06-16-140624', 'App\\Database\\Migrations\\SwParalelo', 'default', 'App', 1627246846, 1),
(101, '2021-06-17-144118', 'App\\Database\\Migrations\\SwArea', 'default', 'App', 1627246846, 1),
(102, '2021-06-17-223351', 'App\\Database\\Migrations\\SwTipoAsignatura', 'default', 'App', 1627246846, 1),
(103, '2021-06-18-003625', 'App\\Database\\Migrations\\SwAsignatura', 'default', 'App', 1627246846, 1),
(104, '2021-06-23-144722', 'App\\Database\\Migrations\\SwTipoPeriodo', 'default', 'App', 1627246846, 1),
(105, '2021-06-24-112657', 'App\\Database\\Migrations\\SwPeriodoEvaluacion', 'default', 'App', 1627246846, 1),
(106, '2021-06-27-143916', 'App\\Database\\Migrations\\SwTipoAporte', 'default', 'App', 1627246846, 1),
(107, '2021-06-27-194523', 'App\\Database\\Migrations\\SwAporteEvaluacion', 'default', 'App', 1627246846, 1),
(108, '2021-07-17-160343', 'App\\Database\\Migrations\\SwRubricaEvaluacion', 'default', 'App', 1627246846, 1),
(109, '2021-07-25-134820', 'App\\Database\\Migrations\\SwAsignaturaCurso', 'default', 'App', 1627246846, 1),
(110, '2021-08-01-172038', 'App\\Database\\Migrations\\SwAsociarCursoSuperior', 'default', 'App', 1627838752, 2),
(111, '2021-08-02-153335', 'App\\Database\\Migrations\\SwParaleloTutor', 'default', 'App', 1627918661, 3),
(112, '2021-08-03-030247', 'App\\Database\\Migrations\\SwParaleloInspector', 'default', 'App', 1627959881, 4),
(113, '2021-08-06-055917', 'App\\Database\\Migrations\\SwEscalaCalificaciones', 'default', 'App', 1628230935, 5),
(114, '2021-08-15-041551', 'App\\Database\\Migrations\\SwAporteParaleloCierre', 'default', 'App', 1629001516, 6),
(116, '2021-09-13-110124', 'App\\Database\\Migrations\\SwMallaCurricular', 'default', 'App', 1631728418, 7),
(117, '2021-09-21-021046', 'App\\Database\\Migrations\\SwDistributivo', 'default', 'App', 1632190637, 8),
(118, '2021-09-22-210204', 'App\\Database\\Migrations\\SwDiaSemana', 'default', 'App', 1632344841, 9),
(119, '2021-09-25-092726', 'App\\Database\\Migrations\\SwHoraClase', 'default', 'App', 1632562583, 10),
(120, '2021-09-27-075317', 'App\\Database\\Migrations\\SwHoraDia', 'default', 'App', 1632729484, 11),
(121, '2021-10-01-122008', 'App\\Database\\Migrations\\SwHorario', 'default', 'App', 1633091082, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_aporte_evaluacion`
--

CREATE TABLE `sw_aporte_evaluacion` (
  `id_aporte_evaluacion` int(11) UNSIGNED NOT NULL,
  `id_periodo_evaluacion` int(11) UNSIGNED NOT NULL,
  `id_tipo_aporte` int(11) UNSIGNED NOT NULL,
  `ap_nombre` varchar(24) NOT NULL,
  `ap_shortname` varchar(45) NOT NULL,
  `ap_abreviatura` varchar(8) NOT NULL,
  `ap_fecha_apertura` date NOT NULL,
  `ap_fecha_cierre` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_aporte_evaluacion`
--

INSERT INTO `sw_aporte_evaluacion` (`id_aporte_evaluacion`, `id_periodo_evaluacion`, `id_tipo_aporte`, `ap_nombre`, `ap_shortname`, `ap_abreviatura`, `ap_fecha_apertura`, `ap_fecha_cierre`) VALUES
(1, 1, 1, 'PRIMER PARCIAL', '', '1ER.P.', '2020-09-01', '2020-11-06'),
(2, 1, 1, 'SEGUNDO PARCIAL', '', '2DO.P.', '2020-11-09', '2021-01-15'),
(3, 1, 2, 'PROYECTO QUIMESTRAL', '', 'PROY.', '2021-01-18', '2021-01-29'),
(4, 2, 1, 'TERCER PARCIAL', '', '3ER.P.', '2021-02-03', '2021-04-09'),
(5, 2, 1, 'CUARTO PARCIAL', '', '4TO.P.', '2021-04-12', '2021-06-18'),
(6, 2, 2, 'PROYECTO QUIMESTRAL', '', 'PROY.', '2021-06-21', '2021-07-02'),
(7, 3, 3, 'EXAMEN SUPLETORIO', '', 'SUP.', '2021-07-19', '2021-07-23'),
(8, 4, 3, 'EXAMEN REMEDIAL', '', 'REM.', '2021-08-23', '2021-08-27'),
(9, 5, 3, 'EXAMEN DE GRACIA', '', 'GRA.', '2021-08-30', '2021-08-31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_aporte_paralelo_cierre`
--

CREATE TABLE `sw_aporte_paralelo_cierre` (
  `id_aporte_paralelo_cierre` int(11) UNSIGNED NOT NULL,
  `id_aporte_evaluacion` int(11) UNSIGNED NOT NULL,
  `id_paralelo` int(11) UNSIGNED NOT NULL,
  `ap_fecha_apertura` date NOT NULL,
  `ap_fecha_cierre` date NOT NULL,
  `ap_estado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_aporte_paralelo_cierre`
--

INSERT INTO `sw_aporte_paralelo_cierre` (`id_aporte_paralelo_cierre`, `id_aporte_evaluacion`, `id_paralelo`, `ap_fecha_apertura`, `ap_fecha_cierre`, `ap_estado`) VALUES
(1, 1, 1, '2020-09-01', '2020-11-06', 'C'),
(2, 2, 1, '2020-11-09', '2021-01-15', 'C'),
(3, 3, 1, '2021-01-18', '2021-01-29', 'C'),
(4, 4, 1, '2021-02-03', '2021-04-09', 'C'),
(5, 5, 1, '2021-04-12', '2021-06-18', 'C'),
(6, 6, 1, '2021-06-21', '2021-07-02', 'C'),
(7, 7, 1, '2021-07-19', '2021-07-23', 'C'),
(8, 8, 1, '2021-08-23', '2021-08-27', 'C'),
(9, 9, 1, '2021-08-30', '2021-08-31', 'C'),
(10, 1, 2, '2020-09-01', '2020-11-06', 'C'),
(11, 2, 2, '2020-11-09', '2021-01-15', 'C'),
(12, 3, 2, '2021-01-18', '2021-01-29', 'C'),
(13, 4, 2, '2021-02-03', '2021-04-09', 'C'),
(14, 5, 2, '2021-04-12', '2021-06-18', 'C'),
(15, 6, 2, '2021-06-21', '2021-07-02', 'C'),
(16, 7, 2, '2021-07-19', '2021-07-23', 'C'),
(17, 8, 2, '2021-08-23', '2021-08-27', 'C'),
(18, 9, 2, '2021-08-30', '2021-08-31', 'C'),
(19, 1, 3, '2020-09-01', '2020-11-06', 'C'),
(20, 2, 3, '2020-11-09', '2021-01-15', 'C'),
(21, 3, 3, '2021-01-18', '2021-01-29', 'C'),
(22, 4, 3, '2021-02-03', '2021-04-09', 'C'),
(23, 5, 3, '2021-04-12', '2021-06-18', 'C'),
(24, 6, 3, '2021-06-21', '2021-07-02', 'C'),
(25, 7, 3, '2021-07-19', '2021-07-23', 'C'),
(26, 8, 3, '2021-08-23', '2021-08-27', 'C'),
(27, 9, 3, '2021-08-30', '2021-08-31', 'C'),
(28, 1, 4, '2020-09-01', '2020-11-06', 'C'),
(29, 2, 4, '2020-11-09', '2021-01-15', 'C'),
(30, 3, 4, '2021-01-18', '2021-01-29', 'C'),
(31, 4, 4, '2021-02-03', '2021-04-09', 'C'),
(32, 5, 4, '2021-04-12', '2021-06-18', 'C'),
(33, 6, 4, '2021-06-21', '2021-07-02', 'C'),
(34, 7, 4, '2021-07-19', '2021-07-23', 'C'),
(35, 8, 4, '2021-08-23', '2021-08-27', 'C'),
(36, 9, 4, '2021-08-30', '2021-08-31', 'C'),
(37, 1, 5, '2020-09-01', '2020-11-06', 'C'),
(38, 2, 5, '2020-11-09', '2021-01-15', 'C'),
(39, 3, 5, '2021-01-18', '2021-01-29', 'C'),
(40, 4, 5, '2021-02-03', '2021-04-09', 'C'),
(41, 5, 5, '2021-04-12', '2021-06-18', 'C'),
(42, 6, 5, '2021-06-21', '2021-07-02', 'C'),
(43, 7, 5, '2021-07-19', '2021-07-23', 'C'),
(44, 8, 5, '2021-08-23', '2021-08-27', 'C'),
(45, 9, 5, '2021-08-30', '2021-08-31', 'C'),
(46, 1, 6, '2020-09-01', '2020-11-06', 'C'),
(47, 2, 6, '2020-11-09', '2021-01-15', 'C'),
(48, 3, 6, '2021-01-18', '2021-01-29', 'C'),
(49, 4, 6, '2021-02-03', '2021-04-09', 'C'),
(50, 5, 6, '2021-04-12', '2021-06-18', 'C'),
(51, 6, 6, '2021-06-21', '2021-07-02', 'C'),
(52, 7, 6, '2021-07-19', '2021-07-23', 'C'),
(53, 8, 6, '2021-08-23', '2021-08-27', 'C'),
(54, 9, 6, '2021-08-30', '2021-08-31', 'C'),
(55, 1, 7, '2020-09-01', '2020-11-06', 'C'),
(56, 2, 7, '2020-11-09', '2021-01-15', 'C'),
(57, 3, 7, '2021-01-18', '2021-01-29', 'C'),
(58, 4, 7, '2021-02-03', '2021-04-09', 'C'),
(59, 5, 7, '2021-04-12', '2021-06-18', 'C'),
(60, 6, 7, '2021-06-21', '2021-07-02', 'C'),
(61, 7, 7, '2021-07-19', '2021-07-23', 'C'),
(62, 8, 7, '2021-08-23', '2021-08-27', 'C'),
(63, 9, 7, '2021-08-30', '2021-08-31', 'C'),
(64, 1, 8, '2020-09-01', '2020-11-06', 'C'),
(65, 2, 8, '2020-11-09', '2021-01-15', 'C'),
(66, 3, 8, '2021-01-18', '2021-01-29', 'C'),
(67, 4, 8, '2021-02-03', '2021-04-09', 'C'),
(68, 5, 8, '2021-04-12', '2021-06-18', 'C'),
(69, 6, 8, '2021-06-21', '2021-07-02', 'C'),
(70, 7, 8, '2021-07-19', '2021-07-23', 'C'),
(71, 8, 8, '2021-08-23', '2021-08-27', 'C'),
(72, 9, 8, '2021-08-30', '2021-08-31', 'C'),
(73, 1, 9, '2020-09-01', '2020-11-06', 'C'),
(74, 2, 9, '2020-11-09', '2021-01-15', 'C'),
(75, 3, 9, '2021-01-18', '2021-01-29', 'C'),
(76, 4, 9, '2021-02-03', '2021-04-09', 'C'),
(77, 5, 9, '2021-04-12', '2021-06-18', 'C'),
(78, 6, 9, '2021-06-21', '2021-07-02', 'C'),
(79, 7, 9, '2021-07-19', '2021-07-23', 'C'),
(80, 8, 9, '2021-08-23', '2021-08-27', 'C'),
(81, 9, 9, '2021-08-30', '2021-08-31', 'C'),
(82, 1, 10, '2020-09-01', '2020-11-06', 'C'),
(83, 2, 10, '2020-11-09', '2021-01-15', 'C'),
(84, 3, 10, '2021-01-18', '2021-01-29', 'C'),
(85, 4, 10, '2021-02-03', '2021-04-09', 'C'),
(86, 5, 10, '2021-04-12', '2021-06-18', 'C'),
(87, 6, 10, '2021-06-21', '2021-07-02', 'C'),
(88, 7, 10, '2021-07-19', '2021-07-23', 'C'),
(89, 8, 10, '2021-08-23', '2021-08-27', 'C'),
(90, 9, 10, '2021-08-30', '2021-08-31', 'C'),
(91, 1, 11, '2020-09-01', '2020-11-06', 'C'),
(92, 2, 11, '2020-11-09', '2021-01-15', 'C'),
(93, 3, 11, '2021-01-18', '2021-01-29', 'C'),
(94, 4, 11, '2021-02-03', '2021-04-09', 'C'),
(95, 5, 11, '2021-04-12', '2021-06-18', 'C'),
(96, 6, 11, '2021-06-21', '2021-07-02', 'C'),
(97, 7, 11, '2021-07-19', '2021-07-23', 'C'),
(98, 8, 11, '2021-08-23', '2021-08-27', 'C'),
(99, 9, 11, '2021-08-30', '2021-08-31', 'C'),
(100, 1, 12, '2020-09-01', '2020-11-06', 'C'),
(101, 2, 12, '2020-11-09', '2021-01-15', 'C'),
(102, 3, 12, '2021-01-18', '2021-01-29', 'C'),
(103, 4, 12, '2021-02-03', '2021-04-09', 'C'),
(104, 5, 12, '2021-04-12', '2021-06-18', 'C'),
(105, 6, 12, '2021-06-21', '2021-07-02', 'C'),
(106, 7, 12, '2021-07-19', '2021-07-23', 'C'),
(107, 8, 12, '2021-08-23', '2021-08-27', 'C'),
(108, 9, 12, '2021-08-30', '2021-08-31', 'C'),
(109, 1, 13, '2020-09-01', '2020-11-06', 'C'),
(110, 2, 13, '2020-11-09', '2021-01-15', 'C'),
(111, 3, 13, '2021-01-18', '2021-01-29', 'C'),
(112, 4, 13, '2021-02-03', '2021-04-09', 'C'),
(113, 5, 13, '2021-04-12', '2021-06-18', 'C'),
(114, 6, 13, '2021-06-21', '2021-07-02', 'C'),
(115, 7, 13, '2021-07-19', '2021-07-23', 'C'),
(116, 8, 13, '2021-08-23', '2021-08-27', 'C'),
(117, 9, 13, '2021-08-30', '2021-08-31', 'C'),
(118, 1, 14, '2020-09-01', '2020-11-06', 'C'),
(119, 2, 14, '2020-11-09', '2021-01-15', 'C'),
(120, 3, 14, '2021-01-18', '2021-01-29', 'C'),
(121, 4, 14, '2021-02-03', '2021-04-09', 'C'),
(122, 5, 14, '2021-04-12', '2021-06-18', 'C'),
(123, 6, 14, '2021-06-21', '2021-07-02', 'C'),
(124, 7, 14, '2021-07-19', '2021-07-23', 'C'),
(125, 8, 14, '2021-08-23', '2021-08-27', 'C'),
(126, 9, 14, '2021-08-30', '2021-08-31', 'C'),
(127, 1, 15, '2020-09-01', '2020-11-06', 'C'),
(128, 2, 15, '2020-11-09', '2021-01-15', 'C'),
(129, 3, 15, '2021-01-18', '2021-01-29', 'C'),
(130, 4, 15, '2021-02-03', '2021-04-09', 'C'),
(131, 5, 15, '2021-04-12', '2021-06-18', 'C'),
(132, 6, 15, '2021-06-21', '2021-07-02', 'C'),
(133, 7, 15, '2021-07-19', '2021-07-23', 'C'),
(134, 8, 15, '2021-08-23', '2021-08-27', 'C'),
(135, 9, 15, '2021-08-30', '2021-08-31', 'C'),
(136, 1, 16, '2020-09-01', '2020-11-06', 'C'),
(137, 2, 16, '2020-11-09', '2021-01-15', 'C'),
(138, 3, 16, '2021-01-18', '2021-01-29', 'C'),
(139, 4, 16, '2021-02-03', '2021-04-09', 'C'),
(140, 5, 16, '2021-04-12', '2021-06-18', 'C'),
(141, 6, 16, '2021-06-21', '2021-07-02', 'C'),
(142, 7, 16, '2021-07-19', '2021-07-23', 'C'),
(143, 8, 16, '2021-08-23', '2021-08-27', 'C'),
(144, 9, 16, '2021-08-30', '2021-08-31', 'C'),
(170, 1, 19, '2020-09-01', '2020-11-06', 'C'),
(171, 2, 19, '2020-11-09', '2021-01-15', 'C'),
(172, 3, 19, '2021-01-18', '2021-01-29', 'C'),
(173, 4, 19, '2021-02-03', '2021-04-09', 'C'),
(174, 5, 19, '2021-04-12', '2021-06-18', 'C'),
(175, 6, 19, '2021-06-21', '2021-07-02', 'C'),
(176, 7, 19, '2021-07-19', '2021-07-23', 'C'),
(177, 8, 19, '2021-08-23', '2021-08-27', 'C'),
(178, 9, 19, '2021-08-30', '2021-08-31', 'C');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_area`
--

CREATE TABLE `sw_area` (
  `id_area` int(11) UNSIGNED NOT NULL,
  `ar_nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_area`
--

INSERT INTO `sw_area` (`id_area`, `ar_nombre`) VALUES
(1, 'CIENCIAS NATURALES'),
(2, 'CIENCIAS SOCIALES'),
(3, 'EDUCACION CULTURAL Y ARTISTICA'),
(4, 'EDUCACION FISICA'),
(5, 'LENGUA EXTRANJERA'),
(6, 'LENGUA Y LITERATURA'),
(7, 'MATEMATICA'),
(8, 'MODULO INTER-ÁREAS'),
(9, 'PROYECTOS ESCOLARES'),
(10, 'CONTABILIDAD'),
(11, 'INFORMATICA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_asignatura`
--

CREATE TABLE `sw_asignatura` (
  `id_asignatura` int(11) UNSIGNED NOT NULL,
  `id_area` int(11) UNSIGNED NOT NULL,
  `id_tipo_asignatura` int(11) UNSIGNED NOT NULL,
  `as_nombre` varchar(84) NOT NULL,
  `as_abreviatura` varchar(12) NOT NULL,
  `as_shortname` varchar(45) NOT NULL,
  `as_curricular` int(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_asignatura`
--

INSERT INTO `sw_asignatura` (`id_asignatura`, `id_area`, `id_tipo_asignatura`, `as_nombre`, `as_abreviatura`, `as_shortname`, `as_curricular`) VALUES
(1, 1, 1, 'BIOLOGIA', 'BIO', '', 1),
(2, 1, 1, 'CIENCIAS NATURALES', 'CCNN', '', 1),
(3, 1, 1, 'FISICA', 'FIS', '', 1),
(4, 1, 1, 'QUIMICA', 'QUIM', '', 1),
(5, 2, 1, 'EDUCACION PARA LA CIUDADANIA', 'EDU.C.', '', 1),
(6, 2, 1, 'ESTUDIOS SOCIALES', 'EESS', '', 1),
(7, 2, 1, 'FILOSOFIA', 'FILO', '', 1),
(8, 2, 1, 'HISTORIA', 'HIST', '', 1),
(9, 10, 1, 'CONTABILIDAD DE COSTOS', 'COSTOS', '', 1),
(10, 10, 1, 'CONTABILIDAD GENERAL', 'CONTA', '', 1),
(11, 10, 1, 'GESTION DEL TALENTO HUMANO', 'G.TAL.H.', '', 1),
(12, 10, 1, 'PAQUETES CONTABLES Y TRIBUTARIOS', 'PAQ.CON.', '', 1),
(13, 10, 1, 'FORMACION Y ORIENTACION LABORAL', 'FOL', '', 1),
(14, 10, 1, 'TRIBUTACION', 'TRIB.', '', 1),
(15, 10, 1, 'CONTABILIDAD BANCARIA', 'CON.BAN.', '', 1),
(16, 3, 1, 'EDUCACION CULTURAL Y ARTISTICA', 'ECA', '', 1),
(17, 11, 1, 'APLICACIONES OFIMATICAS LOCALES Y EN LINEA', 'APL.OF.', '', 1),
(18, 11, 1, 'FORMACION Y ORIENTACION LABORAL', 'FOL', '', 1),
(19, 11, 1, 'PROGRAMACION Y BASES DE DATOS', 'PROG.', '', 1),
(20, 11, 1, 'SISTEMAS OPERATIVOS Y REDES', 'SIS.OP.', '', 1),
(21, 11, 1, 'SOPORTE TECNICO', 'SOP.TEC.', '', 1),
(22, 6, 1, 'LENGUA Y LITERATURA', 'LENGUA', '', 1),
(23, 7, 1, 'MATEMATICA', 'MATE', '', 1),
(24, 8, 1, 'EMPRENDIMIENTO Y GESTION', 'EMPRE', '', 1),
(25, 5, 1, 'INGLES', 'ING', '', 1),
(26, 11, 1, 'DISEÑO Y DESARROLLO WEB', 'DIS.WEB', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_asignatura_curso`
--

CREATE TABLE `sw_asignatura_curso` (
  `id_asignatura_curso` int(11) UNSIGNED NOT NULL,
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `id_curso` int(11) UNSIGNED NOT NULL,
  `id_asignatura` int(11) UNSIGNED NOT NULL,
  `ac_orden` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_asignatura_curso`
--

INSERT INTO `sw_asignatura_curso` (`id_asignatura_curso`, `id_periodo_lectivo`, `id_curso`, `id_asignatura`, `ac_orden`) VALUES
(1, 1, 1, 22, 1),
(2, 1, 1, 23, 2),
(3, 1, 1, 6, 3),
(4, 1, 1, 2, 4),
(5, 1, 1, 16, 5),
(6, 1, 1, 25, 6),
(7, 1, 2, 22, 1),
(8, 1, 2, 23, 2),
(9, 1, 2, 6, 3),
(10, 1, 2, 2, 4),
(11, 1, 2, 16, 5),
(12, 1, 2, 25, 6),
(13, 1, 3, 22, 1),
(14, 1, 3, 23, 2),
(15, 1, 3, 6, 3),
(16, 1, 3, 2, 4),
(17, 1, 3, 16, 5),
(18, 1, 3, 25, 6),
(20, 1, 4, 23, 1),
(21, 1, 4, 3, 2),
(22, 1, 4, 4, 3),
(23, 1, 4, 1, 4),
(24, 1, 4, 8, 5),
(25, 1, 4, 5, 6),
(26, 1, 4, 7, 7),
(27, 1, 4, 22, 8),
(28, 1, 4, 25, 9),
(29, 1, 4, 24, 10),
(30, 1, 5, 23, 1),
(31, 1, 5, 3, 2),
(32, 1, 5, 4, 3),
(33, 1, 5, 1, 4),
(34, 1, 5, 8, 5),
(35, 1, 5, 5, 6),
(36, 1, 5, 7, 7),
(37, 1, 5, 22, 8),
(38, 1, 5, 25, 9),
(39, 1, 5, 24, 10),
(40, 1, 6, 23, 1),
(41, 1, 6, 3, 2),
(42, 1, 6, 4, 3),
(43, 1, 6, 1, 4),
(44, 1, 6, 8, 5),
(45, 1, 6, 22, 6),
(46, 1, 6, 25, 7),
(47, 1, 6, 24, 8),
(48, 1, 7, 23, 1),
(49, 1, 7, 3, 2),
(50, 1, 7, 4, 3),
(51, 1, 7, 1, 4),
(52, 1, 7, 8, 5),
(53, 1, 7, 5, 6),
(54, 1, 7, 7, 7),
(55, 1, 7, 22, 8),
(56, 1, 7, 25, 9),
(57, 1, 7, 24, 10),
(58, 1, 7, 10, 11),
(59, 1, 7, 14, 12),
(60, 1, 7, 12, 13),
(61, 1, 8, 23, 1),
(62, 1, 8, 3, 2),
(63, 1, 8, 4, 3),
(64, 1, 8, 1, 4),
(65, 1, 8, 8, 5),
(66, 1, 8, 5, 6),
(67, 1, 8, 7, 7),
(68, 1, 8, 22, 8),
(69, 1, 8, 25, 9),
(70, 1, 8, 24, 10),
(71, 1, 8, 10, 11),
(72, 1, 8, 14, 12),
(73, 1, 8, 12, 13),
(74, 1, 9, 23, 1),
(75, 1, 9, 3, 2),
(76, 1, 9, 4, 3),
(77, 1, 9, 1, 4),
(78, 1, 9, 8, 5),
(79, 1, 9, 22, 6),
(80, 1, 9, 25, 7),
(81, 1, 9, 24, 8),
(82, 1, 9, 10, 9),
(83, 1, 9, 9, 10),
(84, 1, 9, 15, 11),
(85, 1, 9, 11, 12),
(86, 1, 9, 12, 13),
(87, 1, 9, 13, 14),
(88, 1, 10, 23, 1),
(89, 1, 10, 3, 2),
(90, 1, 10, 4, 3),
(91, 1, 10, 1, 4),
(92, 1, 10, 8, 5),
(93, 1, 10, 5, 6),
(94, 1, 10, 7, 7),
(95, 1, 10, 22, 8),
(96, 1, 10, 25, 9),
(97, 1, 10, 24, 10),
(98, 1, 10, 17, 11),
(99, 1, 10, 20, 12),
(100, 1, 10, 19, 13),
(101, 1, 10, 21, 14),
(102, 1, 11, 23, 1),
(103, 1, 11, 3, 2),
(104, 1, 11, 4, 3),
(105, 1, 11, 1, 4),
(106, 1, 11, 8, 5),
(107, 1, 11, 5, 6),
(108, 1, 11, 7, 7),
(109, 1, 11, 22, 8),
(110, 1, 11, 25, 9),
(111, 1, 11, 24, 10),
(112, 1, 11, 20, 11),
(113, 1, 11, 19, 12),
(114, 1, 11, 21, 13),
(115, 1, 11, 26, 14),
(116, 1, 12, 23, 1),
(117, 1, 12, 3, 2),
(118, 1, 12, 4, 3),
(119, 1, 12, 1, 4),
(120, 1, 12, 8, 5),
(121, 1, 12, 22, 6),
(122, 1, 12, 25, 7),
(123, 1, 12, 24, 8),
(124, 1, 12, 17, 9),
(125, 1, 12, 20, 10),
(126, 1, 12, 19, 11),
(127, 1, 12, 21, 12),
(128, 1, 12, 26, 13),
(129, 1, 12, 18, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_asociar_curso_superior`
--

CREATE TABLE `sw_asociar_curso_superior` (
  `id_asociar_curso_superior` int(11) UNSIGNED NOT NULL,
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `id_curso_inferior` int(11) UNSIGNED NOT NULL,
  `id_curso_superior` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_asociar_curso_superior`
--

INSERT INTO `sw_asociar_curso_superior` (`id_asociar_curso_superior`, `id_periodo_lectivo`, `id_curso_inferior`, `id_curso_superior`) VALUES
(1, 1, 1, 2),
(2, 1, 2, 3),
(3, 1, 3, 4),
(4, 1, 4, 5),
(5, 1, 5, 6),
(6, 1, 7, 8),
(7, 1, 8, 9),
(8, 1, 10, 11),
(9, 1, 11, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_curso`
--

CREATE TABLE `sw_curso` (
  `id_curso` int(11) UNSIGNED NOT NULL,
  `id_especialidad` int(11) UNSIGNED NOT NULL,
  `id_curso_superior` int(11) UNSIGNED NOT NULL,
  `cu_nombre` varchar(128) NOT NULL,
  `cu_shortname` varchar(45) NOT NULL,
  `cu_abreviatura` varchar(5) NOT NULL,
  `cu_orden` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `quien_inserta_comp` int(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_curso`
--

INSERT INTO `sw_curso` (`id_curso`, `id_especialidad`, `id_curso_superior`, `cu_nombre`, `cu_shortname`, `cu_abreviatura`, `cu_orden`, `quien_inserta_comp`) VALUES
(1, 4, 0, 'OCTAVO', 'OCTAVO', '8vo.', 1, 0),
(2, 4, 0, 'NOVENO', 'NOVENO', '9no.', 2, 0),
(3, 4, 0, 'DECIMO', 'DECIMO', '10mo.', 3, 0),
(4, 5, 0, 'PRIMER CURSO', 'PRIMERO CIENCIAS', '1ero.', 4, 0),
(5, 5, 0, 'SEGUNDO CURSO', 'SEGUNDO CIENCIAS', '2do.', 5, 0),
(6, 5, 0, 'TERCER CURSO', 'TERCERO CIENCIAS', '3ro.', 6, 0),
(7, 6, 0, 'PRIMER CURSO', 'PRIMERO CONTABILIDAD', '1ero.', 7, 0),
(8, 6, 0, 'SEGUNDO CURSO', 'SEGUNDO CONTABILIDAD', '2do.', 8, 0),
(9, 6, 0, 'TERCER CURSO', 'TERCERO CONTABILIDAD', '3ro.', 9, 0),
(10, 7, 0, 'PRIMER CURSO', 'PRIMERO INFORMATICA', '1ero.', 10, 0),
(11, 7, 0, 'SEGUNDO CURSO', 'SEGUNDO INFORMATICA', '2do.', 11, 0),
(12, 7, 0, 'TERCER CURSO', 'TERCERO INFORMATICA', '3ro.', 12, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_dia_semana`
--

CREATE TABLE `sw_dia_semana` (
  `id_dia_semana` int(11) UNSIGNED NOT NULL,
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `ds_nombre` varchar(10) NOT NULL,
  `ds_ordinal` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_dia_semana`
--

INSERT INTO `sw_dia_semana` (`id_dia_semana`, `id_periodo_lectivo`, `ds_nombre`, `ds_ordinal`) VALUES
(1, 1, 'LUNES', 1),
(2, 1, 'MARTES', 2),
(3, 1, 'MIERCOLES', 3),
(4, 1, 'JUEVES', 4),
(5, 1, 'VIERNES', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_distributivo`
--

CREATE TABLE `sw_distributivo` (
  `id_distributivo` int(11) UNSIGNED NOT NULL,
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `id_malla_curricular` int(11) UNSIGNED NOT NULL,
  `id_paralelo` int(11) UNSIGNED NOT NULL,
  `id_asignatura` int(11) UNSIGNED NOT NULL,
  `id_usuario` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_distributivo`
--

INSERT INTO `sw_distributivo` (`id_distributivo`, `id_periodo_lectivo`, `id_malla_curricular`, `id_paralelo`, `id_asignatura`, `id_usuario`) VALUES
(1, 1, 20, 5, 3, 5),
(2, 1, 20, 6, 3, 5),
(3, 1, 48, 11, 3, 5),
(4, 1, 88, 14, 3, 5),
(5, 1, 30, 7, 3, 5),
(6, 1, 30, 8, 3, 5),
(7, 1, 61, 12, 3, 5),
(8, 1, 102, 15, 3, 5),
(9, 1, 40, 9, 3, 5),
(10, 1, 40, 10, 3, 5),
(11, 1, 74, 13, 3, 5),
(12, 1, 116, 16, 3, 5),
(13, 1, 19, 5, 23, 6),
(14, 1, 19, 6, 23, 6),
(15, 1, 47, 11, 23, 6),
(16, 1, 87, 14, 23, 6),
(17, 1, 85, 13, 12, 6),
(18, 1, 99, 14, 19, 6),
(19, 1, 100, 14, 21, 6),
(20, 1, 111, 15, 20, 6),
(21, 1, 113, 15, 21, 6),
(22, 1, 123, 16, 17, 6),
(23, 1, 124, 16, 20, 6),
(24, 1, 126, 16, 21, 6),
(25, 1, 21, 5, 4, 7),
(26, 1, 21, 6, 4, 7),
(27, 1, 49, 11, 4, 7),
(28, 1, 75, 13, 4, 7),
(30, 1, 31, 7, 4, 7),
(31, 1, 31, 8, 4, 7),
(32, 1, 62, 12, 4, 7),
(33, 1, 103, 15, 4, 7),
(34, 1, 41, 9, 4, 7),
(35, 1, 41, 10, 4, 7),
(36, 1, 117, 16, 4, 7),
(37, 1, 9, 2, 6, 8),
(38, 1, 15, 3, 6, 8),
(39, 1, 15, 4, 6, 8),
(40, 1, 23, 5, 8, 8),
(41, 1, 25, 5, 7, 8),
(42, 1, 23, 6, 8, 8),
(43, 1, 25, 6, 7, 8),
(44, 1, 51, 11, 8, 8),
(45, 1, 53, 11, 7, 8),
(46, 1, 91, 14, 8, 8),
(47, 1, 93, 14, 7, 8),
(48, 1, 35, 7, 7, 8),
(49, 1, 35, 8, 7, 8),
(50, 1, 66, 12, 7, 8),
(51, 1, 107, 15, 7, 8),
(52, 1, 1, 19, 22, 9),
(53, 1, 13, 3, 22, 9),
(54, 1, 26, 5, 22, 9),
(55, 1, 26, 6, 22, 9),
(56, 1, 54, 11, 22, 9),
(57, 1, 94, 14, 22, 9),
(58, 1, 36, 7, 22, 9),
(59, 1, 36, 8, 22, 9),
(60, 1, 67, 12, 22, 9),
(61, 1, 108, 15, 22, 9),
(62, 1, 3, 19, 6, 10),
(63, 1, 24, 5, 5, 10),
(64, 1, 24, 6, 5, 10),
(65, 1, 52, 11, 5, 10),
(66, 1, 92, 14, 5, 10),
(67, 1, 33, 7, 8, 10),
(68, 1, 33, 8, 8, 10),
(69, 1, 64, 12, 8, 10),
(70, 1, 105, 15, 8, 10),
(71, 1, 34, 7, 5, 10),
(72, 1, 34, 8, 5, 10),
(73, 1, 65, 12, 5, 10),
(74, 1, 106, 15, 5, 10),
(75, 1, 43, 9, 8, 10),
(76, 1, 43, 10, 8, 10),
(77, 1, 77, 13, 8, 10),
(78, 1, 119, 16, 8, 10),
(79, 1, 2, 1, 23, 2),
(80, 1, 8, 2, 23, 2),
(81, 1, 2, 19, 23, 11),
(82, 1, 29, 7, 23, 11),
(83, 1, 29, 8, 23, 11),
(84, 1, 60, 12, 23, 11),
(85, 1, 101, 15, 23, 11),
(86, 1, 39, 9, 23, 11),
(87, 1, 39, 10, 23, 11),
(88, 1, 73, 13, 23, 11),
(89, 1, 115, 16, 23, 11),
(90, 1, 14, 4, 23, 1),
(92, 1, 59, 11, 12, 1),
(93, 1, 72, 12, 12, 1),
(94, 1, 97, 14, 17, 1),
(95, 1, 98, 14, 20, 1),
(96, 1, 112, 15, 19, 1),
(97, 1, 114, 15, 26, 1),
(98, 1, 125, 16, 19, 1),
(99, 1, 127, 16, 26, 1),
(100, 1, 128, 16, 18, 1),
(101, 1, 14, 3, 23, 3),
(102, 1, 4, 1, 2, 12),
(103, 1, 4, 19, 2, 12),
(104, 1, 17, 3, 16, 12),
(105, 1, 17, 4, 16, 12),
(106, 1, 16, 3, 2, 13),
(107, 1, 16, 4, 2, 13),
(108, 1, 22, 5, 1, 13),
(109, 1, 22, 6, 1, 13),
(110, 1, 32, 7, 1, 13),
(111, 1, 32, 8, 1, 13),
(112, 1, 42, 9, 1, 13),
(113, 1, 42, 10, 1, 13),
(114, 1, 50, 11, 1, 13),
(115, 1, 63, 12, 1, 13),
(116, 1, 76, 13, 1, 13),
(117, 1, 90, 14, 1, 13),
(118, 1, 104, 15, 1, 13),
(119, 1, 118, 16, 1, 13),
(120, 1, 89, 14, 4, 13),
(121, 1, 12, 2, 25, 14),
(122, 1, 45, 9, 25, 14),
(123, 1, 45, 10, 25, 14),
(124, 1, 79, 13, 25, 14),
(125, 1, 121, 16, 25, 14),
(126, 1, 1, 1, 22, 15),
(127, 1, 5, 1, 16, 15),
(128, 1, 5, 19, 16, 15),
(129, 1, 7, 2, 22, 15),
(130, 1, 11, 2, 16, 15),
(131, 1, 13, 4, 22, 15),
(132, 1, 44, 9, 22, 15),
(133, 1, 44, 10, 22, 15),
(134, 1, 78, 13, 22, 15),
(135, 1, 120, 16, 22, 15),
(136, 1, 6, 1, 25, 16),
(137, 1, 6, 19, 25, 16),
(138, 1, 18, 3, 25, 16),
(139, 1, 18, 4, 25, 16),
(140, 1, 27, 5, 25, 16),
(141, 1, 27, 6, 25, 16),
(142, 1, 55, 11, 25, 16),
(143, 1, 95, 14, 25, 16),
(144, 1, 37, 7, 25, 16),
(145, 1, 37, 8, 25, 16),
(146, 1, 68, 12, 25, 16),
(147, 1, 109, 15, 25, 16),
(148, 1, 57, 11, 10, 17),
(149, 1, 58, 11, 14, 17),
(150, 1, 70, 12, 10, 17),
(151, 1, 71, 12, 14, 17),
(152, 1, 81, 13, 10, 17),
(153, 1, 82, 13, 9, 17),
(154, 1, 83, 13, 15, 17),
(155, 1, 3, 1, 6, 18),
(156, 1, 10, 2, 2, 18),
(157, 1, 28, 5, 24, 18),
(158, 1, 28, 6, 24, 18),
(159, 1, 56, 11, 24, 18),
(160, 1, 96, 14, 24, 18),
(161, 1, 38, 7, 24, 18),
(162, 1, 38, 8, 24, 18),
(163, 1, 69, 12, 24, 18),
(164, 1, 110, 15, 24, 18),
(165, 1, 46, 9, 24, 18),
(166, 1, 46, 10, 24, 18),
(167, 1, 80, 13, 24, 18),
(168, 1, 84, 13, 11, 18),
(169, 1, 86, 13, 13, 18),
(170, 1, 122, 16, 24, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_escala_calificaciones`
--

CREATE TABLE `sw_escala_calificaciones` (
  `id_escala_calificaciones` int(11) UNSIGNED NOT NULL,
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `ec_cualitativa` varchar(64) NOT NULL,
  `ec_cuantitativa` varchar(16) NOT NULL,
  `ec_nota_minima` float NOT NULL,
  `ec_nota_maxima` float NOT NULL,
  `ec_orden` int(4) NOT NULL,
  `ec_equivalencia` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_escala_calificaciones`
--

INSERT INTO `sw_escala_calificaciones` (`id_escala_calificaciones`, `id_periodo_lectivo`, `ec_cualitativa`, `ec_cuantitativa`, `ec_nota_minima`, `ec_nota_maxima`, `ec_orden`, `ec_equivalencia`) VALUES
(1, 1, 'Domina los aprendizajes requeridos', '9.00 - 10.00', 9, 10, 1, 'DA'),
(2, 1, 'Alcanza los aprendizajes requeridos', '7.00 - 8.99', 7, 8.99, 2, 'AA'),
(3, 1, 'Está próximo a alcanzar los aprendizajes requeridos', '4.01 - 6.99', 4.01, 6.99, 3, 'EA'),
(4, 1, 'No alcanza los aprendizajes requeridos', '<= 4', 0, 4, 4, 'NA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_especialidad`
--

CREATE TABLE `sw_especialidad` (
  `id_especialidad` int(11) UNSIGNED NOT NULL,
  `id_tipo_educacion` int(11) UNSIGNED NOT NULL,
  `es_nombre` varchar(64) NOT NULL,
  `es_figura` varchar(50) NOT NULL,
  `es_abreviatura` varchar(15) NOT NULL,
  `es_orden` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_especialidad`
--

INSERT INTO `sw_especialidad` (`id_especialidad`, `id_tipo_educacion`, `es_nombre`, `es_figura`, `es_abreviatura`, `es_orden`) VALUES
(2, 2, 'EGB ELEMENTAL', 'EGB ELEMENTAL', 'EGB. 2-3-4', 2),
(3, 3, 'EGB MEDIA', 'EGB MEDIA', 'EGB. 5-6-7', 3),
(4, 4, 'EGB SUPERIOR', 'EDUCACION GENERAL BASICA SUPERIOR', 'EGBS', 4),
(5, 5, 'BACHILLERATO GENERAL UNIFICADO', 'BACHILLERATO GENERAL UNIFICADO', 'BGU', 5),
(6, 6, 'Área Técnica de Servicios', 'CONTABILIDAD', 'CONTA', 6),
(7, 6, 'Área Técnica de Servicios', 'INFORMATICA', 'INFO', 7),
(8, 1, 'Primer año', 'Primer año', 'Pre.', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_horario`
--

CREATE TABLE `sw_horario` (
  `id_horario` int(11) UNSIGNED NOT NULL,
  `id_asignatura` int(11) UNSIGNED NOT NULL,
  `id_paralelo` int(11) UNSIGNED NOT NULL,
  `id_dia_semana` int(11) UNSIGNED NOT NULL,
  `id_hora_clase` int(11) UNSIGNED NOT NULL,
  `id_usuario` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_horario`
--

INSERT INTO `sw_horario` (`id_horario`, `id_asignatura`, `id_paralelo`, `id_dia_semana`, `id_hora_clase`, `id_usuario`) VALUES
(1, 2, 1, 2, 3, 12),
(4, 16, 1, 3, 2, 15),
(5, 22, 1, 4, 2, 15),
(6, 23, 1, 4, 3, 2),
(7, 6, 1, 5, 2, 18),
(8, 25, 1, 5, 3, 16),
(11, 2, 19, 2, 3, 12),
(12, 16, 19, 3, 2, 15),
(13, 22, 19, 3, 3, 9),
(14, 6, 19, 4, 2, 10),
(15, 23, 19, 4, 3, 11),
(16, 25, 19, 5, 3, 16),
(17, 6, 2, 3, 2, 8),
(18, 22, 2, 3, 3, 15),
(19, 25, 2, 3, 4, 14),
(21, 2, 2, 4, 3, 18),
(22, 23, 2, 5, 1, 2),
(23, 16, 2, 5, 2, 15),
(24, 16, 3, 2, 4, 12),
(25, 22, 3, 3, 2, 9),
(26, 6, 3, 3, 3, 8),
(27, 2, 3, 4, 2, 13),
(28, 23, 3, 4, 3, 3),
(29, 25, 3, 5, 2, 16),
(30, 16, 4, 2, 4, 12),
(31, 6, 4, 3, 3, 8),
(32, 2, 4, 4, 2, 13),
(33, 22, 4, 4, 3, 15),
(34, 25, 4, 5, 2, 16),
(35, 23, 4, 5, 4, 1),
(36, 3, 5, 2, 2, 5),
(37, 22, 5, 2, 3, 9),
(38, 1, 5, 3, 2, 13),
(39, 23, 5, 3, 3, 6),
(40, 8, 5, 3, 4, 8),
(41, 4, 5, 4, 2, 7),
(42, 25, 5, 4, 3, 16),
(43, 24, 5, 4, 4, 18),
(44, 7, 5, 5, 2, 8),
(45, 5, 5, 5, 3, 10),
(46, 3, 6, 2, 2, 5),
(47, 22, 6, 2, 3, 9),
(48, 1, 6, 3, 2, 13),
(49, 1, 6, 3, 3, 13),
(50, 8, 6, 3, 4, 8),
(51, 4, 6, 4, 2, 7),
(52, 25, 6, 4, 3, 16),
(53, 24, 6, 4, 4, 18),
(54, 7, 6, 5, 2, 8),
(55, 5, 6, 5, 3, 10),
(56, 24, 7, 2, 2, 18),
(57, 23, 7, 2, 3, 11),
(58, 25, 7, 3, 2, 16),
(59, 8, 7, 3, 3, 10),
(60, 1, 7, 3, 4, 13),
(61, 7, 7, 4, 2, 8),
(62, 4, 7, 4, 3, 7),
(63, 5, 7, 4, 4, 10),
(64, 3, 7, 5, 2, 5),
(65, 22, 7, 5, 3, 9),
(66, 24, 8, 2, 2, 18),
(67, 23, 8, 2, 3, 11),
(68, 25, 8, 3, 2, 16),
(69, 8, 8, 3, 3, 10),
(70, 1, 8, 3, 4, 13),
(71, 7, 8, 4, 2, 8),
(72, 4, 8, 4, 3, 7),
(73, 5, 8, 4, 4, 10),
(74, 3, 8, 5, 2, 5),
(75, 22, 8, 5, 3, 9),
(76, 25, 9, 2, 1, 14),
(77, 4, 9, 3, 2, 7),
(78, 3, 9, 3, 3, 5),
(79, 23, 9, 4, 2, 11),
(80, 1, 9, 4, 3, 13),
(81, 22, 9, 4, 4, 15),
(82, 8, 9, 5, 2, 10),
(83, 24, 9, 5, 3, 18),
(84, 25, 10, 2, 1, 14),
(85, 4, 10, 3, 2, 7),
(86, 3, 10, 3, 3, 5),
(87, 23, 10, 4, 2, 11),
(88, 1, 10, 4, 3, 13),
(89, 22, 10, 4, 4, 15),
(90, 8, 10, 5, 2, 10),
(91, 24, 10, 5, 3, 18),
(92, 22, 11, 2, 2, 9),
(93, 10, 11, 2, 3, 17),
(94, 1, 11, 2, 4, 13),
(95, 12, 11, 3, 2, 1),
(96, 4, 11, 3, 3, 7),
(97, 5, 11, 3, 4, 10),
(98, 23, 11, 4, 1, 6),
(99, 25, 11, 4, 2, 16),
(100, 8, 11, 4, 3, 8),
(101, 7, 11, 4, 4, 8),
(102, 10, 11, 5, 1, 17),
(103, 14, 11, 5, 2, 17),
(104, 3, 11, 5, 3, 5),
(105, 24, 11, 5, 4, 18),
(106, 24, 12, 2, 1, 18),
(107, 23, 12, 2, 2, 11),
(108, 3, 12, 2, 3, 5),
(109, 10, 12, 2, 4, 17),
(110, 8, 12, 3, 2, 10),
(111, 25, 12, 3, 3, 16),
(112, 12, 12, 3, 4, 1),
(113, 10, 12, 4, 1, 17),
(114, 14, 12, 4, 2, 17),
(115, 5, 12, 4, 3, 10),
(116, 1, 12, 4, 4, 13),
(117, 22, 12, 5, 2, 9),
(118, 4, 12, 5, 3, 7),
(119, 7, 12, 5, 4, 8),
(120, 10, 13, 2, 1, 17),
(121, 1, 13, 2, 2, 13),
(122, 11, 13, 2, 3, 18),
(123, 24, 13, 2, 4, 18),
(124, 3, 13, 3, 2, 5),
(125, 23, 13, 3, 3, 11),
(126, 22, 13, 3, 4, 15),
(127, 25, 13, 4, 1, 14),
(128, 13, 13, 4, 2, 18),
(129, 9, 13, 4, 3, 17),
(130, 15, 13, 4, 4, 17),
(131, 12, 13, 5, 1, 6),
(132, 4, 13, 5, 2, 7),
(133, 10, 13, 5, 3, 17),
(134, 8, 13, 5, 4, 10),
(135, 22, 14, 2, 2, 9),
(136, 4, 14, 2, 3, 13),
(137, 1, 14, 2, 4, 13),
(138, 19, 14, 3, 1, 6),
(139, 21, 14, 3, 2, 6),
(140, 17, 14, 3, 3, 1),
(141, 5, 14, 3, 4, 10),
(142, 23, 14, 4, 1, 6),
(143, 25, 14, 4, 2, 16),
(144, 8, 14, 4, 3, 8),
(145, 7, 14, 4, 4, 8),
(146, 20, 14, 5, 2, 1),
(147, 3, 14, 5, 3, 5),
(148, 24, 14, 5, 4, 18),
(149, 24, 15, 2, 1, 18),
(150, 23, 15, 2, 2, 11),
(151, 3, 15, 2, 3, 5),
(152, 8, 15, 3, 2, 10),
(153, 25, 15, 3, 3, 16),
(154, 20, 15, 3, 4, 6),
(155, 19, 15, 4, 1, 1),
(156, 21, 15, 4, 2, 6),
(157, 5, 15, 4, 3, 10),
(158, 1, 15, 4, 4, 13),
(159, 26, 15, 5, 1, 1),
(160, 22, 15, 5, 2, 9),
(161, 4, 15, 5, 3, 7),
(162, 7, 15, 5, 4, 8),
(163, 1, 16, 2, 2, 13),
(164, 24, 16, 2, 4, 18),
(165, 19, 16, 3, 1, 1),
(166, 3, 16, 3, 2, 5),
(167, 23, 16, 3, 3, 11),
(168, 22, 16, 3, 4, 15),
(169, 25, 16, 4, 1, 14),
(170, 26, 16, 4, 2, 1),
(171, 20, 16, 4, 3, 6),
(172, 21, 16, 4, 4, 6),
(173, 17, 16, 5, 1, 6),
(174, 4, 16, 5, 2, 7),
(175, 18, 16, 5, 3, 1),
(176, 8, 16, 5, 4, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_hora_clase`
--

CREATE TABLE `sw_hora_clase` (
  `id_hora_clase` int(11) UNSIGNED NOT NULL,
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `hc_nombre` varchar(12) NOT NULL,
  `hc_hora_inicio` time NOT NULL,
  `hc_hora_fin` time NOT NULL,
  `hc_ordinal` int(11) UNSIGNED NOT NULL,
  `hc_tipo` char(1) NOT NULL DEFAULT 'C'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_hora_clase`
--

INSERT INTO `sw_hora_clase` (`id_hora_clase`, `id_periodo_lectivo`, `hc_nombre`, `hc_hora_inicio`, `hc_hora_fin`, `hc_ordinal`, `hc_tipo`) VALUES
(1, 1, '1a. virtual', '18:50:00', '19:30:00', 1, 'C'),
(2, 1, '2a. virtual', '19:30:00', '20:10:00', 2, 'C'),
(3, 1, '3a. virtual', '20:10:00', '20:50:00', 3, 'C'),
(4, 1, '4a. virtual', '20:50:00', '21:30:00', 4, 'C'),
(5, 1, '5a. virtual', '21:30:00', '22:10:00', 5, 'C');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_hora_dia`
--

CREATE TABLE `sw_hora_dia` (
  `id_hora_dia` int(11) UNSIGNED NOT NULL,
  `id_dia_semana` int(11) UNSIGNED NOT NULL,
  `id_hora_clase` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_hora_dia`
--

INSERT INTO `sw_hora_dia` (`id_hora_dia`, `id_dia_semana`, `id_hora_clase`) VALUES
(1, 2, 1),
(2, 2, 2),
(3, 2, 3),
(4, 2, 4),
(5, 2, 5),
(6, 3, 1),
(7, 3, 2),
(8, 3, 3),
(9, 3, 4),
(10, 3, 5),
(11, 4, 1),
(12, 4, 2),
(13, 4, 3),
(14, 4, 4),
(15, 4, 5),
(16, 5, 1),
(17, 5, 2),
(18, 5, 3),
(19, 5, 4),
(20, 5, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_institucion`
--

CREATE TABLE `sw_institucion` (
  `id_institucion` int(11) UNSIGNED NOT NULL,
  `in_nombre` varchar(64) NOT NULL,
  `in_direccion` varchar(64) NOT NULL,
  `in_telefono` varchar(64) NOT NULL,
  `in_nom_rector` varchar(45) NOT NULL,
  `in_nom_vicerrector` varchar(45) NOT NULL,
  `in_nom_secretario` varchar(45) NOT NULL,
  `in_url` varchar(64) NOT NULL,
  `in_logo` varchar(64) NOT NULL,
  `in_amie` varchar(16) NOT NULL,
  `in_ciudad` varchar(64) NOT NULL,
  `in_copiar_y_pegar` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_institucion`
--

INSERT INTO `sw_institucion` (`id_institucion`, `in_nombre`, `in_direccion`, `in_telefono`, `in_nom_rector`, `in_nom_vicerrector`, `in_nom_secretario`, `in_url`, `in_logo`, `in_amie`, `in_ciudad`, `in_copiar_y_pegar`) VALUES
(1, 'UNIDAD EDUCATIVA PCEI FISCAL SALAMANCA', 'Calle el Tiempo y Pasaje Mónaco', '2256311/2254818', 'Msc. Wilson Proaño', 'Lic. Rómulo Mejía', 'Lic. Alicia Salazar O.', 'http://colegionocturnosalamanca.com', 'logo_salamanca.gif', '17H00215', 'Quito D.M.', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_jornada`
--

CREATE TABLE `sw_jornada` (
  `id_jornada` int(11) UNSIGNED NOT NULL,
  `jo_nombre` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_jornada`
--

INSERT INTO `sw_jornada` (`id_jornada`, `jo_nombre`) VALUES
(1, 'MATUTINA'),
(2, 'VESPERTINA'),
(3, 'NOCTURNA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_malla_curricular`
--

CREATE TABLE `sw_malla_curricular` (
  `id_malla_curricular` int(11) UNSIGNED NOT NULL,
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `id_curso` int(11) UNSIGNED NOT NULL,
  `id_asignatura` int(11) UNSIGNED NOT NULL,
  `ma_horas_presenciales` int(11) UNSIGNED NOT NULL,
  `ma_horas_autonomas` int(11) UNSIGNED NOT NULL,
  `ma_horas_tutorias` int(11) UNSIGNED NOT NULL,
  `ma_subtotal` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_malla_curricular`
--

INSERT INTO `sw_malla_curricular` (`id_malla_curricular`, `id_periodo_lectivo`, `id_curso`, `id_asignatura`, `ma_horas_presenciales`, `ma_horas_autonomas`, `ma_horas_tutorias`, `ma_subtotal`) VALUES
(1, 1, 1, 22, 3, 2, 1, 6),
(2, 1, 1, 23, 3, 2, 1, 6),
(3, 1, 1, 6, 3, 2, 2, 7),
(4, 1, 1, 2, 2, 2, 1, 5),
(5, 1, 1, 16, 1, 1, 0, 2),
(6, 1, 1, 25, 2, 1, 1, 4),
(7, 1, 2, 22, 3, 2, 1, 6),
(8, 1, 2, 23, 3, 2, 1, 6),
(9, 1, 2, 6, 3, 2, 2, 7),
(10, 1, 2, 2, 2, 2, 1, 5),
(11, 1, 2, 16, 1, 1, 0, 2),
(12, 1, 2, 25, 2, 1, 1, 4),
(13, 1, 3, 22, 3, 2, 1, 6),
(14, 1, 3, 23, 3, 2, 1, 6),
(15, 1, 3, 6, 3, 2, 2, 7),
(16, 1, 3, 2, 2, 2, 1, 5),
(17, 1, 3, 16, 1, 1, 0, 2),
(18, 1, 3, 25, 2, 1, 1, 4),
(19, 1, 4, 23, 2, 1, 1, 4),
(20, 1, 4, 3, 2, 1, 0, 3),
(21, 1, 4, 4, 2, 1, 1, 4),
(22, 1, 4, 1, 1, 1, 1, 3),
(23, 1, 4, 8, 1, 1, 1, 3),
(24, 1, 4, 5, 1, 1, 0, 2),
(25, 1, 4, 7, 1, 1, 0, 2),
(26, 1, 4, 22, 2, 1, 1, 4),
(27, 1, 4, 25, 2, 1, 0, 3),
(28, 1, 4, 24, 1, 1, 0, 2),
(29, 1, 5, 23, 2, 1, 1, 4),
(30, 1, 5, 3, 2, 1, 0, 3),
(31, 1, 5, 4, 2, 1, 1, 4),
(32, 1, 5, 1, 1, 1, 1, 3),
(33, 1, 5, 8, 1, 1, 1, 3),
(34, 1, 5, 5, 1, 1, 0, 2),
(35, 1, 5, 7, 1, 1, 0, 2),
(36, 1, 5, 22, 2, 1, 1, 4),
(37, 1, 5, 25, 2, 1, 0, 3),
(38, 1, 5, 24, 1, 1, 0, 2),
(39, 1, 6, 23, 2, 2, 1, 5),
(40, 1, 6, 3, 2, 2, 1, 5),
(41, 1, 6, 4, 2, 1, 0, 3),
(42, 1, 6, 1, 2, 2, 1, 5),
(43, 1, 6, 8, 2, 1, 1, 4),
(44, 1, 6, 22, 2, 2, 1, 5),
(45, 1, 6, 25, 2, 0, 0, 2),
(46, 1, 6, 24, 1, 0, 0, 1),
(47, 1, 7, 23, 2, 1, 1, 4),
(48, 1, 7, 3, 2, 1, 0, 3),
(49, 1, 7, 4, 2, 1, 1, 4),
(50, 1, 7, 1, 1, 1, 1, 3),
(51, 1, 7, 8, 1, 1, 1, 3),
(52, 1, 7, 5, 1, 1, 0, 2),
(53, 1, 7, 7, 1, 1, 0, 2),
(54, 1, 7, 22, 2, 1, 1, 4),
(55, 1, 7, 25, 2, 1, 0, 3),
(56, 1, 7, 24, 1, 1, 0, 2),
(57, 1, 7, 10, 6, 0, 0, 6),
(58, 1, 7, 14, 2, 0, 0, 2),
(59, 1, 7, 12, 2, 0, 0, 2),
(60, 1, 8, 23, 2, 1, 1, 5),
(61, 1, 8, 3, 2, 1, 0, 3),
(62, 1, 8, 4, 2, 1, 1, 4),
(63, 1, 8, 1, 1, 1, 1, 3),
(64, 1, 8, 8, 1, 1, 1, 3),
(65, 1, 8, 5, 1, 1, 0, 2),
(66, 1, 8, 7, 1, 1, 0, 2),
(67, 1, 8, 22, 2, 1, 1, 4),
(68, 1, 8, 25, 2, 1, 0, 3),
(69, 1, 8, 24, 1, 1, 0, 2),
(70, 1, 8, 10, 6, 0, 0, 6),
(71, 1, 8, 14, 2, 0, 0, 2),
(72, 1, 8, 12, 2, 0, 0, 2),
(73, 1, 9, 23, 2, 2, 1, 5),
(74, 1, 9, 3, 2, 2, 1, 5),
(75, 1, 9, 4, 2, 1, 0, 3),
(76, 1, 9, 1, 2, 2, 1, 5),
(77, 1, 9, 8, 2, 1, 1, 4),
(78, 1, 9, 22, 2, 2, 1, 5),
(79, 1, 9, 25, 2, 0, 0, 2),
(80, 1, 9, 24, 1, 0, 0, 1),
(81, 1, 9, 10, 3, 0, 0, 3),
(82, 1, 9, 9, 2, 0, 0, 2),
(83, 1, 9, 15, 2, 0, 0, 2),
(84, 1, 9, 11, 1, 0, 0, 1),
(85, 1, 9, 12, 1, 0, 0, 1),
(86, 1, 9, 13, 1, 0, 0, 1),
(87, 1, 10, 23, 2, 1, 1, 4),
(88, 1, 10, 3, 2, 1, 0, 3),
(89, 1, 10, 4, 2, 1, 1, 4),
(90, 1, 10, 1, 1, 1, 1, 3),
(91, 1, 10, 8, 1, 1, 1, 3),
(92, 1, 10, 5, 1, 0, 0, 1),
(93, 1, 10, 7, 1, 0, 0, 1),
(94, 1, 10, 22, 2, 1, 1, 4),
(95, 1, 10, 25, 2, 1, 0, 3),
(96, 1, 10, 24, 1, 1, 0, 2),
(97, 1, 10, 17, 2, 0, 0, 2),
(98, 1, 10, 20, 2, 0, 0, 2),
(99, 1, 10, 19, 4, 0, 0, 4),
(100, 1, 10, 21, 2, 0, 0, 2),
(101, 1, 11, 23, 2, 1, 1, 4),
(102, 1, 11, 3, 2, 1, 0, 3),
(103, 1, 11, 4, 2, 1, 1, 4),
(104, 1, 11, 1, 1, 1, 1, 3),
(105, 1, 11, 8, 1, 1, 1, 3),
(106, 1, 11, 5, 1, 1, 0, 2),
(107, 1, 11, 7, 1, 1, 0, 2),
(108, 1, 11, 22, 2, 1, 1, 4),
(109, 1, 11, 25, 2, 1, 0, 3),
(110, 1, 11, 24, 1, 1, 0, 2),
(111, 1, 11, 20, 2, 0, 0, 2),
(112, 1, 11, 19, 4, 0, 0, 4),
(113, 1, 11, 21, 2, 0, 0, 2),
(114, 1, 11, 26, 2, 0, 0, 2),
(115, 1, 12, 23, 2, 2, 1, 5),
(116, 1, 12, 3, 2, 2, 1, 5),
(117, 1, 12, 4, 2, 1, 0, 3),
(118, 1, 12, 1, 2, 2, 1, 5),
(119, 1, 12, 8, 2, 1, 1, 4),
(120, 1, 12, 22, 2, 2, 1, 5),
(121, 1, 12, 25, 2, 0, 0, 2),
(122, 1, 12, 24, 1, 0, 0, 1),
(123, 1, 12, 17, 2, 0, 0, 2),
(124, 1, 12, 20, 2, 0, 0, 2),
(125, 1, 12, 19, 2, 0, 0, 2),
(126, 1, 12, 21, 1, 0, 0, 1),
(127, 1, 12, 26, 2, 0, 0, 2),
(128, 1, 12, 18, 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_menu`
--

CREATE TABLE `sw_menu` (
  `id_menu` int(11) UNSIGNED NOT NULL,
  `mnu_texto` varchar(32) NOT NULL,
  `mnu_link` varchar(64) NOT NULL,
  `mnu_nivel` int(11) UNSIGNED NOT NULL,
  `mnu_orden` int(11) UNSIGNED NOT NULL,
  `mnu_padre` int(11) UNSIGNED NOT NULL,
  `mnu_publicado` int(11) UNSIGNED NOT NULL,
  `mnu_icono` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_menu`
--

INSERT INTO `sw_menu` (`id_menu`, `mnu_texto`, `mnu_link`, `mnu_nivel`, `mnu_orden`, `mnu_padre`, `mnu_publicado`, `mnu_icono`) VALUES
(1, 'Administración', '#', 1, 1, 0, 1, ''),
(2, 'Definiciones', '#', 1, 2, 0, 1, ''),
(3, 'Especificaciones', '#', 1, 3, 0, 1, ''),
(4, 'Asociar', '#', 1, 4, 0, 1, ''),
(5, 'Cierres', '#', 1, 5, 0, 1, ''),
(6, 'Modalidades', 'admin/modalidades', 2, 1, 1, 1, ''),
(7, 'Períodos Lectivos', 'admin/periodos_lectivos', 2, 2, 1, 1, ''),
(8, 'Perfiles', 'admin/perfiles', 2, 3, 1, 1, ''),
(9, 'Menús', 'admin/menus', 2, 4, 1, 1, ''),
(10, 'Usuarios', 'admin/usuarios', 2, 5, 1, 1, ''),
(11, 'Niveles de Educación', 'admin/tipos_educacion', 2, 1, 2, 1, ''),
(12, 'Especialidades', 'admin/especialidades', 2, 2, 2, 1, ''),
(13, 'Cursos', 'admin/cursos', 2, 3, 2, 1, ''),
(14, 'Paralelos', 'admin/paralelos', 2, 4, 2, 1, ''),
(15, 'Areas', 'admin/areas', 2, 5, 2, 1, ''),
(16, 'Asignaturas', 'admin/asignaturas', 2, 6, 2, 1, ''),
(17, 'Institución', 'admin/institucion', 2, 7, 2, 1, ''),
(18, 'Períodos de Evaluación', 'admin/periodos_evaluacion', 2, 1, 3, 1, ''),
(19, 'Aportes de Evaluación', 'admin/aportes_evaluacion', 2, 2, 3, 1, ''),
(20, 'Insumos de Evaluación', 'admin/rubricas_evaluacion', 2, 3, 3, 1, ''),
(21, 'Escalas de Calificaciones', 'admin/escalas_calificaciones', 2, 4, 3, 1, ''),
(22, 'Asignaturas Cursos', 'admin/asignaturas_cursos', 2, 1, 4, 1, ''),
(23, 'Curso Superior', 'admin/curso_superior', 2, 2, 4, 1, ''),
(24, 'Paralelos Tutores', 'admin/paralelos_tutores', 2, 3, 4, 1, ''),
(25, 'Paralelos Inspectores', 'admin/paralelos_inspectores', 2, 4, 4, 1, ''),
(26, 'Periodos', 'admin/cierre_periodos', 2, 1, 5, 1, ''),
(27, 'Definiciones', '#', 1, 1, 0, 1, ''),
(28, 'Horarios', '#', 1, 2, 0, 1, ''),
(29, 'Reportes', '#', 1, 3, 0, 1, ''),
(30, 'Consultas', '#', 1, 4, 0, 1, ''),
(31, 'Estadísticas', '#', 1, 5, 0, 1, ''),
(32, 'Malla Curricular', 'autoridad/mallas_curriculares', 2, 1, 27, 1, ''),
(33, 'Distributivo', 'autoridad/distributivos', 2, 2, 27, 1, ''),
(34, 'Definir Días de la Semana', 'autoridad/dias_semana', 2, 1, 28, 1, ''),
(35, 'Definir Horas Clase', 'autoridad/horas_clase', 2, 2, 28, 1, ''),
(36, 'Asociar Dia-Hora', 'autoridad/horas_dia', 2, 3, 28, 1, ''),
(37, 'Definir Horario Semanal', 'autoridad/horarios', 2, 4, 28, 1, ''),
(38, 'Definir Inasistencias', 'autoridad/inasistencias', 2, 5, 28, 1, ''),
(39, 'Parciales', 'autoridad/reporte_parciales', 2, 1, 29, 1, ''),
(40, 'Quimestrales', 'autoridad/reporte_quimestral', 2, 2, 29, 1, ''),
(41, 'Anuales', 'autoridad/reporte_anual', 2, 3, 29, 1, ''),
(42, 'Lista de docentes', 'autoridad/lista_docentes', 2, 1, 30, 1, ''),
(43, 'Horarios de clase', 'autoridad/horario_clases', 2, 2, 30, 1, ''),
(44, 'Aprobados por Paralelo', 'autoridad/aprobados_paralelo', 2, 1, 31, 1, ''),
(45, 'Matriculación', '#', 1, 1, 0, 1, ''),
(46, 'Libretación', 'secretaria/libretacion', 1, 2, 0, 1, ''),
(47, 'Reporte', '#', 1, 3, 0, 1, ''),
(48, 'A Excel', '#', 1, 4, 0, 1, ''),
(49, 'Promoción', 'secretaria/promocion', 1, 5, 0, 1, ''),
(50, 'Paralelos', 'secretaria/matriculacion', 2, 1, 45, 1, ''),
(51, 'Nómina de Matriculados', 'secretaria/nomina_matriculados', 2, 1, 47, 1, ''),
(52, 'Por Asignatura', 'secretaria/por_asignatura', 2, 2, 47, 1, ''),
(53, 'Parciales', 'secretaria/reporte_parciales', 2, 3, 47, 1, ''),
(54, 'Quimestral', 'secretaria/reporte_quimestral', 2, 4, 47, 1, ''),
(55, 'Anual', 'secretaria/reporte_anual', 2, 5, 47, 1, ''),
(56, 'De Supletorios', 'secretaria/reporte_supletorios', 2, 6, 47, 1, ''),
(57, 'De Remediales', 'secretaria/reporte_remediales', 2, 7, 47, 1, ''),
(58, 'De Exámenes de Gracia', 'secretaria/reporte_examenes_gracia', 2, 8, 47, 1, ''),
(59, 'Padrón Electoral', 'secretaria/padron_electoral', 2, 1, 48, 1, ''),
(60, 'Cuadro Final', 'secretaria/cuadro_final', 2, 2, 48, 1, ''),
(61, 'Cuadro Remediales', 'secretaria/cuadro_remediales', 2, 3, 48, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_menu_perfil`
--

CREATE TABLE `sw_menu_perfil` (
  `id_perfil` int(11) UNSIGNED NOT NULL,
  `id_menu` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_menu_perfil`
--

INSERT INTO `sw_menu_perfil` (`id_perfil`, `id_menu`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(2, 27),
(2, 28),
(2, 29),
(2, 30),
(2, 31),
(2, 32),
(2, 33),
(2, 34),
(2, 35),
(2, 36),
(2, 37),
(2, 38),
(2, 39),
(2, 40),
(2, 41),
(2, 42),
(2, 43),
(2, 44),
(7, 45),
(7, 46),
(7, 47),
(7, 48),
(7, 49),
(7, 50),
(7, 51),
(7, 52),
(7, 53),
(7, 54),
(7, 55),
(7, 56),
(7, 57),
(7, 58),
(7, 59),
(7, 60),
(7, 61);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_modalidad`
--

CREATE TABLE `sw_modalidad` (
  `id_modalidad` int(11) UNSIGNED NOT NULL,
  `mo_nombre` varchar(64) NOT NULL,
  `mo_activo` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_modalidad`
--

INSERT INTO `sw_modalidad` (`id_modalidad`, `mo_nombre`, `mo_activo`) VALUES
(1, 'SEMIPRESENCIAL', 1),
(2, 'BGU INTENSIVO', 0),
(3, 'EGB SUPERIOR INTENSIVA', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_paralelo`
--

CREATE TABLE `sw_paralelo` (
  `id_paralelo` int(11) UNSIGNED NOT NULL,
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `id_curso` int(11) UNSIGNED NOT NULL,
  `id_jornada` int(11) UNSIGNED NOT NULL,
  `pa_nombre` varchar(5) NOT NULL,
  `pa_orden` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_paralelo`
--

INSERT INTO `sw_paralelo` (`id_paralelo`, `id_periodo_lectivo`, `id_curso`, `id_jornada`, `pa_nombre`, `pa_orden`) VALUES
(1, 1, 1, 3, 'A', 1),
(2, 1, 2, 3, 'A', 3),
(3, 1, 3, 3, 'A', 4),
(4, 1, 3, 3, 'B', 5),
(5, 1, 4, 3, 'A', 6),
(6, 1, 4, 3, 'B', 7),
(7, 1, 5, 3, 'A', 8),
(8, 1, 5, 3, 'B', 9),
(9, 1, 6, 3, 'A', 10),
(10, 1, 6, 3, 'B', 11),
(11, 1, 7, 3, 'A', 12),
(12, 1, 8, 3, 'A', 13),
(13, 1, 9, 3, 'A', 14),
(14, 1, 10, 3, 'A', 15),
(15, 1, 11, 3, 'A', 16),
(16, 1, 12, 3, 'A', 17),
(19, 1, 1, 3, 'B', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_paralelo_inspector`
--

CREATE TABLE `sw_paralelo_inspector` (
  `id_paralelo_inspector` int(11) UNSIGNED NOT NULL,
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `id_paralelo` int(11) UNSIGNED NOT NULL,
  `id_usuario` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_paralelo_inspector`
--

INSERT INTO `sw_paralelo_inspector` (`id_paralelo_inspector`, `id_periodo_lectivo`, `id_paralelo`, `id_usuario`) VALUES
(2, 1, 1, 12),
(3, 1, 2, 12),
(4, 1, 3, 12),
(5, 1, 4, 12),
(6, 1, 5, 12),
(7, 1, 6, 12),
(8, 1, 7, 12),
(9, 1, 8, 12),
(10, 1, 9, 12),
(11, 1, 10, 12),
(12, 1, 11, 12),
(13, 1, 12, 12),
(14, 1, 13, 12),
(15, 1, 14, 12),
(16, 1, 15, 12),
(17, 1, 16, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_paralelo_tutor`
--

CREATE TABLE `sw_paralelo_tutor` (
  `id_paralelo_tutor` int(11) UNSIGNED NOT NULL,
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `id_paralelo` int(11) UNSIGNED NOT NULL,
  `id_usuario` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_paralelo_tutor`
--

INSERT INTO `sw_paralelo_tutor` (`id_paralelo_tutor`, `id_periodo_lectivo`, `id_paralelo`, `id_usuario`) VALUES
(2, 1, 2, 15),
(3, 1, 3, 9),
(4, 1, 4, 12),
(5, 1, 5, 8),
(6, 1, 6, 2),
(7, 1, 7, 11),
(8, 1, 8, 10),
(9, 1, 9, 7),
(10, 1, 10, 5),
(11, 1, 11, 17),
(12, 1, 12, 16),
(13, 1, 13, 18),
(14, 1, 14, 13),
(15, 1, 15, 6),
(16, 1, 16, 1),
(17, 1, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_perfil`
--

CREATE TABLE `sw_perfil` (
  `id_perfil` int(11) UNSIGNED NOT NULL,
  `pe_nombre` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_perfil`
--

INSERT INTO `sw_perfil` (`id_perfil`, `pe_nombre`) VALUES
(1, 'Administrador'),
(2, 'Autoridad'),
(3, 'DECE'),
(4, 'Docente'),
(5, 'Inspección'),
(6, 'Representante'),
(7, 'Secretaría'),
(8, 'Tutor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_periodo_estado`
--

CREATE TABLE `sw_periodo_estado` (
  `id_periodo_estado` int(11) UNSIGNED NOT NULL,
  `pe_descripcion` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_periodo_estado`
--

INSERT INTO `sw_periodo_estado` (`id_periodo_estado`, `pe_descripcion`) VALUES
(1, 'ACTUAL'),
(2, 'TERMINADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_periodo_evaluacion`
--

CREATE TABLE `sw_periodo_evaluacion` (
  `id_periodo_evaluacion` int(11) UNSIGNED NOT NULL,
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `id_tipo_periodo` int(11) UNSIGNED NOT NULL,
  `pe_nombre` varchar(24) NOT NULL,
  `pe_abreviatura` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_periodo_evaluacion`
--

INSERT INTO `sw_periodo_evaluacion` (`id_periodo_evaluacion`, `id_periodo_lectivo`, `id_tipo_periodo`, `pe_nombre`, `pe_abreviatura`) VALUES
(1, 1, 1, 'PRIMER QUIMESTRE', '1ER.Q.'),
(2, 1, 1, 'SEGUNDO QUIMESTRE', '2DO.Q.'),
(3, 1, 2, 'EXAMEN SUPLETORIO', 'SUP.'),
(4, 1, 3, 'EXAMEN REMEDIAL', 'REM.'),
(5, 1, 4, 'EXAMEN DE GRACIA', 'GRA.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_periodo_lectivo`
--

CREATE TABLE `sw_periodo_lectivo` (
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `id_periodo_estado` int(11) UNSIGNED NOT NULL,
  `id_modalidad` int(11) UNSIGNED NOT NULL,
  `pe_anio_inicio` int(5) UNSIGNED NOT NULL,
  `pe_anio_fin` int(5) UNSIGNED NOT NULL,
  `pe_fecha_inicio` date NOT NULL,
  `pe_fecha_fin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_periodo_lectivo`
--

INSERT INTO `sw_periodo_lectivo` (`id_periodo_lectivo`, `id_periodo_estado`, `id_modalidad`, `pe_anio_inicio`, `pe_anio_fin`, `pe_fecha_inicio`, `pe_fecha_fin`) VALUES
(1, 1, 1, 2021, 2022, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_rubrica_evaluacion`
--

CREATE TABLE `sw_rubrica_evaluacion` (
  `id_rubrica_evaluacion` int(11) UNSIGNED NOT NULL,
  `id_aporte_evaluacion` int(11) UNSIGNED NOT NULL,
  `id_tipo_asignatura` int(1) UNSIGNED NOT NULL DEFAULT 1,
  `ru_nombre` varchar(24) NOT NULL,
  `ru_abreviatura` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_rubrica_evaluacion`
--

INSERT INTO `sw_rubrica_evaluacion` (`id_rubrica_evaluacion`, `id_aporte_evaluacion`, `id_tipo_asignatura`, `ru_nombre`, `ru_abreviatura`) VALUES
(1, 1, 1, 'TRABAJOS', 'TR'),
(2, 1, 1, 'SUMATIVA', 'SU'),
(3, 2, 1, 'TRABAJOS', 'TR'),
(4, 2, 1, 'SUMATIVA', 'SU'),
(5, 3, 1, 'PROYECTO QUIMESTRAL', 'PROY'),
(6, 4, 1, 'TRABAJOS', 'TR'),
(7, 4, 1, 'SUMATIVA', 'SU'),
(8, 5, 1, 'TRABAJOS', 'TR'),
(9, 5, 1, 'SUMATIVA', 'SU'),
(10, 6, 1, 'PROYECTO QUIMESTRAL', 'PROY'),
(11, 7, 1, 'EXAMEN SUPLETORIO', 'SUP'),
(12, 8, 1, 'EXAMEN REMEDIAL', 'REM'),
(13, 9, 1, 'EXAMEN DE GRACIA', 'GRA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tipo_aporte`
--

CREATE TABLE `sw_tipo_aporte` (
  `id_tipo_aporte` int(11) UNSIGNED NOT NULL,
  `ta_descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_tipo_aporte`
--

INSERT INTO `sw_tipo_aporte` (`id_tipo_aporte`, `ta_descripcion`) VALUES
(1, 'PARCIAL'),
(2, 'EXAMEN QUIMESTRAL'),
(3, 'SUPLETORIO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tipo_asignatura`
--

CREATE TABLE `sw_tipo_asignatura` (
  `id_tipo_asignatura` int(11) UNSIGNED NOT NULL,
  `ta_descripcion` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_tipo_asignatura`
--

INSERT INTO `sw_tipo_asignatura` (`id_tipo_asignatura`, `ta_descripcion`) VALUES
(1, 'CUANTITATIVA'),
(2, 'CUALITATIVA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tipo_educacion`
--

CREATE TABLE `sw_tipo_educacion` (
  `id_tipo_educacion` int(11) UNSIGNED NOT NULL,
  `id_periodo_lectivo` int(11) UNSIGNED NOT NULL,
  `te_nombre` varchar(48) NOT NULL,
  `te_bachillerato` int(1) UNSIGNED NOT NULL,
  `te_orden` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_tipo_educacion`
--

INSERT INTO `sw_tipo_educacion` (`id_tipo_educacion`, `id_periodo_lectivo`, `te_nombre`, `te_bachillerato`, `te_orden`) VALUES
(1, 1, 'Educación General Básica Preparatoria', 0, 1),
(2, 1, 'Educación General Básica Elemental', 0, 2),
(3, 1, 'Educación General Básica Media', 0, 3),
(4, 1, 'Educación General Básica Superior', 0, 4),
(5, 1, 'Bachillerato General Unificado', 1, 5),
(6, 1, 'Bachillerato Técnico', 1, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_tipo_periodo`
--

CREATE TABLE `sw_tipo_periodo` (
  `id_tipo_periodo` int(11) UNSIGNED NOT NULL,
  `tp_descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_tipo_periodo`
--

INSERT INTO `sw_tipo_periodo` (`id_tipo_periodo`, `tp_descripcion`) VALUES
(1, 'QUIMESTRE'),
(2, 'SUPLETORIO'),
(3, 'REMEDIAL'),
(4, 'DE GRACIA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_usuario`
--

CREATE TABLE `sw_usuario` (
  `id_usuario` int(11) UNSIGNED NOT NULL,
  `us_titulo` varchar(8) NOT NULL,
  `us_apellidos` varchar(32) NOT NULL,
  `us_nombres` varchar(32) NOT NULL,
  `us_shortname` varchar(45) NOT NULL,
  `us_fullname` varchar(64) NOT NULL,
  `us_login` varchar(24) NOT NULL,
  `us_password` varchar(535) NOT NULL,
  `us_foto` varchar(100) NOT NULL,
  `us_genero` varchar(1) NOT NULL,
  `us_activo` int(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_usuario`
--

INSERT INTO `sw_usuario` (`id_usuario`, `us_titulo`, `us_apellidos`, `us_nombres`, `us_shortname`, `us_fullname`, `us_login`, `us_password`, `us_foto`, `us_genero`, `us_activo`) VALUES
(1, 'Ing.', 'Peñaherrera Escobar', 'Gonzalo Nicolás', 'Ing. Gonzalo Peñaherrera', 'Peñaherrera Escobar Gonzalo Nicolás', 'Administrador', '$2y$10$7xpqgFKUUeKDD0RPSXXjVOZUtIzpmPSldn0qzXw2px1AJOvVyha02', '1629333584_da236f72cd46439cacd3.png', 'M', 1),
(2, 'Lic.', 'Mejía Segarra', 'Rómulo Oswaldo', 'Lic. Rómulo Mejía', 'Mejía Segarra Rómulo Oswaldo', 'ROMULOM', '$2y$10$UUwrJKP/kahfFIP3B5r.EOYi9b.r4umuSRKq9QFGN54x5WaVBm/1y', '1627762117_c4212d45204fe2777d15.jpg', 'M', 1),
(3, 'Msc.', 'Proaño Estrella', 'Wilson Eduardo', 'Msc. Wilson Proaño', 'Proaño Estrella Wilson Eduardo', 'wilsonp', '$2y$10$U.JC3rEw34vrRkpTqDqbhuQF.jXxBCp.ACp7pHFM6VZl0yOWnGhDa', '1627762292_0893721ed3d22401d1c1.jpg', 'M', 1),
(4, 'Ps.Cl.', 'Suasnavas Silva', 'Iván Alfonso', 'Ps.Cl. Iván Suasnavas', 'Suasnavas Silva Iván Alfonso', 'ivans', '$2y$10$w58w38tz2ZPvQXEqnJFO2euKRDuqGmTzF8/QVof52imTDyruKkDJ2', '1627762499_bdca8569af5d8d7e0960.png', 'M', 1),
(5, 'Ing.', 'Benavides Ortiz', 'German Gustavo', 'Ing. German Benavides', 'Benavides Ortiz German Gustavo', 'germanb', '$2y$10$0Zdno62snMLhf1GGHlaXPuYtJZxQvcEts3q0Y.mYmK/CDYXNlYrDO', '1627762799_d2656db4df50a1bba42b.jpg', 'M', 1),
(6, 'Tlgo.', 'Cabascango Herrera', 'Milton Fabián', 'Tlgo. Milton Cabascango', 'Cabascango Herrera Milton Fabián', 'MILTONC', '$2y$10$QSh1sO3RA8LWb.Vx7NYgKu.MkJ8wOO8ndCXovyjwbGebSwbCw5SRO', '1627763007_5193576ce79b33492d76.jpg', 'M', 1),
(7, 'Lic.', 'Calero Navarrete', 'Elmo Eduardo', 'Lic. Elmo Calero', 'Calero Navarrete Elmo Eduardo', 'elmoc', '$2y$10$iHfZY/9h15.qnL./WVU5Oe8u87OHAXmhkNH9.Pc7HwfLGPC2Rf7zG', '1627763224_e6a8cc4ab536342b10a0.jpg', 'M', 1),
(8, 'Lic.', 'Cedeño Zambrano', 'Edith Monserrate', 'Lic. Edith Cedeño', 'Cedeño Zambrano Edith Monserrate', 'EDITHC', '$2y$10$htqGjTV/1.r6GhtKVKO0ze8ROMlAhbZ4yBbin3/6FW7lpq2Xhqy8m', '1627767034_0e49ade84190b8fbdb8c.jpg', 'F', 1),
(9, 'Dr.', 'Enríquez Martínez', 'Carlos Alberto', 'Dr. Carlos Enríquez', 'Enríquez Martínez Carlos Alberto', 'CARLOSE', '$2y$10$dGWLG4OI1Yidp4Dj9DnLlOqmm02/agy13jrGUlOtWsL469Fgfbnr2', '1627799349_510dce2818e205493fb3.png', 'M', 1),
(10, 'Dr.', 'Guamán Calderón', 'Luis Alfredo', 'Dr. Luis Guamán', 'Guamán Calderón Luis Alfredo', 'LUISG', '$2y$10$rfhT1/rQ9vuo2Rl8ae08NukgyjBvW0cPkYoD.Jd5gtPxEL90dce0S', '1627799492_1d2b4b59353e6c769aba.png', 'M', 1),
(11, 'Lic.', 'Montenegro Yépez', 'Jaime Efrén', 'Lic. Jaime Montenegro', 'Montenegro Yépez Jaime Efrén', 'EFRENM', '$2y$10$7QVBFwSgJBrlIX0FE1VFcOensd0U2LCXXPuYUp1GyC.DsJHVkKTJ2', '1627799642_f48e342718bdd5f12e60.jpg', 'M', 1),
(12, 'Lic.', 'Quevedo Barrezueta', 'Alfonso Miguel', 'Lic. Alfonso Quevedo', 'Quevedo Barrezueta Alfonso Miguel', 'ALFONSOQ', '$2y$10$LSKQtbt.ALtTXrFBt8dZ0.oKKMLy4U22oLWwdYN4Y8VtbQJRE98EK', '1627799996_149535e40e447f514606.jpg', 'M', 1),
(13, 'Msc.', 'Rosero Medina', 'Roberto Hernán', 'Msc. Roberto Rosero', 'Rosero Medina Roberto Hernán', 'robertos', '$2y$10$REwdcrXjTpCEmDmJslkfve3l224xwRUas7RMRxqIoitbQ4dp/2.r2', '1627800084_2173771fd758a82a372d.jpg', 'M', 1),
(14, 'Lic.', 'Salazar Ordoñez', 'Carmen Alicia', 'Lic. Carmen Salazar', 'Salazar Ordoñez Carmen Alicia', 'alicias', '$2y$10$NSbTOB5csy41DYGLFErS7O2dIisIbRXXSC/KJPMtszUA74rmvlyx.', '1627800351_4afc1c9fb98545873c46.jpg', 'F', 1),
(15, 'Lic.', 'Salgado Araujo', 'María del Rosario', 'Lic. María Salgado', 'Salgado Araujo María del Rosario', 'rosarios', '$2y$10$dNcXhJvDhuDw8bEuYjQfpuw240.ESldER57JyUB6sDz0VvQ2nn01u', '1627800554_4b682488595f70a54762.png', 'F', 1),
(16, 'Msc.', 'Sanmartín Vásquez', 'Sandra Verónica', 'Msc. Sandra Sanmartín', 'Sanmartín Vásquez Sandra Verónica', 'veronicas', '$2y$10$tByNDDtBEeF.fiMTOmEbpuZihYoquA6mW2INt4urjl2Rk1dJcZNh.', '1627800689_7f43cb2200f6cf85af7c.png', 'F', 1),
(17, 'Lic.', 'Trujillo Realpe', 'William Oswaldo', 'Lic. William Trujillo', 'Trujillo Realpe William Oswaldo', 'williamt', '$2y$10$bkBspRx6H5KrEjWXN/RjdurakZJrEo.5yu7StRbL2dmIDsvva4lka', '1627800803_f4c68fdadd80c3da62ee.jpg', 'M', 1),
(18, 'Lic.', 'Zambrano Cedeño', 'Walter Adbón', 'Lic. Walter Zambrano', 'Zambrano Cedeño Walter Adbón', 'walterz', '$2y$10$LhiyFJ.snWeeXSv8fUBj/uQ6AXZqu4KNwoB6KUR.B55rHgchK6McW', '1627800923_6470304960e43357fcb5.jpg', 'M', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sw_usuario_perfil`
--

CREATE TABLE `sw_usuario_perfil` (
  `id_usuario` int(11) UNSIGNED NOT NULL,
  `id_perfil` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sw_usuario_perfil`
--

INSERT INTO `sw_usuario_perfil` (`id_usuario`, `id_perfil`) VALUES
(2, 2),
(2, 4),
(2, 8),
(3, 2),
(3, 4),
(4, 3),
(5, 4),
(5, 8),
(6, 4),
(6, 8),
(7, 4),
(7, 8),
(8, 4),
(8, 8),
(9, 4),
(9, 8),
(10, 4),
(10, 8),
(11, 4),
(11, 8),
(12, 4),
(12, 5),
(12, 8),
(13, 4),
(13, 8),
(14, 4),
(14, 7),
(14, 8),
(15, 4),
(15, 8),
(16, 4),
(16, 8),
(17, 4),
(17, 8),
(18, 4),
(18, 8),
(1, 1),
(1, 4),
(1, 8);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sw_aporte_evaluacion`
--
ALTER TABLE `sw_aporte_evaluacion`
  ADD PRIMARY KEY (`id_aporte_evaluacion`),
  ADD KEY `sw_aporte_evaluacion_id_periodo_evaluacion_foreign` (`id_periodo_evaluacion`),
  ADD KEY `sw_aporte_evaluacion_id_tipo_aporte_foreign` (`id_tipo_aporte`);

--
-- Indices de la tabla `sw_aporte_paralelo_cierre`
--
ALTER TABLE `sw_aporte_paralelo_cierre`
  ADD PRIMARY KEY (`id_aporte_paralelo_cierre`),
  ADD KEY `sw_aporte_paralelo_cierre_id_aporte_evaluacion_foreign` (`id_aporte_evaluacion`),
  ADD KEY `sw_aporte_paralelo_cierre_id_paralelo_foreign` (`id_paralelo`);

--
-- Indices de la tabla `sw_area`
--
ALTER TABLE `sw_area`
  ADD PRIMARY KEY (`id_area`);

--
-- Indices de la tabla `sw_asignatura`
--
ALTER TABLE `sw_asignatura`
  ADD PRIMARY KEY (`id_asignatura`),
  ADD KEY `sw_asignatura_id_area_foreign` (`id_area`),
  ADD KEY `sw_asignatura_id_tipo_asignatura_foreign` (`id_tipo_asignatura`);

--
-- Indices de la tabla `sw_asignatura_curso`
--
ALTER TABLE `sw_asignatura_curso`
  ADD PRIMARY KEY (`id_asignatura_curso`),
  ADD KEY `sw_asignatura_curso_id_periodo_lectivo_foreign` (`id_periodo_lectivo`),
  ADD KEY `sw_asignatura_curso_id_curso_foreign` (`id_curso`),
  ADD KEY `sw_asignatura_curso_id_asignatura_foreign` (`id_asignatura`);

--
-- Indices de la tabla `sw_asociar_curso_superior`
--
ALTER TABLE `sw_asociar_curso_superior`
  ADD PRIMARY KEY (`id_asociar_curso_superior`),
  ADD KEY `sw_asociar_curso_superior_id_periodo_lectivo_foreign` (`id_periodo_lectivo`),
  ADD KEY `sw_asociar_curso_superior_id_curso_inferior_foreign` (`id_curso_inferior`),
  ADD KEY `sw_asociar_curso_superior_id_curso_superior_foreign` (`id_curso_superior`);

--
-- Indices de la tabla `sw_curso`
--
ALTER TABLE `sw_curso`
  ADD PRIMARY KEY (`id_curso`),
  ADD KEY `sw_curso_id_especialidad_foreign` (`id_especialidad`);

--
-- Indices de la tabla `sw_dia_semana`
--
ALTER TABLE `sw_dia_semana`
  ADD PRIMARY KEY (`id_dia_semana`),
  ADD KEY `sw_dia_semana_id_periodo_lectivo_foreign` (`id_periodo_lectivo`);

--
-- Indices de la tabla `sw_distributivo`
--
ALTER TABLE `sw_distributivo`
  ADD PRIMARY KEY (`id_distributivo`),
  ADD KEY `sw_distributivo_id_periodo_lectivo_foreign` (`id_periodo_lectivo`),
  ADD KEY `sw_distributivo_id_malla_curricular_foreign` (`id_malla_curricular`),
  ADD KEY `sw_distributivo_id_paralelo_foreign` (`id_paralelo`),
  ADD KEY `sw_distributivo_id_asignatura_foreign` (`id_asignatura`),
  ADD KEY `sw_distributivo_id_usuario_foreign` (`id_usuario`);

--
-- Indices de la tabla `sw_escala_calificaciones`
--
ALTER TABLE `sw_escala_calificaciones`
  ADD PRIMARY KEY (`id_escala_calificaciones`),
  ADD KEY `sw_escala_calificaciones_id_periodo_lectivo_foreign` (`id_periodo_lectivo`);

--
-- Indices de la tabla `sw_especialidad`
--
ALTER TABLE `sw_especialidad`
  ADD PRIMARY KEY (`id_especialidad`),
  ADD KEY `sw_especialidad_id_tipo_educacion_foreign` (`id_tipo_educacion`);

--
-- Indices de la tabla `sw_horario`
--
ALTER TABLE `sw_horario`
  ADD PRIMARY KEY (`id_horario`),
  ADD KEY `sw_horario_id_asignatura_foreign` (`id_asignatura`),
  ADD KEY `sw_horario_id_paralelo_foreign` (`id_paralelo`),
  ADD KEY `sw_horario_id_dia_semana_foreign` (`id_dia_semana`),
  ADD KEY `sw_horario_id_hora_clase_foreign` (`id_hora_clase`),
  ADD KEY `sw_horario_id_usuario_foreign` (`id_usuario`);

--
-- Indices de la tabla `sw_hora_clase`
--
ALTER TABLE `sw_hora_clase`
  ADD PRIMARY KEY (`id_hora_clase`),
  ADD KEY `sw_hora_clase_id_periodo_lectivo_foreign` (`id_periodo_lectivo`);

--
-- Indices de la tabla `sw_hora_dia`
--
ALTER TABLE `sw_hora_dia`
  ADD PRIMARY KEY (`id_hora_dia`),
  ADD KEY `sw_hora_dia_id_dia_semana_foreign` (`id_dia_semana`),
  ADD KEY `sw_hora_dia_id_hora_clase_foreign` (`id_hora_clase`);

--
-- Indices de la tabla `sw_institucion`
--
ALTER TABLE `sw_institucion`
  ADD PRIMARY KEY (`id_institucion`);

--
-- Indices de la tabla `sw_jornada`
--
ALTER TABLE `sw_jornada`
  ADD PRIMARY KEY (`id_jornada`);

--
-- Indices de la tabla `sw_malla_curricular`
--
ALTER TABLE `sw_malla_curricular`
  ADD PRIMARY KEY (`id_malla_curricular`),
  ADD KEY `sw_malla_curricular_id_periodo_lectivo_foreign` (`id_periodo_lectivo`),
  ADD KEY `sw_malla_curricular_id_curso_foreign` (`id_curso`),
  ADD KEY `sw_malla_curricular_id_asignatura_foreign` (`id_asignatura`);

--
-- Indices de la tabla `sw_menu`
--
ALTER TABLE `sw_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indices de la tabla `sw_menu_perfil`
--
ALTER TABLE `sw_menu_perfil`
  ADD KEY `sw_menu_perfil_id_perfil_foreign` (`id_perfil`),
  ADD KEY `sw_menu_perfil_id_menu_foreign` (`id_menu`);

--
-- Indices de la tabla `sw_modalidad`
--
ALTER TABLE `sw_modalidad`
  ADD PRIMARY KEY (`id_modalidad`);

--
-- Indices de la tabla `sw_paralelo`
--
ALTER TABLE `sw_paralelo`
  ADD PRIMARY KEY (`id_paralelo`),
  ADD KEY `sw_paralelo_id_periodo_lectivo_foreign` (`id_periodo_lectivo`),
  ADD KEY `sw_paralelo_id_curso_foreign` (`id_curso`),
  ADD KEY `sw_paralelo_id_jornada_foreign` (`id_jornada`);

--
-- Indices de la tabla `sw_paralelo_inspector`
--
ALTER TABLE `sw_paralelo_inspector`
  ADD PRIMARY KEY (`id_paralelo_inspector`),
  ADD KEY `sw_paralelo_inspector_id_periodo_lectivo_foreign` (`id_periodo_lectivo`),
  ADD KEY `sw_paralelo_inspector_id_paralelo_foreign` (`id_paralelo`),
  ADD KEY `sw_paralelo_inspector_id_usuario_foreign` (`id_usuario`);

--
-- Indices de la tabla `sw_paralelo_tutor`
--
ALTER TABLE `sw_paralelo_tutor`
  ADD PRIMARY KEY (`id_paralelo_tutor`),
  ADD KEY `sw_paralelo_tutor_id_periodo_lectivo_foreign` (`id_periodo_lectivo`),
  ADD KEY `sw_paralelo_tutor_id_paralelo_foreign` (`id_paralelo`),
  ADD KEY `sw_paralelo_tutor_id_usuario_foreign` (`id_usuario`);

--
-- Indices de la tabla `sw_perfil`
--
ALTER TABLE `sw_perfil`
  ADD PRIMARY KEY (`id_perfil`);

--
-- Indices de la tabla `sw_periodo_estado`
--
ALTER TABLE `sw_periodo_estado`
  ADD PRIMARY KEY (`id_periodo_estado`);

--
-- Indices de la tabla `sw_periodo_evaluacion`
--
ALTER TABLE `sw_periodo_evaluacion`
  ADD PRIMARY KEY (`id_periodo_evaluacion`),
  ADD KEY `sw_periodo_evaluacion_id_periodo_lectivo_foreign` (`id_periodo_lectivo`),
  ADD KEY `sw_periodo_evaluacion_id_tipo_periodo_foreign` (`id_tipo_periodo`);

--
-- Indices de la tabla `sw_periodo_lectivo`
--
ALTER TABLE `sw_periodo_lectivo`
  ADD PRIMARY KEY (`id_periodo_lectivo`),
  ADD KEY `sw_periodo_lectivo_id_periodo_estado_foreign` (`id_periodo_estado`),
  ADD KEY `sw_periodo_lectivo_id_modalidad_foreign` (`id_modalidad`);

--
-- Indices de la tabla `sw_rubrica_evaluacion`
--
ALTER TABLE `sw_rubrica_evaluacion`
  ADD PRIMARY KEY (`id_rubrica_evaluacion`),
  ADD KEY `sw_rubrica_evaluacion_id_aporte_evaluacion_foreign` (`id_aporte_evaluacion`),
  ADD KEY `sw_rubrica_evaluacion_id_tipo_asignatura_foreign` (`id_tipo_asignatura`);

--
-- Indices de la tabla `sw_tipo_aporte`
--
ALTER TABLE `sw_tipo_aporte`
  ADD PRIMARY KEY (`id_tipo_aporte`);

--
-- Indices de la tabla `sw_tipo_asignatura`
--
ALTER TABLE `sw_tipo_asignatura`
  ADD PRIMARY KEY (`id_tipo_asignatura`);

--
-- Indices de la tabla `sw_tipo_educacion`
--
ALTER TABLE `sw_tipo_educacion`
  ADD PRIMARY KEY (`id_tipo_educacion`),
  ADD KEY `sw_tipo_educacion_id_periodo_lectivo_foreign` (`id_periodo_lectivo`);

--
-- Indices de la tabla `sw_tipo_periodo`
--
ALTER TABLE `sw_tipo_periodo`
  ADD PRIMARY KEY (`id_tipo_periodo`);

--
-- Indices de la tabla `sw_usuario`
--
ALTER TABLE `sw_usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `sw_usuario_perfil`
--
ALTER TABLE `sw_usuario_perfil`
  ADD KEY `sw_usuario_perfil_id_usuario_foreign` (`id_usuario`),
  ADD KEY `sw_usuario_perfil_id_perfil_foreign` (`id_perfil`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT de la tabla `sw_aporte_evaluacion`
--
ALTER TABLE `sw_aporte_evaluacion`
  MODIFY `id_aporte_evaluacion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `sw_aporte_paralelo_cierre`
--
ALTER TABLE `sw_aporte_paralelo_cierre`
  MODIFY `id_aporte_paralelo_cierre` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT de la tabla `sw_area`
--
ALTER TABLE `sw_area`
  MODIFY `id_area` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `sw_asignatura`
--
ALTER TABLE `sw_asignatura`
  MODIFY `id_asignatura` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `sw_asignatura_curso`
--
ALTER TABLE `sw_asignatura_curso`
  MODIFY `id_asignatura_curso` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT de la tabla `sw_asociar_curso_superior`
--
ALTER TABLE `sw_asociar_curso_superior`
  MODIFY `id_asociar_curso_superior` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `sw_curso`
--
ALTER TABLE `sw_curso`
  MODIFY `id_curso` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `sw_dia_semana`
--
ALTER TABLE `sw_dia_semana`
  MODIFY `id_dia_semana` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sw_distributivo`
--
ALTER TABLE `sw_distributivo`
  MODIFY `id_distributivo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT de la tabla `sw_escala_calificaciones`
--
ALTER TABLE `sw_escala_calificaciones`
  MODIFY `id_escala_calificaciones` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sw_especialidad`
--
ALTER TABLE `sw_especialidad`
  MODIFY `id_especialidad` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `sw_horario`
--
ALTER TABLE `sw_horario`
  MODIFY `id_horario` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- AUTO_INCREMENT de la tabla `sw_hora_clase`
--
ALTER TABLE `sw_hora_clase`
  MODIFY `id_hora_clase` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sw_hora_dia`
--
ALTER TABLE `sw_hora_dia`
  MODIFY `id_hora_dia` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `sw_institucion`
--
ALTER TABLE `sw_institucion`
  MODIFY `id_institucion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sw_jornada`
--
ALTER TABLE `sw_jornada`
  MODIFY `id_jornada` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sw_malla_curricular`
--
ALTER TABLE `sw_malla_curricular`
  MODIFY `id_malla_curricular` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT de la tabla `sw_menu`
--
ALTER TABLE `sw_menu`
  MODIFY `id_menu` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de la tabla `sw_modalidad`
--
ALTER TABLE `sw_modalidad`
  MODIFY `id_modalidad` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sw_paralelo`
--
ALTER TABLE `sw_paralelo`
  MODIFY `id_paralelo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `sw_paralelo_inspector`
--
ALTER TABLE `sw_paralelo_inspector`
  MODIFY `id_paralelo_inspector` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `sw_paralelo_tutor`
--
ALTER TABLE `sw_paralelo_tutor`
  MODIFY `id_paralelo_tutor` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `sw_perfil`
--
ALTER TABLE `sw_perfil`
  MODIFY `id_perfil` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `sw_periodo_estado`
--
ALTER TABLE `sw_periodo_estado`
  MODIFY `id_periodo_estado` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `sw_periodo_evaluacion`
--
ALTER TABLE `sw_periodo_evaluacion`
  MODIFY `id_periodo_evaluacion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sw_periodo_lectivo`
--
ALTER TABLE `sw_periodo_lectivo`
  MODIFY `id_periodo_lectivo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sw_rubrica_evaluacion`
--
ALTER TABLE `sw_rubrica_evaluacion`
  MODIFY `id_rubrica_evaluacion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `sw_tipo_aporte`
--
ALTER TABLE `sw_tipo_aporte`
  MODIFY `id_tipo_aporte` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sw_tipo_asignatura`
--
ALTER TABLE `sw_tipo_asignatura`
  MODIFY `id_tipo_asignatura` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `sw_tipo_educacion`
--
ALTER TABLE `sw_tipo_educacion`
  MODIFY `id_tipo_educacion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sw_tipo_periodo`
--
ALTER TABLE `sw_tipo_periodo`
  MODIFY `id_tipo_periodo` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `sw_usuario`
--
ALTER TABLE `sw_usuario`
  MODIFY `id_usuario` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `sw_aporte_evaluacion`
--
ALTER TABLE `sw_aporte_evaluacion`
  ADD CONSTRAINT `sw_aporte_evaluacion_id_periodo_evaluacion_foreign` FOREIGN KEY (`id_periodo_evaluacion`) REFERENCES `sw_periodo_evaluacion` (`id_periodo_evaluacion`),
  ADD CONSTRAINT `sw_aporte_evaluacion_id_tipo_aporte_foreign` FOREIGN KEY (`id_tipo_aporte`) REFERENCES `sw_tipo_aporte` (`id_tipo_aporte`);

--
-- Filtros para la tabla `sw_aporte_paralelo_cierre`
--
ALTER TABLE `sw_aporte_paralelo_cierre`
  ADD CONSTRAINT `sw_aporte_paralelo_cierre_id_aporte_evaluacion_foreign` FOREIGN KEY (`id_aporte_evaluacion`) REFERENCES `sw_aporte_evaluacion` (`id_aporte_evaluacion`),
  ADD CONSTRAINT `sw_aporte_paralelo_cierre_id_paralelo_foreign` FOREIGN KEY (`id_paralelo`) REFERENCES `sw_paralelo` (`id_paralelo`);

--
-- Filtros para la tabla `sw_asignatura`
--
ALTER TABLE `sw_asignatura`
  ADD CONSTRAINT `sw_asignatura_id_area_foreign` FOREIGN KEY (`id_area`) REFERENCES `sw_area` (`id_area`),
  ADD CONSTRAINT `sw_asignatura_id_tipo_asignatura_foreign` FOREIGN KEY (`id_tipo_asignatura`) REFERENCES `sw_tipo_asignatura` (`id_tipo_asignatura`);

--
-- Filtros para la tabla `sw_asignatura_curso`
--
ALTER TABLE `sw_asignatura_curso`
  ADD CONSTRAINT `sw_asignatura_curso_id_asignatura_foreign` FOREIGN KEY (`id_asignatura`) REFERENCES `sw_asignatura` (`id_asignatura`),
  ADD CONSTRAINT `sw_asignatura_curso_id_curso_foreign` FOREIGN KEY (`id_curso`) REFERENCES `sw_curso` (`id_curso`),
  ADD CONSTRAINT `sw_asignatura_curso_id_periodo_lectivo_foreign` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Filtros para la tabla `sw_asociar_curso_superior`
--
ALTER TABLE `sw_asociar_curso_superior`
  ADD CONSTRAINT `sw_asociar_curso_superior_id_curso_inferior_foreign` FOREIGN KEY (`id_curso_inferior`) REFERENCES `sw_curso` (`id_curso`),
  ADD CONSTRAINT `sw_asociar_curso_superior_id_curso_superior_foreign` FOREIGN KEY (`id_curso_superior`) REFERENCES `sw_curso` (`id_curso`),
  ADD CONSTRAINT `sw_asociar_curso_superior_id_periodo_lectivo_foreign` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Filtros para la tabla `sw_curso`
--
ALTER TABLE `sw_curso`
  ADD CONSTRAINT `sw_curso_id_especialidad_foreign` FOREIGN KEY (`id_especialidad`) REFERENCES `sw_especialidad` (`id_especialidad`);

--
-- Filtros para la tabla `sw_dia_semana`
--
ALTER TABLE `sw_dia_semana`
  ADD CONSTRAINT `sw_dia_semana_id_periodo_lectivo_foreign` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Filtros para la tabla `sw_distributivo`
--
ALTER TABLE `sw_distributivo`
  ADD CONSTRAINT `sw_distributivo_id_asignatura_foreign` FOREIGN KEY (`id_asignatura`) REFERENCES `sw_asignatura` (`id_asignatura`),
  ADD CONSTRAINT `sw_distributivo_id_malla_curricular_foreign` FOREIGN KEY (`id_malla_curricular`) REFERENCES `sw_malla_curricular` (`id_malla_curricular`),
  ADD CONSTRAINT `sw_distributivo_id_paralelo_foreign` FOREIGN KEY (`id_paralelo`) REFERENCES `sw_paralelo` (`id_paralelo`),
  ADD CONSTRAINT `sw_distributivo_id_periodo_lectivo_foreign` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`),
  ADD CONSTRAINT `sw_distributivo_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `sw_usuario` (`id_usuario`);

--
-- Filtros para la tabla `sw_escala_calificaciones`
--
ALTER TABLE `sw_escala_calificaciones`
  ADD CONSTRAINT `sw_escala_calificaciones_id_periodo_lectivo_foreign` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Filtros para la tabla `sw_especialidad`
--
ALTER TABLE `sw_especialidad`
  ADD CONSTRAINT `sw_especialidad_id_tipo_educacion_foreign` FOREIGN KEY (`id_tipo_educacion`) REFERENCES `sw_tipo_educacion` (`id_tipo_educacion`);

--
-- Filtros para la tabla `sw_horario`
--
ALTER TABLE `sw_horario`
  ADD CONSTRAINT `sw_horario_id_asignatura_foreign` FOREIGN KEY (`id_asignatura`) REFERENCES `sw_asignatura` (`id_asignatura`),
  ADD CONSTRAINT `sw_horario_id_dia_semana_foreign` FOREIGN KEY (`id_dia_semana`) REFERENCES `sw_dia_semana` (`id_dia_semana`),
  ADD CONSTRAINT `sw_horario_id_hora_clase_foreign` FOREIGN KEY (`id_hora_clase`) REFERENCES `sw_hora_clase` (`id_hora_clase`),
  ADD CONSTRAINT `sw_horario_id_paralelo_foreign` FOREIGN KEY (`id_paralelo`) REFERENCES `sw_paralelo` (`id_paralelo`),
  ADD CONSTRAINT `sw_horario_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `sw_usuario` (`id_usuario`);

--
-- Filtros para la tabla `sw_hora_clase`
--
ALTER TABLE `sw_hora_clase`
  ADD CONSTRAINT `sw_hora_clase_id_periodo_lectivo_foreign` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Filtros para la tabla `sw_hora_dia`
--
ALTER TABLE `sw_hora_dia`
  ADD CONSTRAINT `sw_hora_dia_id_dia_semana_foreign` FOREIGN KEY (`id_dia_semana`) REFERENCES `sw_dia_semana` (`id_dia_semana`),
  ADD CONSTRAINT `sw_hora_dia_id_hora_clase_foreign` FOREIGN KEY (`id_hora_clase`) REFERENCES `sw_hora_clase` (`id_hora_clase`);

--
-- Filtros para la tabla `sw_malla_curricular`
--
ALTER TABLE `sw_malla_curricular`
  ADD CONSTRAINT `sw_malla_curricular_id_asignatura_foreign` FOREIGN KEY (`id_asignatura`) REFERENCES `sw_asignatura` (`id_asignatura`),
  ADD CONSTRAINT `sw_malla_curricular_id_curso_foreign` FOREIGN KEY (`id_curso`) REFERENCES `sw_curso` (`id_curso`),
  ADD CONSTRAINT `sw_malla_curricular_id_periodo_lectivo_foreign` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Filtros para la tabla `sw_menu_perfil`
--
ALTER TABLE `sw_menu_perfil`
  ADD CONSTRAINT `sw_menu_perfil_id_menu_foreign` FOREIGN KEY (`id_menu`) REFERENCES `sw_menu` (`id_menu`),
  ADD CONSTRAINT `sw_menu_perfil_id_perfil_foreign` FOREIGN KEY (`id_perfil`) REFERENCES `sw_perfil` (`id_perfil`);

--
-- Filtros para la tabla `sw_paralelo`
--
ALTER TABLE `sw_paralelo`
  ADD CONSTRAINT `sw_paralelo_id_curso_foreign` FOREIGN KEY (`id_curso`) REFERENCES `sw_curso` (`id_curso`),
  ADD CONSTRAINT `sw_paralelo_id_jornada_foreign` FOREIGN KEY (`id_jornada`) REFERENCES `sw_jornada` (`id_jornada`),
  ADD CONSTRAINT `sw_paralelo_id_periodo_lectivo_foreign` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Filtros para la tabla `sw_paralelo_inspector`
--
ALTER TABLE `sw_paralelo_inspector`
  ADD CONSTRAINT `sw_paralelo_inspector_id_paralelo_foreign` FOREIGN KEY (`id_paralelo`) REFERENCES `sw_paralelo` (`id_paralelo`),
  ADD CONSTRAINT `sw_paralelo_inspector_id_periodo_lectivo_foreign` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`),
  ADD CONSTRAINT `sw_paralelo_inspector_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `sw_usuario` (`id_usuario`);

--
-- Filtros para la tabla `sw_paralelo_tutor`
--
ALTER TABLE `sw_paralelo_tutor`
  ADD CONSTRAINT `sw_paralelo_tutor_id_paralelo_foreign` FOREIGN KEY (`id_paralelo`) REFERENCES `sw_paralelo` (`id_paralelo`),
  ADD CONSTRAINT `sw_paralelo_tutor_id_periodo_lectivo_foreign` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`),
  ADD CONSTRAINT `sw_paralelo_tutor_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `sw_usuario` (`id_usuario`);

--
-- Filtros para la tabla `sw_periodo_evaluacion`
--
ALTER TABLE `sw_periodo_evaluacion`
  ADD CONSTRAINT `sw_periodo_evaluacion_id_periodo_lectivo_foreign` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`),
  ADD CONSTRAINT `sw_periodo_evaluacion_id_tipo_periodo_foreign` FOREIGN KEY (`id_tipo_periodo`) REFERENCES `sw_tipo_periodo` (`id_tipo_periodo`);

--
-- Filtros para la tabla `sw_periodo_lectivo`
--
ALTER TABLE `sw_periodo_lectivo`
  ADD CONSTRAINT `sw_periodo_lectivo_id_modalidad_foreign` FOREIGN KEY (`id_modalidad`) REFERENCES `sw_modalidad` (`id_modalidad`),
  ADD CONSTRAINT `sw_periodo_lectivo_id_periodo_estado_foreign` FOREIGN KEY (`id_periodo_estado`) REFERENCES `sw_periodo_estado` (`id_periodo_estado`);

--
-- Filtros para la tabla `sw_rubrica_evaluacion`
--
ALTER TABLE `sw_rubrica_evaluacion`
  ADD CONSTRAINT `sw_rubrica_evaluacion_id_aporte_evaluacion_foreign` FOREIGN KEY (`id_aporte_evaluacion`) REFERENCES `sw_aporte_evaluacion` (`id_aporte_evaluacion`),
  ADD CONSTRAINT `sw_rubrica_evaluacion_id_tipo_asignatura_foreign` FOREIGN KEY (`id_tipo_asignatura`) REFERENCES `sw_tipo_asignatura` (`id_tipo_asignatura`);

--
-- Filtros para la tabla `sw_tipo_educacion`
--
ALTER TABLE `sw_tipo_educacion`
  ADD CONSTRAINT `sw_tipo_educacion_id_periodo_lectivo_foreign` FOREIGN KEY (`id_periodo_lectivo`) REFERENCES `sw_periodo_lectivo` (`id_periodo_lectivo`);

--
-- Filtros para la tabla `sw_usuario_perfil`
--
ALTER TABLE `sw_usuario_perfil`
  ADD CONSTRAINT `sw_usuario_perfil_id_perfil_foreign` FOREIGN KEY (`id_perfil`) REFERENCES `sw_perfil` (`id_perfil`),
  ADD CONSTRAINT `sw_usuario_perfil_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `sw_usuario` (`id_usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
